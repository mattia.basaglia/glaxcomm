import os
import sys


libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "lib")
sys.path.append(os.path.join(libdir, "lottie", "lib"))


def set_default_settings():
    os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

    os.environ.setdefault(
        'DJANGO_SETTINGS_MODULE',
        open(os.path.join(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
            "default_settings"
        )).read().strip()
    )
