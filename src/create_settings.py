#!/usr/bin/env python3
import os
import sys
from collections import OrderedDict
from distutils.util import strtobool
from django.core.management.utils import get_random_secret_key
from django.template.engine import Engine
from django.template.context import Context
from django.conf import settings


data = OrderedDict()
maxkl = 0


def boolean(str):
    return bool(strtobool(str))


def get_value(prompt, default, type):
    fullp = "%s [%s]: " % (prompt, default)
    val = input(fullp)
    if not val:
        val = default
    return type(val)


def get_data_value(prompt, default, type, key=None):
    global maxkl
    if not key:
        key = prompt
    val = get_value(prompt.replace("_", " "), default, type)
    data[key] = val
    if len(key) > maxkl:
        maxkl = len(key)
    return val


modulename = get_data_value("module", "settings.local", str)

get_data_value("debug", "Y", boolean)
get_data_value("allowed_hosts", "127.0.0.1 localhost", lambda x: x.split(" "))

dbtype = get_data_value("database_type", "sqlite", str)
if dbtype == "sqlite":
    get_data_value("database_name", "db.sqlite3", str)
elif dbtype == "postgres":
    get_data_value("database_name", "glaxcomm", str)
    get_data_value("database_user", "postgres", str)
    get_data_value("database_password", "postgres", str)
    get_data_value("database_host", "127.0.0.1", str)
    get_data_value("database_port", 5432, int)
elif dbtype == "mariadb":
    get_data_value("database_name", "glaxcomm", str)
    get_data_value("database_user", "mariadb", str)
    get_data_value("database_password", "mariadb", str)
    get_data_value("database_host", "127.0.0.1", str)
    get_data_value("database_port", 3306, int)


if not data["debug"]:
    get_data_value("data_root", "/var/www/comm.dragon.best/public", str)

print("")
for name, value in data.items():
    print("%s: %s" % (name.ljust(maxkl), value))

print("")
if get_value("Confirm?", "Y", boolean):
    here = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(here, *modulename.split(".")) + ".py"
    if os.path.exists(filename):
        if not get_value("Overwrite existing module?", "N", boolean):
            print("Canceled")
            sys.exit(0)

    print("Writing")
    data["secret_key"] = get_random_secret_key()
    engine = Engine([os.path.join(here, "templates")], debug=True);
    template = engine.find_template("internal/settings.py")[0]
    context = Context(data)
    settings.configure()
    rendered = template.render(context)
    with open(filename, "w") as outfile:
        outfile.write(rendered)
    print("Created %s" % filename)
else:
    print("Canceled")
