import os
from settings.default import *
from django.contrib import messages


SECRET_KEY = '{{ secret_key }}'

DEBUG = {{ debug }}

{% if debug %}
MESSAGE_LEVEL = messages.DEBUG

INTERNAL_IPS = [
    '127.0.0.1',
]
{% endif %}

ALLOWED_HOSTS = [{% for host in allowed_hosts %}
    '{{ host }}',{% endfor %}
]


DATABASES = {
    'default': {{% if database_type == "sqlite" %}
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '{{ database_name }}'),
    {% else %}
    {% if database_type == "mariadb" %}
    'ENGINE': 'django.db.backends.mysql',
    {% elif database_type == "postgres" %}
    'ENGINE': 'django.db.backends.postgresql',
    {% endif %}
    'NAME': '{{ database_name }}',
    'USER': '{{ database_user }}',
    'PASSWORD': '{{ database_password }}',
    'HOST': '{{ database_host }}',
    'PORT': {{ database_port }},
    {% endif %}}
}

{% if debug %}
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
{% else %}
data_root = "{{ data_root }}"
MEDIA_ROOT = os.path.join(data_root, "media")
STATIC_ROOT = os.path.join(data_root, "static")
{% endif %}

# Keys
TELEGRAM = {
    "default": {
        "TOKEN": "",
    },
}

TRELLO_KEY = ""


TWITTER = {
    "API_KEY": "",
    "SECRET_KEY": "",
}
