import uuid
import urllib.parse
import urllib.request
import urllib.error


Request = urllib.request.Request


def urljoin(*pieces):
    if len(pieces) == 0:
        return ""
    pieces = list(pieces)
    root = pieces.pop(0)
    while pieces:
        head = pieces.pop(0)
        if isinstance(head, dict):
            root += "?" + urllib.parse.urlencode(head)
        elif head.startswith("#"):
            root += head
        elif head.startswith("/"):
            if root.endswith("/"):
                root += head[1:]
            else:
                root += head
        elif root.endswith("/"):
            root += head
        else:
            root += "/" + head
    return root


def urlopen(*a, **kw):
    try:
        uokw = {}
        if "timeout" in kw:
            uokw["timeout"] = kw.pop("timeout")
        response = urllib.request.urlopen(Request(*a, **kw), **uokw)
        response.error = False
        return response
    except urllib.error.HTTPError as e:
        e.error = True
        return e


def parse_querystring(qs):
    return dict(urllib.parse.parse_qsl(qs.decode("ascii")))


class MultiPartEncoder:
    def __init__(self, boundary=None):
        self.boundary = boundary or uuid.uuid4().hex.encode("ascii")

    @property
    def content_type(self):
        return "multipart/form-data; boundary=%s" % self.boundary.decode("ascii")

    def format_field(self, name, value):
        return b"--%s\r\nContent-Disposition: form-data; name=\"%s\"\r\n\r\n%s\r\n" % (
            self.boundary,
            name.encode("utf-8"),
            self._enc(value)
        )

    def format_file(self, name, value, filename, content_type):
        return (
            b"--%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n" +
            b"Content-Type: %s\r\n\r\n%s\r\n"
        ) % (
            self.boundary,
            name.encode("utf-8"),
            filename.encode("utf-8"),
            content_type.encode("utf-8"),
            self._enc(value)
        )

    def _enc(self, v):
        if isinstance(v, bytes):
            return v
        return str(v).encode("utf-8")

    def format_end(self):
        return b"--%s--\r\n" % self.boundary
