from . import api


class Participant:
    def __init__(self, user: api.User):
        self.user = user
        self.comments = []
        self.retweet = False
        self.faved = False


class Raffle:
    def __init__(self, client: api.AuthorizedTwitter, message: api.Message):
        self.client = client
        self.message = message
        self.participants = {}

    def get_retweeted(self):
        # NOTE: issues in the twitter API means this only works up to 100 retweets
        # but it's possible to capture retweets as they happen with the streaming API
        for retweet in message.fetch_retweets():
            self._participant(retweet.user).retweet = True

    def get_faved(self):
        pass

    def get_replied(self):
        pass

    def _participant(self, user: api.User):
        if user.user_id not in self.participants:
            self.participants[user.user_id] = Participant(user)
        return self.participants[user.user_id]

    @property
    def participant_list(self):
        return list(self.participants.values())
