from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import cached_property
from . import api


class TwitterToken(models.Model):
    user = models.OneToOneField(User, models.CASCADE, related_name="twitter_token")
    oauth_token = models.CharField(max_length=32)
    oauth_token_secret = models.CharField(max_length=32)

    class Meta:
        unique_together = [
            ["oauth_token", "oauth_token_secret"]
        ]

    @cached_property
    def api(self):
        return api.AuthorizedTwitter(api.Twitter(), self.oauth_token, self.oauth_token_secret)
