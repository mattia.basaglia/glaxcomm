import json
import time
import datetime
import email.utils
from collections import namedtuple
from django.conf import settings
from django.urls import reverse
from django.utils.functional import cached_property
import oauth2
from apps.httputils import urljoin, urlopen, parse_querystring


class Twitter:
    def __init__(self, api_key=None, secret_key=None):
        self.api_key = api_key or settings.TWITTER["API_KEY"]
        self.secret_key = secret_key or settings.TWITTER["SECRET_KEY"]
        self.api_url = "https://api.twitter.com/"
        self.consumer = oauth2.Consumer(self.api_key, self.secret_key)

    def oauth_url(self, request):
        callback_url = request.build_absolute_uri(reverse("twitter:callback"))
        request_token_url = urljoin(self.api_url, "oauth/request_token")
        client = oauth2.Client(self.consumer)
        resp, content = client.request(request_token_url, "GET")
        if resp["status"] != "200":
            return None

        authorize_url = urljoin(self.api_url, "oauth/authorize")
        request_token = parse_querystring(content)
        request.session["twitter_oauth_token"] = request_token["oauth_token"]
        request.session["twitter_oauth_token_secret"] = request_token["oauth_token_secret"]
        return "%s?oauth_token=%s" % (authorize_url, request_token["oauth_token"])

    def oauth_verify(self, request):
        twitter_oauth_token = request.session.get("twitter_oauth_token", "")
        twitter_oauth_token_secret = request.session.get("twitter_oauth_token_secret", "")
        oauth_verifier = request.GET.get("oauth_verifier", "")
        if not oauth_verifier or not twitter_oauth_token or not twitter_oauth_token_secret:
            return False

        token = oauth2.Token(twitter_oauth_token, twitter_oauth_token_secret)
        token.set_verifier(oauth_verifier)
        client = oauth2.Client(self.consumer, token)
        access_token_url = urljoin(self.api_url, "oauth/access_token")
        resp, content = client.request(access_token_url, "POST")
        access_token = parse_querystring(content)
        from .models import TwitterToken
        try:
            TwitterToken.objects.update_or_create(
                user=request.user,
                defaults={
                    "oauth_token": access_token["oauth_token"],
                    "oauth_token_secret": access_token["oauth_token_secret"]
                }
            )
            return True
        except KeyError:
            return False


class AuthorizedTwitter:
    def __init__(self, twitter, oauth_token, oauth_token_secret):
        self.twitter = twitter
        self.api_url = urljoin(twitter.api_url, "1.1")
        self.token = oauth2.Token(oauth_token, oauth_token_secret)
        self.client = oauth2.Client(twitter.consumer, self.token)

    def request(self, api_method, url_params, http_method):
        resp, content = self.client.request(urljoin(self.api_url, api_method, url_params), http_method)
        if not content:
            return None
        return json.loads(content.decode("utf-8"))

    def get(self, api_method, url_params={}):
        return self.request(api_method, url_params, "GET")

    def post(self, api_method, url_params={}):
        return self.request(api_method, url_params, "POST")

    @cached_property
    def settings(self):
        return UserSettings(self)

    def tweet(self, id):
        data = self.get("statuses/show.json", {"id": id})
        if not data or "errors" in data:
            return None

        return Message(self, data)


class TwitterObject:
    def __init__(self, client: AuthorizedTwitter, data=None, autofetch=True):
        self.client = client
        if data is not None:
            self.from_data(data)
        elif autofetch:
            self.refresh()

    def from_data(self, data):
        raise NotImplementedError()

    def refresh(self):
        data = self._get_data()
        self.error = "errors" in data
        if not self.error:
            self.from_data(data)

    def _get_data(self):
        raise NotImplementedError()


class UserSettings(TwitterObject):
    def _get_data(self):
        return self.client.get("account/settings.json")

    def from_data(self, data):
        self.screen_name = data["screen_name"]

    @cached_property
    def user(self):
        return User(self.client, screen_name=self.screen_name)


class User(TwitterObject):
    def __init__(self, client, user_id=None, screen_name=None, data=None, autofetch=True):
        self.screen_name = screen_name
        self.user_id = user_id
        super().__init__(client, data, autofetch)

    def _get_data(self):
        if self.user_id is not None:
            params = {"user_id": self.user_id}
        else:
            params = {"screen_name": self.screen_name}
        return self.client.get("users/show.json", params)

    def from_data(self, data):
        self.user_id = data["id"]
        self.name = data["name"]
        self.screen_name = data["screen_name"]
        self.url = data["url"]
        self.description = data["description"]
        self.protected = data["protected"]
        self.verified = data["verified"]
        self.followers_count = data["followers_count"]
        self.friends_count = data["friends_count"]
        self.statuses_count = data["statuses_count"]
        self.profile_image_url_https = data["profile_image_url_https"]
        self.link = "https://twitter.com/%s" % self.screen_name


class OutMessage:
    def __init__(self, client: AuthorizedTwitter, text, possibly_sensitive=False):
        self.client = client
        self.text = text
        self.possibly_sensitive = possibly_sensitive
        self.media = []

    def send(self):
        params = {
            "status": self.text,
            "possibly_sensitive": self.possibly_sensitive,
            "trim_user": False,
        }
        data = self.client.post("statuses/update.json", params)
        if "errors" in data:
            return TwitterError(data)
        return Message(self.client, data)


class TwitterError:
    Error = namedtuple("Error", ["code", "message"])

    def __init__(self, data):
        self.errors = [self.Error(**e) for e in data["errors"]]

    def __bool__(self):
        return False


class Message(TwitterObject):
    def from_data(self, data):
        # contributors
        # coordinates
        self.created_at = datetime.datetime.fromtimestamp(time.mktime(email.utils.parsedate(data["created_at"])))
        # entities
        self.favorite_count = data["favorite_count"]
        self.favorited = data["favorited"]
        # geo
        self.id = data["id"]
        # in_reply_*
        # is_quote_status
        # lang
        # place
        self.retweet_count = data["retweet_count"]
        self.retweeted = data["retweeted"]
        # source
        self.text = data["text"]
        self.truncated = data["truncated"]
        if len(data["user"]) > 2:
            self.user = User(self.client, data=data["user"])
        else:
            self.user = User(self.client, user_id=data["user"]["id"], autofetch=False)

    @property
    def url(self):
        return "https://twitter.com/%s/status/%s" % (self.user.screen_name, self.id)

    def fetch_retweets(self):
        return [
            Message(self.client, data)
            for data in self.client.get("statuses/retweets/%s.json" % self.id)
        ]
