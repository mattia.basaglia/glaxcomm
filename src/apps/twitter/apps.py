import re
import json

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http.response import HttpResponseRedirect, Http404

from apps.social_media.integration import Integration, view


def token_view(*view_a, **view_kw):
    def decorator(func):
        @view(*view_a, **view_kw)
        def wrapper(self, request, **kw):
            token = getattr(request.user, "twitter_token", None)
            if token is None:
                return redirect("twitter:connect")
            return func(self, request, token, **kw)
        return wrapper
    return decorator


class TwitterConfig(Integration):
    name = 'apps.twitter'
    is_connected_attr = "twitter_token"

    def integration_ready(self):
        from . import api
        self.api = api

    @view("")
    def manage(self, request):
        token = getattr(request.user, "twitter_token", None)
        ctx = {
            "token": token,
        }
        return render(request, "twitter/manage.html", ctx)

    @view()
    def compose(self, request):
        result = self.api.OutMessage(token.api, "Just testing the API yet again").send()
        if not result:
            if request.user.is_superuser:
                for error in result.errors:
                    messages.error(request, "Twitter error %s: %s" % (error.code, error.message))

    @view()
    def callback(self, request):
        success = self.api.Twitter().oauth_verify(request)
        if not success:
            messages.error(request, "Could not verify Twitter token")
        return redirect("twitter:manage")

    @view()
    def connect(self, request):
        redirect_url = self.api.Twitter().oauth_url(request)
        if not redirect_url:
            messages.error(request, "Could not connect to Twitter")
            return redirect("twitter:manage")
        return HttpResponseRedirect(redirect_url)

    @view()
    def disconnect(self, request):
        self.models_module.TwitterToken.objects.filter(user=request.user).delete()
        return redirect("twitter:manage")

    @view()
    def test(self, request):
        token = getattr(request.user, "twitter_token", None)

        if not request.user.is_superuser or not token:
            raise Http404()

        data = ""
        method = ""
        field_info = []
        n_fields = 0

        if request.method == "POST":
            http = "get"
            n_fields = int(request.POST.get("n_fields"))
            method = request.POST.get("method")
            fields = {}

            for i in range(n_fields):
                name = request.POST.get("field_name_%s" % i)
                value_key = "field_value_%s" % i
                #if value_key in request.FILES:
                #    file = request.FILES[value_key]
                #    value = http_api.InputFile(file.name, file.read())
                #    http = "post"
                #else:
                value = request.POST.get(value_key)

                field_info.append((i, name, value))
                if name:
                    fields[name] = value

            data = json.dumps(getattr(token.api, http)(method, fields), indent="  ")

        ctx = {
            "data": data,
            "field_info": field_info,
            "method": method,
            "n_fields": n_fields,
        }
        return render(request, "twitter/api_test.html", ctx)

    @view("raffle")
    def raffle_start(self, request):
        id = request.POST.get("id", "")
        if "/" in id:
            match = re.search("([0-9]+)", id)
            id = match.group(0) if match else ""

        if not id:
            return render(request, "twitter/raffle_start.html", {})

        return redirect("twitter:raffle", id=id)

    @token_view("raffle/<str:id>")
    def raffle(self, request, token, id):

        message = token.api.tweet(id)

        participants = []

        if request.method == "POST":
            from .raffle import Raffle
            raffle = Raffle(token.api, message)
            raffle.get_retweeted()
            raffle.get_faved()
            raffle.get_replied()
            participants = raffle.participant_list()

        ctx = {
            "message": message,
            "participants": participants,
        }
        return render(request, "twitter/raffle.html", ctx)
