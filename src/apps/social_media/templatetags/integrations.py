from django import template
from ..integration import Integration


register = template.Library()


@register.filter
def all_integrations(user):
    return Integration.all()


@register.filter
def active_integrations(user):
    for integ in Integration.all():
        if integ.is_connected(user):
            yield integ
