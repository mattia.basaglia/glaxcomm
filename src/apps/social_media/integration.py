import inspect
from django.urls import reverse
from django.apps import AppConfig
from django.urls import path, include
from django.contrib.auth.decorators import login_required


class Integration(AppConfig):
    _all = []

    def __init__(self, app_name, app_module):
        super().__init__(app_name, app_module)

        if not hasattr(self, "awesome_icon"):
            self.awesome_icon = (self.label, "fab")

        self.urlpatterns = path(
            self.label+"/",
            include((list(self._urls()), self.label))
        )

    def _urls(self):
        for attname in dir(self):
            att = inspect.getattr_static(self, attname)
            if inspect.getattr_static(att, "is_view", False):
                yield path(att.view_path, bound_view(getattr(self, attname)), name=attname)

    def url(self, name):
        return reverse("%s:%s" % (self.label, name))

    @property
    def default_url(self):
        return self.url("manage")

    def icon(self):
        from fontawesome_5.templatetags.fontawesome_5 import fa5_icon
        return fa5_icon(*self.awesome_icon)

    def __str__(self):
        return self.name

    def is_connected(self, user):
        return hasattr(user, self.is_connected_attr)

    def ready(self):
        Integration._all.append(self)
        self.integration_ready()

    @staticmethod
    def all():
        return Integration._all

    def integration_ready(self):
        pass

    def wsgi_ready(self):
        pass


def view(path=None, login_required=True):
    def decorator(func):
        func.login_required = login_required
        func.is_view = True

        if path is not None:
            func.view_path = path
        else:
            func.view_path = func.__name__ + "/"

        return func

    return decorator


def bound_view(func):
    wrapper = func
    if func.login_required:
        wrapper = login_required(wrapper)
    return wrapper
