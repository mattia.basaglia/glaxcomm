from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
import lottie.utils.font


@login_required
def home(request):
    ctx = {
    }
    return render(request, "dashboard/home.html", ctx)


@user_passes_test(lambda u: u.is_superuser)
def fonts(request):
    ctx = {
        "fonts": sorted(lottie.utils.font.fonts, key=lambda x: x.family),
    }
    return render(request, "dashboard/fonts.html", ctx)
