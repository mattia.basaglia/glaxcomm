class EmojiBoard
{
    constructor(element, on_click, max_version, style)
    {
        this.board_element = element;
        this.on_click = on_click;
        this.max_version = max_version;

        let group_links = this.board_element.appendChild(document.createElement("ul"));
        group_links.classList.add("emoji-group-links");

        let board_main = this.board_element.appendChild(document.createElement("div"));
        board_main.classList.add("emoji-board-main");

        this.group_links = [];
        this.group_elements = [];

        let first = null;
        for ( let group of emoji_data )
        {
            let group_link = group_links.appendChild(document.createElement("li")).appendChild(document.createElement("a"));
            group_link.addEventListener("click", this.on_header_click.bind(this));
            group_link.dataset.emojiGroup = group.name;
            group_link.title = group.name;
            group_link.appendChild(document.createTextNode(group.emoji[0].unicode));
            this.group_links.push(group_link);

            let group_element = board_main.appendChild(document.createElement("div"));
            group_element.classList.add("emoji-group");
            group_element.dataset.emojiGroup = group.name;
            group_element.appendChild(document.createElement("header")).appendChild(document.createTextNode(group.name));
            let group_emoji_container = group_element.appendChild(document.createElement("div"));
            for ( let emoji of group.emoji )
            {
                if ( this.passes_filter(emoji) )
                {
                    let emoji_div = group_emoji_container.appendChild(document.createElement("div"));
                    emoji_div.classList.add("emoji-emoji");
                    emoji_div.title = emoji.name;
                    emoji_div.dataset.emoji = emoji.unicode;
                    emoji_div.addEventListener("click", (e) => this.on_click(emoji.unicode));
                    emoji_div.appendChild(document.createTextNode(emoji.unicode));
                }
            }
            this.group_elements.push(group_element);
        }

        this.on_header_click_element(this.group_links[0]);
        this.visible = true;
        this.target = null;
        this.style = style;
        this.style(this.board_element);
    }

    set_target(target)
    {
        let element = emoji_get_element(target);
        this.on_click = (unicode) => element.value += unicode;
        this.target = element;
    }

    passes_filter(emoji)
    {
        if ( !this.max_version )
            return true;

        if ( this.max_version[0] > emoji.version[0] )
            return true;

        return this.max_version[0] == emoji.version[0] && this.max_version[1] >= emoji.version[1];
    }

    on_header_click_element(element)
    {
        for ( let group_link of this.group_links )
        {
            group_link.classList.remove("emoji-active");
        }
        element.classList.add("emoji-active");

        for ( let group of this.group_elements )
        {
            group.style.display = group.dataset.emojiGroup == element.dataset.emojiGroup ? "block" : "none";
        }
    }

    on_header_click(event)
    {
        this.on_header_click_element(event.target);
    }

    show()
    {
        this._visible = true;
        this.board_element.style.display = "flex";
    }

    hide()
    {
        this._visible = false;
        this.board_element.style.display = "none";
    }

    set visible(visible)
    {
        if ( visible )
            this.show();
        else
            this.hide();
    }

    get visible()
    {
        return this._visible;
    }

    add_target(target)
    {
        let board = this;

        let target_element = emoji_get_element(target);
        target_element.addEventListener("focus", (ev) => board.set_target(target_element));

        if ( !this.target )
            this.set_target(target_element);
    }
}

const EmojiStyle = Object.freeze({
    native: (e) => null,
    twemoji: (e) => twemoji.parse(e)
});

function emoji_get_element(element)
{
    if ( !(element instanceof HTMLElement) )
        return document.querySelector(element);
    return element;
}

function emoji_board(conf)
{
    let on_click = console.log;
    if ( "on_click" in conf )
        on_click = conf.on_click;

    let style = EmojiStyle.native;
    if ( "style" in conf )
        style = conf.style;

    let board = new EmojiBoard(emoji_get_element(conf.element), on_click, conf.max_version, style);

    if ( "target" in conf )
        board.set_target(conf.target);

    if ( "targets" in conf )
    {
        for ( let target of conf.targets )
            board.add_target(target);
    }

    if ( "visible" in conf )
        board.visible = conf.visible;

    return board;
}

function emoji_board_toggle_button(board, target=null, title="Emoji")
{
    let button = document.createElement("button");
    button.addEventListener("click", function (event){
        if ( target && target != board.target )
        {
            board.set_target(target);
            board.visible = true;
            return;
        }

        board.visible = !board.visible;
    });
    button.appendChild(document.createTextNode(emoji_data[0].emoji[0].unicode));
    board.style(button);
    return button;
}

let emoji_data = {{ compiled|safe }};
