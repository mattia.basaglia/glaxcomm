import os
import urllib.request
from ..base_command import BaseEmojiCommand


class Command(BaseEmojiCommand):
    def add_extra_arguments(self, parser):
        parser.add_argument(
            "--url",
            default="http://www.unicode.org/Public/emoji/latest/emoji-test.txt",
        )

    def handle(self, url, data_file, **kw):
        with open(data_file, "wb") as out:
            out.write(urllib.request.urlopen(url).read())
