import os
import urllib.request
from ..base_command import BaseEmojiCommand
from ...data import EmojiData


class Command(BaseEmojiCommand):
    def add_extra_arguments(self, parser):
        parser.add_argument(
            "search",
            help="Search term"
        )
        parser.add_argument(
            "--group",
            "-g",
            action="store_true",
            help="Search groups",
            dest="search_groups"
        )

    def handle(self, search, search_groups, data_file, **kw):
        data = EmojiData()
        data.load(data_file)

        search = search.lower()
        if search_groups:
            for group in data.groups:
                if search in group.name.lower():
                    print(group.name)

                    for subgroup in group.children:
                        if search in subgroup.name.lower():
                            print("%s > %s" % (group.name, subgroup.name))
        else:
            for group in data.groups:
                for subgroup in group.children:
                    for emoji in subgroup.children:
                        if search in emoji.name.lower():
                            print("%s > %s: %s %s%s %s" % (
                                group.name,
                                subgroup.name,
                                emoji.points,
                                emoji.name,
                                ": " + ",".join(emoji.details) if emoji.details else "",
                                emoji.unicode
                            ))
