import os
import json
from django.template import loader
from ..base_command import BaseEmojiCommand
from ...data import EmojiData, STATIC_ROOT


class Command(BaseEmojiCommand):
    def add_extra_arguments(self, parser):
        parser.add_argument(
            "--output",
            "-o",
            default=os.path.join(STATIC_ROOT, "emoji-board.js"),
            help="Output file"
        )

    def handle(self, output, data_file, **kw):
        data = EmojiData()
        data.load(data_file)
        compiled = data.compile()

        with open(output, "w") as of:
            of.write(loader.render_to_string("emoji/emoji-board.js", {
                "compiled": json.dumps(compiled, indent=2)
            }))
