from django.core.management import BaseCommand
from ..data import DATA_FILE


class BaseEmojiCommand(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--data-file",
            default=DATA_FILE,
            help="Emoji data file",
        )

        self.add_extra_arguments(parser)

    def add_extra_arguments(self):
        raise NotImplementedError()

