class EmojiBoard
{
    constructor(element, on_click, max_version, style)
    {
        this.board_element = element;
        this.on_click = on_click;
        this.max_version = max_version;

        let group_links = this.board_element.appendChild(document.createElement("ul"));
        group_links.classList.add("emoji-group-links");

        let board_main = this.board_element.appendChild(document.createElement("div"));
        board_main.classList.add("emoji-board-main");

        this.group_links = [];
        this.group_elements = [];

        let first = null;
        for ( let group of emoji_data )
        {
            let group_link = group_links.appendChild(document.createElement("li")).appendChild(document.createElement("a"));
            group_link.addEventListener("click", this.on_header_click.bind(this));
            group_link.dataset.emojiGroup = group.name;
            group_link.title = group.name;
            group_link.appendChild(document.createTextNode(group.emoji[0].unicode));
            this.group_links.push(group_link);

            let group_element = board_main.appendChild(document.createElement("div"));
            group_element.classList.add("emoji-group");
            group_element.dataset.emojiGroup = group.name;
            group_element.appendChild(document.createElement("header")).appendChild(document.createTextNode(group.name));
            let group_emoji_container = group_element.appendChild(document.createElement("div"));
            for ( let emoji of group.emoji )
            {
                if ( this.passes_filter(emoji) )
                {
                    let emoji_div = group_emoji_container.appendChild(document.createElement("div"));
                    emoji_div.classList.add("emoji-emoji");
                    emoji_div.title = emoji.name;
                    emoji_div.dataset.emoji = emoji.unicode;
                    emoji_div.addEventListener("click", (e) => this.on_click(emoji.unicode));
                    emoji_div.appendChild(document.createTextNode(emoji.unicode));
                }
            }
            this.group_elements.push(group_element);
        }

        this.on_header_click_element(this.group_links[0]);
        this.visible = true;
        this.target = null;
        this.style = style;
        this.style(this.board_element);
    }

    set_target(target)
    {
        let element = emoji_get_element(target);
        this.on_click = (unicode) => element.value += unicode;
        this.target = element;
    }

    passes_filter(emoji)
    {
        if ( !this.max_version )
            return true;

        if ( this.max_version[0] > emoji.version[0] )
            return true;

        return this.max_version[0] == emoji.version[0] && this.max_version[1] >= emoji.version[1];
    }

    on_header_click_element(element)
    {
        for ( let group_link of this.group_links )
        {
            group_link.classList.remove("emoji-active");
        }
        element.classList.add("emoji-active");

        for ( let group of this.group_elements )
        {
            group.style.display = group.dataset.emojiGroup == element.dataset.emojiGroup ? "block" : "none";
        }
    }

    on_header_click(event)
    {
        this.on_header_click_element(event.target);
    }

    show()
    {
        this._visible = true;
        this.board_element.style.display = "flex";
    }

    hide()
    {
        this._visible = false;
        this.board_element.style.display = "none";
    }

    set visible(visible)
    {
        if ( visible )
            this.show();
        else
            this.hide();
    }

    get visible()
    {
        return this._visible;
    }

    add_target(target)
    {
        let board = this;

        let target_element = emoji_get_element(target);
        target_element.addEventListener("focus", (ev) => board.set_target(target_element));

        if ( !this.target )
            this.set_target(target_element);
    }
}

const EmojiStyle = Object.freeze({
    native: (e) => null,
    twemoji: (e) => twemoji.parse(e)
});

function emoji_get_element(element)
{
    if ( !(element instanceof HTMLElement) )
        return document.querySelector(element);
    return element;
}

function emoji_board(conf)
{
    let on_click = console.log;
    if ( "on_click" in conf )
        on_click = conf.on_click;

    let style = EmojiStyle.native;
    if ( "style" in conf )
        style = conf.style;

    let board = new EmojiBoard(emoji_get_element(conf.element), on_click, conf.max_version, style);

    if ( "target" in conf )
        board.set_target(conf.target);

    if ( "targets" in conf )
    {
        for ( let target of conf.targets )
            board.add_target(target);
    }

    if ( "visible" in conf )
        board.visible = conf.visible;

    return board;
}

function emoji_board_toggle_button(board, target=null, title="Emoji")
{
    let button = document.createElement("button");
    button.addEventListener("click", function (event){
        if ( target && target != board.target )
        {
            board.set_target(target);
            board.visible = true;
            return;
        }

        board.visible = !board.visible;
    });
    button.appendChild(document.createTextNode(emoji_data[0].emoji[0].unicode));
    board.style(button);
    return button;
}

let emoji_data = [
  {
    "name": "Smileys & Emotion",
    "emoji": [
      {
        "unicode": "\ud83d\ude00",
        "name": "grinning face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude03",
        "name": "grinning face with big eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude04",
        "name": "grinning face with smiling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude01",
        "name": "beaming face with smiling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude06",
        "name": "grinning squinting face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude05",
        "name": "grinning face with sweat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd23",
        "name": "rolling on the floor laughing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude02",
        "name": "face with tears of joy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude42",
        "name": "slightly smiling face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude43",
        "name": "upside-down face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude09",
        "name": "winking face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0a",
        "name": "smiling face with smiling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude07",
        "name": "smiling face with halo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd70",
        "name": "smiling face with hearts",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0d",
        "name": "smiling face with heart-eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd29",
        "name": "star-struck",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude18",
        "name": "face blowing a kiss",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude17",
        "name": "kissing face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u263a\ufe0f",
        "name": "smiling face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1a",
        "name": "kissing face with closed eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude19",
        "name": "kissing face with smiling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0b",
        "name": "face savoring food",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1b",
        "name": "face with tongue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1c",
        "name": "winking face with tongue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2a",
        "name": "zany face",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1d",
        "name": "squinting face with tongue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd11",
        "name": "money-mouth face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd17",
        "name": "hugging face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2d",
        "name": "face with hand over mouth",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2b",
        "name": "shushing face",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd14",
        "name": "thinking face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd10",
        "name": "zipper-mouth face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd28",
        "name": "face with raised eyebrow",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude10",
        "name": "neutral face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude11",
        "name": "expressionless face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude36",
        "name": "face without mouth",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0f",
        "name": "smirking face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude12",
        "name": "unamused face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude44",
        "name": "face with rolling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2c",
        "name": "grimacing face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd25",
        "name": "lying face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0c",
        "name": "relieved face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude14",
        "name": "pensive face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2a",
        "name": "sleepy face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd24",
        "name": "drooling face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude34",
        "name": "sleeping face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude37",
        "name": "face with medical mask",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd12",
        "name": "face with thermometer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd15",
        "name": "face with head-bandage",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd22",
        "name": "nauseated face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2e",
        "name": "face vomiting",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd27",
        "name": "sneezing face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd75",
        "name": "hot face",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd76",
        "name": "cold face",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd74",
        "name": "woozy face",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude35",
        "name": "dizzy face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2f",
        "name": "exploding head",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd20",
        "name": "cowboy hat face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd73",
        "name": "partying face",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude0e",
        "name": "smiling face with sunglasses",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd13",
        "name": "nerd face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd0",
        "name": "face with monocle",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude15",
        "name": "confused face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1f",
        "name": "worried face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude41",
        "name": "slightly frowning face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2639\ufe0f",
        "name": "frowning face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2e",
        "name": "face with open mouth",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2f",
        "name": "hushed face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude32",
        "name": "astonished face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude33",
        "name": "flushed face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7a",
        "name": "pleading face",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude26",
        "name": "frowning face with open mouth",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude27",
        "name": "anguished face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude28",
        "name": "fearful face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude30",
        "name": "anxious face with sweat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude25",
        "name": "sad but relieved face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude22",
        "name": "crying face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2d",
        "name": "loudly crying face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude31",
        "name": "face screaming in fear",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude16",
        "name": "confounded face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude23",
        "name": "persevering face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude1e",
        "name": "disappointed face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude13",
        "name": "downcast face with sweat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude29",
        "name": "weary face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude2b",
        "name": "tired face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd71",
        "name": "yawning face",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\ude24",
        "name": "face with steam from nose",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude21",
        "name": "pouting face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude20",
        "name": "angry face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd2c",
        "name": "face with symbols on mouth",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude08",
        "name": "smiling face with horns",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7f",
        "name": "angry face with horns",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc80",
        "name": "skull",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2620\ufe0f",
        "name": "skull and crossbones",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca9",
        "name": "pile of poo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd21",
        "name": "clown face",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc79",
        "name": "ogre",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7a",
        "name": "goblin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7b",
        "name": "ghost",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7d",
        "name": "alien",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7e",
        "name": "alien monster",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd16",
        "name": "robot",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3a",
        "name": "grinning cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude38",
        "name": "grinning cat with smiling eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude39",
        "name": "cat with tears of joy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3b",
        "name": "smiling cat with heart-eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3c",
        "name": "cat with wry smile",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3d",
        "name": "kissing cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude40",
        "name": "weary cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3f",
        "name": "crying cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude3e",
        "name": "pouting cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude48",
        "name": "see-no-evil monkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude49",
        "name": "hear-no-evil monkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4a",
        "name": "speak-no-evil monkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc8b",
        "name": "kiss mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc8c",
        "name": "love letter",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc98",
        "name": "heart with arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9d",
        "name": "heart with ribbon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc96",
        "name": "sparkling heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc97",
        "name": "growing heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc93",
        "name": "beating heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9e",
        "name": "revolving hearts",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc95",
        "name": "two hearts",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9f",
        "name": "heart decoration",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2763\ufe0f",
        "name": "heart exclamation",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc94",
        "name": "broken heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2764\ufe0f",
        "name": "red heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde1",
        "name": "orange heart",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9b",
        "name": "yellow heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9a",
        "name": "green heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc99",
        "name": "blue heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc9c",
        "name": "purple heart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd0e",
        "name": "brown heart",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udda4",
        "name": "black heart",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd0d",
        "name": "white heart",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udcaf",
        "name": "hundred points",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca2",
        "name": "anger symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca5",
        "name": "collision",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcab",
        "name": "dizzy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca6",
        "name": "sweat droplets",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca8",
        "name": "dashing away",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd73\ufe0f",
        "name": "hole",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca3",
        "name": "bomb",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcac",
        "name": "speech balloon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc41\ufe0f\u200d\ud83d\udde8\ufe0f",
        "name": "eye in speech bubble",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udde8\ufe0f",
        "name": "left speech bubble",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddef\ufe0f",
        "name": "right anger bubble",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcad",
        "name": "thought balloon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca4",
        "name": "zzz",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "People & Body",
    "emoji": [
      {
        "unicode": "\ud83d\udc4b",
        "name": "waving hand",
        "variants": [
          {
            "unicode": "\ud83d\udc4b\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4b\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4b\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4b\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4b\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1a",
        "name": "raised back of hand",
        "variants": [
          {
            "unicode": "\ud83e\udd1a\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1a\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1a\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1a\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1a\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd90\ufe0f",
        "name": "hand with fingers splayed",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u270b",
        "name": "raised hand",
        "variants": [
          {
            "unicode": "\u270b\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270b\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270b\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270b\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270b\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd96",
        "name": "vulcan salute",
        "variants": [
          {
            "unicode": "\ud83d\udd96\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd96\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd96\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd96\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd96\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc4c",
        "name": "OK hand",
        "variants": [
          {
            "unicode": "\ud83d\udc4c\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4c\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4c\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4c\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4c\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd0f",
        "name": "pinching hand",
        "variants": [
          {
            "unicode": "\ud83e\udd0f\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\udd0f\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\udd0f\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\udd0f\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\udd0f\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\u270c\ufe0f",
        "name": "victory hand",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1e",
        "name": "crossed fingers",
        "variants": [
          {
            "unicode": "\ud83e\udd1e\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1e\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1e\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1e\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1e\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1f",
        "name": "love-you gesture",
        "variants": [
          {
            "unicode": "\ud83e\udd1f\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1f\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1f\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1f\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1f\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd18",
        "name": "sign of the horns",
        "variants": [
          {
            "unicode": "\ud83e\udd18\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd18\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd18\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd18\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd18\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd19",
        "name": "call me hand",
        "variants": [
          {
            "unicode": "\ud83e\udd19\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd19\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd19\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd19\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd19\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc48",
        "name": "backhand index pointing left",
        "variants": [
          {
            "unicode": "\ud83d\udc48\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc48\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc48\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc48\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc48\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc49",
        "name": "backhand index pointing right",
        "variants": [
          {
            "unicode": "\ud83d\udc49\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc49\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc49\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc49\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc49\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc46",
        "name": "backhand index pointing up",
        "variants": [
          {
            "unicode": "\ud83d\udc46\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc46\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc46\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc46\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc46\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd95",
        "name": "middle finger",
        "variants": [
          {
            "unicode": "\ud83d\udd95\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd95\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd95\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd95\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd95\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc47",
        "name": "backhand index pointing down",
        "variants": [
          {
            "unicode": "\ud83d\udc47\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc47\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc47\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc47\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc47\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u261d\ufe0f",
        "name": "index pointing up",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc4d",
        "name": "thumbs up",
        "variants": [
          {
            "unicode": "\ud83d\udc4d\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4d\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4d\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4d\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4d\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc4e",
        "name": "thumbs down",
        "variants": [
          {
            "unicode": "\ud83d\udc4e\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4e\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4e\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4e\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4e\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u270a",
        "name": "raised fist",
        "variants": [
          {
            "unicode": "\u270a\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270a\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270a\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270a\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\u270a\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc4a",
        "name": "oncoming fist",
        "variants": [
          {
            "unicode": "\ud83d\udc4a\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4a\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4a\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4a\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4a\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1b",
        "name": "left-facing fist",
        "variants": [
          {
            "unicode": "\ud83e\udd1b\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1b\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1b\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1b\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1b\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1c",
        "name": "right-facing fist",
        "variants": [
          {
            "unicode": "\ud83e\udd1c\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1c\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1c\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1c\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd1c\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc4f",
        "name": "clapping hands",
        "variants": [
          {
            "unicode": "\ud83d\udc4f\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4f\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4f\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4f\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc4f\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4c",
        "name": "raising hands",
        "variants": [
          {
            "unicode": "\ud83d\ude4c\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4c\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4c\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4c\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4c\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc50",
        "name": "open hands",
        "variants": [
          {
            "unicode": "\ud83d\udc50\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc50\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc50\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc50\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc50\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd32",
        "name": "palms up together",
        "variants": [
          {
            "unicode": "\ud83e\udd32\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd32\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd32\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd32\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd32\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd1d",
        "name": "handshake",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4f",
        "name": "folded hands",
        "variants": [
          {
            "unicode": "\ud83d\ude4f\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4f\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4f\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4f\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4f\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u270d\ufe0f",
        "name": "writing hand",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc85",
        "name": "nail polish",
        "variants": [
          {
            "unicode": "\ud83d\udc85\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc85\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc85\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc85\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc85\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd33",
        "name": "selfie",
        "variants": [
          {
            "unicode": "\ud83e\udd33\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd33\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd33\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd33\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd33\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcaa",
        "name": "flexed biceps",
        "variants": [
          {
            "unicode": "\ud83d\udcaa\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udcaa\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udcaa\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udcaa\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udcaa\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddbe",
        "name": "mechanical arm",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddbf",
        "name": "mechanical leg",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddb5",
        "name": "leg",
        "variants": [
          {
            "unicode": "\ud83e\uddb5\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb5\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb5\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb5\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb5\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          }
        ],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb6",
        "name": "foot",
        "variants": [
          {
            "unicode": "\ud83e\uddb6\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb6\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb6\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb6\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb6\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          }
        ],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc42",
        "name": "ear",
        "variants": [
          {
            "unicode": "\ud83d\udc42\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc42\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc42\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc42\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc42\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddbb",
        "name": "ear with hearing aid",
        "variants": [
          {
            "unicode": "\ud83e\uddbb\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddbb\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddbb\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddbb\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddbb\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc43",
        "name": "nose",
        "variants": [
          {
            "unicode": "\ud83d\udc43\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc43\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc43\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc43\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc43\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde0",
        "name": "brain",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb7",
        "name": "tooth",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb4",
        "name": "bone",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc40",
        "name": "eyes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc41\ufe0f",
        "name": "eye",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc45",
        "name": "tongue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc44",
        "name": "mouth",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc76",
        "name": "baby",
        "variants": [
          {
            "unicode": "\ud83d\udc76\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc76\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc76\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc76\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc76\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd2",
        "name": "child",
        "variants": [
          {
            "unicode": "\ud83e\uddd2\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd2\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd2\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd2\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd2\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc66",
        "name": "boy",
        "variants": [
          {
            "unicode": "\ud83d\udc66\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc66\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc66\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc66\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc66\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc67",
        "name": "girl",
        "variants": [
          {
            "unicode": "\ud83d\udc67\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc67\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc67\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc67\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc67\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1",
        "name": "person",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71",
            "name": " blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffb",
            "name": " light skin tone, blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffc",
            "name": " medium-light skin tone, blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffd",
            "name": " medium skin tone, blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffe",
            "name": " medium-dark skin tone, blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udfff",
            "name": " dark skin tone, blond hair",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd1\u200d\ud83e\uddb0",
            "name": " red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddb0",
            "name": " light skin tone, red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddb0",
            "name": " medium-light skin tone, red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddb0",
            "name": " medium skin tone, red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddb0",
            "name": " medium-dark skin tone, red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddb0",
            "name": " dark skin tone, red hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\u200d\ud83e\uddb1",
            "name": " curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddb1",
            "name": " light skin tone, curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddb1",
            "name": " medium-light skin tone, curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddb1",
            "name": " medium skin tone, curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddb1",
            "name": " medium-dark skin tone, curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddb1",
            "name": " dark skin tone, curly hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\u200d\ud83e\uddb3",
            "name": " white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddb3",
            "name": " light skin tone, white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddb3",
            "name": " medium-light skin tone, white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddb3",
            "name": " medium skin tone, white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddb3",
            "name": " medium-dark skin tone, white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddb3",
            "name": " dark skin tone, white hair",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\u200d\ud83e\uddb2",
            "name": " bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddb2",
            "name": " light skin tone, bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddb2",
            "name": " medium-light skin tone, bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddb2",
            "name": " medium skin tone, bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddb2",
            "name": " medium-dark skin tone, bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddb2",
            "name": " dark skin tone, bald",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc68",
        "name": "man",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4",
            "name": " beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4\ud83c\udffb",
            "name": " light skin tone, beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4\ud83c\udffc",
            "name": " medium-light skin tone, beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4\ud83c\udffd",
            "name": " medium skin tone, beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4\ud83c\udffe",
            "name": " medium-dark skin tone, beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd4\ud83c\udfff",
            "name": " dark skin tone, beard",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83e\uddb0",
            "name": " red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddb0",
            "name": " light skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddb0",
            "name": " medium-light skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddb0",
            "name": " medium skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddb0",
            "name": " medium-dark skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddb0",
            "name": " dark skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83e\uddb1",
            "name": " curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddb1",
            "name": " light skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddb1",
            "name": " medium-light skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddb1",
            "name": " medium skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddb1",
            "name": " medium-dark skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddb1",
            "name": " dark skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83e\uddb3",
            "name": " white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddb3",
            "name": " light skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddb3",
            "name": " medium-light skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddb3",
            "name": " medium skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddb3",
            "name": " medium-dark skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddb3",
            "name": " dark skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83e\uddb2",
            "name": " bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddb2",
            "name": " light skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddb2",
            "name": " medium-light skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddb2",
            "name": " medium skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddb2",
            "name": " medium-dark skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddb2",
            "name": " dark skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\u200d\u2642\ufe0f",
            "name": " blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\u200d\u2642",
            "name": " blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffb\u200d\u2642\ufe0f",
            "name": " light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffb\u200d\u2642",
            "name": " light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffc\u200d\u2642\ufe0f",
            "name": " medium-light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffc\u200d\u2642",
            "name": " medium-light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffd\u200d\u2642\ufe0f",
            "name": " medium skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffd\u200d\u2642",
            "name": " medium skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffe\u200d\u2642\ufe0f",
            "name": " medium-dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffe\u200d\u2642",
            "name": " medium-dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udfff\u200d\u2642\ufe0f",
            "name": " dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udfff\u200d\u2642",
            "name": " dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69",
        "name": "woman",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83e\uddb0",
            "name": " red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddb0",
            "name": " light skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddb0",
            "name": " medium-light skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddb0",
            "name": " medium skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddb0",
            "name": " medium-dark skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddb0",
            "name": " dark skin tone, red hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83e\uddb1",
            "name": " curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddb1",
            "name": " light skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddb1",
            "name": " medium-light skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddb1",
            "name": " medium skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddb1",
            "name": " medium-dark skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddb1",
            "name": " dark skin tone, curly hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83e\uddb3",
            "name": " white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddb3",
            "name": " light skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddb3",
            "name": " medium-light skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddb3",
            "name": " medium skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddb3",
            "name": " medium-dark skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddb3",
            "name": " dark skin tone, white hair",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83e\uddb2",
            "name": " bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddb2",
            "name": " light skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddb2",
            "name": " medium-light skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddb2",
            "name": " medium skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddb2",
            "name": " medium-dark skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddb2",
            "name": " dark skin tone, bald",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\u200d\u2640\ufe0f",
            "name": " blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\u200d\u2640",
            "name": " blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffb\u200d\u2640\ufe0f",
            "name": " light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffb\u200d\u2640",
            "name": " light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffc\u200d\u2640\ufe0f",
            "name": " medium-light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffc\u200d\u2640",
            "name": " medium-light skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffd\u200d\u2640\ufe0f",
            "name": " medium skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffd\u200d\u2640",
            "name": " medium skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffe\u200d\u2640\ufe0f",
            "name": " medium-dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udffe\u200d\u2640",
            "name": " medium-dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udfff\u200d\u2640\ufe0f",
            "name": " dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc71\ud83c\udfff\u200d\u2640",
            "name": " dark skin tone, blond hair",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd3",
        "name": "older person",
        "variants": [
          {
            "unicode": "\ud83e\uddd3\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd3\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd3\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd3\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd3\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc74",
        "name": "old man",
        "variants": [
          {
            "unicode": "\ud83d\udc74\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc74\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc74\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc74\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc74\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc75",
        "name": "old woman",
        "variants": [
          {
            "unicode": "\ud83d\udc75\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc75\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc75\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc75\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc75\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4d",
        "name": "person frowning",
        "variants": [
          {
            "unicode": "\ud83d\ude4d\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4d\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4d\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4d\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4d\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4d\u200d\u2642\ufe0f",
        "name": "man frowning",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4d\u200d\u2640\ufe0f",
        "name": "woman frowning",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4e",
        "name": "person pouting",
        "variants": [
          {
            "unicode": "\ud83d\ude4e\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4e\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4e\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4e\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4e\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4e\u200d\u2642\ufe0f",
        "name": "man pouting",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4e\u200d\u2640\ufe0f",
        "name": "woman pouting",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude45",
        "name": "person gesturing NO",
        "variants": [
          {
            "unicode": "\ud83d\ude45\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude45\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude45\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude45\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude45\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude45\u200d\u2642\ufe0f",
        "name": "man gesturing NO",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude45\u200d\u2640\ufe0f",
        "name": "woman gesturing NO",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude46",
        "name": "person gesturing OK",
        "variants": [
          {
            "unicode": "\ud83d\ude46\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude46\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude46\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude46\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude46\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude46\u200d\u2642\ufe0f",
        "name": "man gesturing OK",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude46\u200d\u2640\ufe0f",
        "name": "woman gesturing OK",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc81",
        "name": "person tipping hand",
        "variants": [
          {
            "unicode": "\ud83d\udc81\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc81\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc81\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc81\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc81\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc81\u200d\u2642\ufe0f",
        "name": "man tipping hand",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc81\u200d\u2640\ufe0f",
        "name": "woman tipping hand",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4b",
        "name": "person raising hand",
        "variants": [
          {
            "unicode": "\ud83d\ude4b\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4b\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4b\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4b\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude4b\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4b\u200d\u2642\ufe0f",
        "name": "man raising hand",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude4b\u200d\u2640\ufe0f",
        "name": "woman raising hand",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddcf",
        "name": "deaf person",
        "variants": [
          {
            "unicode": "\ud83e\uddcf\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcf\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcf\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcf\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcf\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddcf\u200d\u2642\ufe0f",
        "name": "deaf man",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddcf\u200d\u2640\ufe0f",
        "name": "deaf woman",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\ude47",
        "name": "person bowing",
        "variants": [
          {
            "unicode": "\ud83d\ude47\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude47\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude47\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude47\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\ude47\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude47\u200d\u2642\ufe0f",
        "name": "man bowing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude47\u200d\u2640\ufe0f",
        "name": "woman bowing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd26",
        "name": "person facepalming",
        "variants": [
          {
            "unicode": "\ud83e\udd26\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd26\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd26\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd26\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd26\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd26\u200d\u2642\ufe0f",
        "name": "man facepalming",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd26\u200d\u2640\ufe0f",
        "name": "woman facepalming",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd37",
        "name": "person shrugging",
        "variants": [
          {
            "unicode": "\ud83e\udd37\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd37\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd37\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd37\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd37\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd37\u200d\u2642\ufe0f",
        "name": "man shrugging",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd37\u200d\u2640\ufe0f",
        "name": "woman shrugging",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\u2695\ufe0f",
        "name": "health worker",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\u2695\ufe0f",
        "name": "man health worker",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\u2695\ufe0f",
        "name": "woman health worker",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udf93",
        "name": "student",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udf93",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udf93",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udf93",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udf93",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udf93",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udf93",
        "name": "man student",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udf93",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udf93",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udf93",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udf93",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udf93",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udf93",
        "name": "woman student",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udf93",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udf93",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udf93",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udf93",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udf93",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udfeb",
        "name": "teacher",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udfeb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udfeb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udfeb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udfeb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udfeb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udfeb",
        "name": "man teacher",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udfeb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udfeb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udfeb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udfeb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udfeb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udfeb",
        "name": "woman teacher",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udfeb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udfeb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udfeb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udfeb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udfeb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\u2696\ufe0f",
        "name": "judge",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\u2696\ufe0f",
        "name": "man judge",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\u2696\ufe0f",
        "name": "woman judge",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udf3e",
        "name": "farmer",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udf3e",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udf3e",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udf3e",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udf3e",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udf3e",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udf3e",
        "name": "man farmer",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udf3e",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udf3e",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udf3e",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udf3e",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udf3e",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udf3e",
        "name": "woman farmer",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udf3e",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udf3e",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udf3e",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udf3e",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udf3e",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udf73",
        "name": "cook",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udf73",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udf73",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udf73",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udf73",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udf73",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udf73",
        "name": "man cook",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udf73",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udf73",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udf73",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udf73",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udf73",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udf73",
        "name": "woman cook",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udf73",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udf73",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udf73",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udf73",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udf73",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\udd27",
        "name": "mechanic",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\udd27",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\udd27",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\udd27",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\udd27",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\udd27",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\udd27",
        "name": "man mechanic",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\udd27",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\udd27",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\udd27",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\udd27",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\udd27",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\udd27",
        "name": "woman mechanic",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\udd27",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\udd27",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\udd27",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\udd27",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\udd27",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udfed",
        "name": "factory worker",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udfed",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udfed",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udfed",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udfed",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udfed",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udfed",
        "name": "man factory worker",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udfed",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udfed",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udfed",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udfed",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udfed",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udfed",
        "name": "woman factory worker",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udfed",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udfed",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udfed",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udfed",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udfed",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\udcbc",
        "name": "office worker",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\udcbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\udcbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\udcbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\udcbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\udcbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\udcbc",
        "name": "man office worker",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\udcbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\udcbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\udcbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\udcbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\udcbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\udcbc",
        "name": "woman office worker",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\udcbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\udcbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\udcbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\udcbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\udcbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\udd2c",
        "name": "scientist",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\udd2c",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\udd2c",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\udd2c",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\udd2c",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\udd2c",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\udd2c",
        "name": "man scientist",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\udd2c",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\udd2c",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\udd2c",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\udd2c",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\udd2c",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\udd2c",
        "name": "woman scientist",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\udd2c",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\udd2c",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\udd2c",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\udd2c",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\udd2c",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\udcbb",
        "name": "technologist",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\udcbb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\udcbb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\udcbb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\udcbb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\udcbb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\udcbb",
        "name": "man technologist",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\udcbb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\udcbb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\udcbb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\udcbb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\udcbb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\udcbb",
        "name": "woman technologist",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\udcbb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\udcbb",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\udcbb",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\udcbb",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\udcbb",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udfa4",
        "name": "singer",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udfa4",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udfa4",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udfa4",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udfa4",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udfa4",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udfa4",
        "name": "man singer",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udfa4",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udfa4",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udfa4",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udfa4",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udfa4",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udfa4",
        "name": "woman singer",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udfa4",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udfa4",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udfa4",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udfa4",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udfa4",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83c\udfa8",
        "name": "artist",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83c\udfa8",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83c\udfa8",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83c\udfa8",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83c\udfa8",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83c\udfa8",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83c\udfa8",
        "name": "man artist",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83c\udfa8",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83c\udfa8",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83c\udfa8",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83c\udfa8",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83c\udfa8",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83c\udfa8",
        "name": "woman artist",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83c\udfa8",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83c\udfa8",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83c\udfa8",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83c\udfa8",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83c\udfa8",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\u2708\ufe0f",
        "name": "pilot",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\u2708\ufe0f",
        "name": "man pilot",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\u2708\ufe0f",
        "name": "woman pilot",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\ude80",
        "name": "astronaut",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\ude80",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\ude80",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\ude80",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\ude80",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\ude80",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\ude80",
        "name": "man astronaut",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\ude80",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\ude80",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\ude80",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\ude80",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\ude80",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\ude80",
        "name": "woman astronaut",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\ude80",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\ude80",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\ude80",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\ude80",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\ude80",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83d\ude92",
        "name": "firefighter",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83d\ude92",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83d\ude92",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83d\ude92",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83d\ude92",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83d\ude92",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83d\ude92",
        "name": "man firefighter",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83d\ude92",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83d\ude92",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83d\ude92",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83d\ude92",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83d\ude92",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83d\ude92",
        "name": "woman firefighter",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83d\ude92",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83d\ude92",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83d\ude92",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83d\ude92",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83d\ude92",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6e",
        "name": "police officer",
        "variants": [
          {
            "unicode": "\ud83d\udc6e\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc6e\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc6e\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc6e\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc6e\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6e\u200d\u2642\ufe0f",
        "name": "man police officer",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6e\u200d\u2640\ufe0f",
        "name": "woman police officer",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd75\ufe0f",
        "name": "detective",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd75\ufe0f\u200d\u2642\ufe0f",
        "name": "man detective",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd75\ufe0f\u200d\u2640\ufe0f",
        "name": "woman detective",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc82",
        "name": "guard",
        "variants": [
          {
            "unicode": "\ud83d\udc82\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc82\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc82\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc82\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc82\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc82\u200d\u2642\ufe0f",
        "name": "man guard",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc82\u200d\u2640\ufe0f",
        "name": "woman guard",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc77",
        "name": "construction worker",
        "variants": [
          {
            "unicode": "\ud83d\udc77\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc77\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc77\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc77\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc77\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc77\u200d\u2642\ufe0f",
        "name": "man construction worker",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc77\u200d\u2640\ufe0f",
        "name": "woman construction worker",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd34",
        "name": "prince",
        "variants": [
          {
            "unicode": "\ud83e\udd34\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd34\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd34\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd34\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd34\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc78",
        "name": "princess",
        "variants": [
          {
            "unicode": "\ud83d\udc78\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc78\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc78\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc78\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc78\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc73",
        "name": "person wearing turban",
        "variants": [
          {
            "unicode": "\ud83d\udc73\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc73\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc73\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc73\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc73\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc73\u200d\u2642\ufe0f",
        "name": "man wearing turban",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc73\u200d\u2640\ufe0f",
        "name": "woman wearing turban",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc72",
        "name": "man with skullcap",
        "variants": [
          {
            "unicode": "\ud83d\udc72\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc72\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc72\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc72\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc72\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd5",
        "name": "woman with headscarf",
        "variants": [
          {
            "unicode": "\ud83e\uddd5\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd5\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd5\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd5\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd5\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd35",
        "name": "man in tuxedo",
        "variants": [
          {
            "unicode": "\ud83e\udd35\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd35\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd35\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd35\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd35\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc70",
        "name": "bride with veil",
        "variants": [
          {
            "unicode": "\ud83d\udc70\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc70\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc70\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc70\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc70\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd30",
        "name": "pregnant woman",
        "variants": [
          {
            "unicode": "\ud83e\udd30\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd30\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd30\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd30\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd30\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd31",
        "name": "breast-feeding",
        "variants": [
          {
            "unicode": "\ud83e\udd31\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd31\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd31\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd31\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd31\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc7c",
        "name": "baby angel",
        "variants": [
          {
            "unicode": "\ud83d\udc7c\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc7c\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc7c\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc7c\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc7c\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf85",
        "name": "Santa Claus",
        "variants": [
          {
            "unicode": "\ud83c\udf85\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udf85\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udf85\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udf85\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udf85\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd36",
        "name": "Mrs. Claus",
        "variants": [
          {
            "unicode": "\ud83e\udd36\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd36\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd36\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd36\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd36\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb8",
        "name": "superhero",
        "variants": [
          {
            "unicode": "\ud83e\uddb8\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb8\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb8\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb8\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb8\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          }
        ],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb8\u200d\u2642\ufe0f",
        "name": "man superhero",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb8\u200d\u2640\ufe0f",
        "name": "woman superhero",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb9",
        "name": "supervillain",
        "variants": [
          {
            "unicode": "\ud83e\uddb9\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb9\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb9\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb9\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddb9\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              11,
              0
            ]
          }
        ],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb9\u200d\u2642\ufe0f",
        "name": "man supervillain",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddb9\u200d\u2640\ufe0f",
        "name": "woman supervillain",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd9",
        "name": "mage",
        "variants": [
          {
            "unicode": "\ud83e\uddd9\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd9\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd9\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd9\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd9\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd9\u200d\u2642\ufe0f",
        "name": "man mage",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd9\u200d\u2640\ufe0f",
        "name": "woman mage",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddda",
        "name": "fairy",
        "variants": [
          {
            "unicode": "\ud83e\uddda\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddda\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddda\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddda\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddda\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddda\u200d\u2642\ufe0f",
        "name": "man fairy",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddda\u200d\u2640\ufe0f",
        "name": "woman fairy",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddb",
        "name": "vampire",
        "variants": [
          {
            "unicode": "\ud83e\udddb\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddb\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddb\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddb\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddb\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddb\u200d\u2642\ufe0f",
        "name": "man vampire",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddb\u200d\u2640\ufe0f",
        "name": "woman vampire",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddc",
        "name": "merperson",
        "variants": [
          {
            "unicode": "\ud83e\udddc\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddc\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddc\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddc\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddc\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddc\u200d\u2642\ufe0f",
        "name": "merman",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddc\u200d\u2640\ufe0f",
        "name": "mermaid",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddd",
        "name": "elf",
        "variants": [
          {
            "unicode": "\ud83e\udddd\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddd\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddd\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddd\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\udddd\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddd\u200d\u2642\ufe0f",
        "name": "man elf",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddd\u200d\u2640\ufe0f",
        "name": "woman elf",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddde",
        "name": "genie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddde\u200d\u2642\ufe0f",
        "name": "man genie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddde\u200d\u2640\ufe0f",
        "name": "woman genie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddf",
        "name": "zombie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddf\u200d\u2642\ufe0f",
        "name": "man zombie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udddf\u200d\u2640\ufe0f",
        "name": "woman zombie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc86",
        "name": "person getting massage",
        "variants": [
          {
            "unicode": "\ud83d\udc86\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc86\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc86\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc86\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc86\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc86\u200d\u2642\ufe0f",
        "name": "man getting massage",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc86\u200d\u2640\ufe0f",
        "name": "woman getting massage",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc87",
        "name": "person getting haircut",
        "variants": [
          {
            "unicode": "\ud83d\udc87\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc87\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc87\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc87\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc87\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc87\u200d\u2642\ufe0f",
        "name": "man getting haircut",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc87\u200d\u2640\ufe0f",
        "name": "woman getting haircut",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb6",
        "name": "person walking",
        "variants": [
          {
            "unicode": "\ud83d\udeb6\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb6\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb6\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb6\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb6\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb6\u200d\u2642\ufe0f",
        "name": "man walking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb6\u200d\u2640\ufe0f",
        "name": "woman walking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddcd",
        "name": "person standing",
        "variants": [
          {
            "unicode": "\ud83e\uddcd\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcd\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcd\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcd\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddcd\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddcd\u200d\u2642\ufe0f",
        "name": "man standing",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddcd\u200d\u2640\ufe0f",
        "name": "woman standing",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddce",
        "name": "person kneeling",
        "variants": [
          {
            "unicode": "\ud83e\uddce\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddce\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddce\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddce\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddce\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddce\u200d\u2642\ufe0f",
        "name": "man kneeling",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddce\u200d\u2640\ufe0f",
        "name": "woman kneeling",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83e\uddaf",
        "name": "person with probing cane",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddaf",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddaf",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddaf",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddaf",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddaf",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83e\uddaf",
        "name": "man with probing cane",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddaf",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddaf",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddaf",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddaf",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddaf",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83e\uddaf",
        "name": "woman with probing cane",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddaf",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddaf",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddaf",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddaf",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddaf",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83e\uddbc",
        "name": "person in motorized wheelchair",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83e\uddbc",
        "name": "man in motorized wheelchair",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83e\uddbc",
        "name": "woman in motorized wheelchair",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddbc",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddbc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddbc",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddbc",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddbc",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83e\uddbd",
        "name": "person in manual wheelchair",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\uddbd",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\uddbd",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\uddbd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\uddbd",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\uddbd",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc68\u200d\ud83e\uddbd",
        "name": "man in manual wheelchair",
        "variants": [
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\uddbd",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\uddbd",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\uddbd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\uddbd",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\uddbd",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc69\u200d\ud83e\uddbd",
        "name": "woman in manual wheelchair",
        "variants": [
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\uddbd",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\uddbd",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\uddbd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\uddbd",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\uddbd",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83c\udfc3",
        "name": "person running",
        "variants": [
          {
            "unicode": "\ud83c\udfc3\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc3\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc3\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc3\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc3\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc3\u200d\u2642\ufe0f",
        "name": "man running",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc3\u200d\u2640\ufe0f",
        "name": "woman running",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc83",
        "name": "woman dancing",
        "variants": [
          {
            "unicode": "\ud83d\udc83\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc83\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc83\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc83\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc83\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd7a",
        "name": "man dancing",
        "variants": [
          {
            "unicode": "\ud83d\udd7a\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd7a\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd7a\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd7a\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udd7a\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd74\ufe0f",
        "name": "man in suit levitating",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6f",
        "name": "people with bunny ears",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6f\u200d\u2642\ufe0f",
        "name": "men with bunny ears",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6f\u200d\u2640\ufe0f",
        "name": "women with bunny ears",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd6",
        "name": "person in steamy room",
        "variants": [
          {
            "unicode": "\ud83e\uddd6\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd6\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd6\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd6\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd6\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd6\u200d\u2642\ufe0f",
        "name": "man in steamy room",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd6\u200d\u2640\ufe0f",
        "name": "woman in steamy room",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd7",
        "name": "person climbing",
        "variants": [
          {
            "unicode": "\ud83e\uddd7\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd7\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd7\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd7\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd7\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd7\u200d\u2642\ufe0f",
        "name": "man climbing",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd7\u200d\u2640\ufe0f",
        "name": "woman climbing",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3a",
        "name": "person fencing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc7",
        "name": "horse racing",
        "variants": [
          {
            "unicode": "\ud83c\udfc7\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc7\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc7\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc7\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc7\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f7\ufe0f",
        "name": "skier",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc2",
        "name": "snowboarder",
        "variants": [
          {
            "unicode": "\ud83c\udfc2\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc2\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc2\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc2\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc2\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcc\ufe0f",
        "name": "person golfing",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcc\ufe0f\u200d\u2642\ufe0f",
        "name": "man golfing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcc\ufe0f\u200d\u2640\ufe0f",
        "name": "woman golfing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc4",
        "name": "person surfing",
        "variants": [
          {
            "unicode": "\ud83c\udfc4\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc4\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc4\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc4\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfc4\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc4\u200d\u2642\ufe0f",
        "name": "man surfing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc4\u200d\u2640\ufe0f",
        "name": "woman surfing",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea3",
        "name": "person rowing boat",
        "variants": [
          {
            "unicode": "\ud83d\udea3\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udea3\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udea3\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udea3\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udea3\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea3\u200d\u2642\ufe0f",
        "name": "man rowing boat",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea3\u200d\u2640\ufe0f",
        "name": "woman rowing boat",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfca",
        "name": "person swimming",
        "variants": [
          {
            "unicode": "\ud83c\udfca\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfca\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfca\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfca\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83c\udfca\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfca\u200d\u2642\ufe0f",
        "name": "man swimming",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfca\u200d\u2640\ufe0f",
        "name": "woman swimming",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u26f9\ufe0f",
        "name": "person bouncing ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f9\ufe0f\u200d\u2642\ufe0f",
        "name": "man bouncing ball",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u26f9\ufe0f\u200d\u2640\ufe0f",
        "name": "woman bouncing ball",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcb\ufe0f",
        "name": "person lifting weights",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcb\ufe0f\u200d\u2642\ufe0f",
        "name": "man lifting weights",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcb\ufe0f\u200d\u2640\ufe0f",
        "name": "woman lifting weights",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb4",
        "name": "person biking",
        "variants": [
          {
            "unicode": "\ud83d\udeb4\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb4\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb4\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb4\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb4\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb4\u200d\u2642\ufe0f",
        "name": "man biking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb4\u200d\u2640\ufe0f",
        "name": "woman biking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb5",
        "name": "person mountain biking",
        "variants": [
          {
            "unicode": "\ud83d\udeb5\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb5\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb5\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb5\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udeb5\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb5\u200d\u2642\ufe0f",
        "name": "man mountain biking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb5\u200d\u2640\ufe0f",
        "name": "woman mountain biking",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd38",
        "name": "person cartwheeling",
        "variants": [
          {
            "unicode": "\ud83e\udd38\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd38\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd38\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd38\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd38\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd38\u200d\u2642\ufe0f",
        "name": "man cartwheeling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd38\u200d\u2640\ufe0f",
        "name": "woman cartwheeling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3c",
        "name": "people wrestling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3c\u200d\u2642\ufe0f",
        "name": "men wrestling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3c\u200d\u2640\ufe0f",
        "name": "women wrestling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3d",
        "name": "person playing water polo",
        "variants": [
          {
            "unicode": "\ud83e\udd3d\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3d\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3d\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3d\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3d\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3d\u200d\u2642\ufe0f",
        "name": "man playing water polo",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3d\u200d\u2640\ufe0f",
        "name": "woman playing water polo",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3e",
        "name": "person playing handball",
        "variants": [
          {
            "unicode": "\ud83e\udd3e\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3e\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3e\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3e\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd3e\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3e\u200d\u2642\ufe0f",
        "name": "man playing handball",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3e\u200d\u2640\ufe0f",
        "name": "woman playing handball",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd39",
        "name": "person juggling",
        "variants": [
          {
            "unicode": "\ud83e\udd39\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd39\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd39\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd39\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83e\udd39\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd39\u200d\u2642\ufe0f",
        "name": "man juggling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd39\u200d\u2640\ufe0f",
        "name": "woman juggling",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd8",
        "name": "person in lotus position",
        "variants": [
          {
            "unicode": "\ud83e\uddd8\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd8\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd8\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd8\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          },
          {
            "unicode": "\ud83e\uddd8\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              5,
              0
            ]
          }
        ],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd8\u200d\u2642\ufe0f",
        "name": "man in lotus position",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd8\u200d\u2640\ufe0f",
        "name": "woman in lotus position",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec0",
        "name": "person taking bath",
        "variants": [
          {
            "unicode": "\ud83d\udec0\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udec0\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udec0\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udec0\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udec0\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udecc",
        "name": "person in bed",
        "variants": [
          {
            "unicode": "\ud83d\udecc\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udecc\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udecc\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udecc\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udecc\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddd1\u200d\ud83e\udd1d\u200d\ud83e\uddd1",
        "name": "people holding hands",
        "variants": [
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffc",
            "name": " light skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffd",
            "name": " light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffe",
            "name": " light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udfff",
            "name": " light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffb",
            "name": " medium-light skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffd",
            "name": " medium-light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffe",
            "name": " medium-light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udfff",
            "name": " medium-light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffb",
            "name": " medium skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffc",
            "name": " medium skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffe",
            "name": " medium skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udfff",
            "name": " medium skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffb",
            "name": " medium-dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffc",
            "name": " medium-dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffd",
            "name": " medium-dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udfff",
            "name": " medium-dark skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffb",
            "name": " dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffc",
            "name": " dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffd",
            "name": " dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udffe",
            "name": " dark skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83e\uddd1\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83e\uddd1\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc6d",
        "name": "women holding hands",
        "variants": [
          {
            "unicode": "\ud83d\udc6d\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffc",
            "name": " light skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffd",
            "name": " light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffe",
            "name": " light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udfff",
            "name": " light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffb",
            "name": " medium-light skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6d\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffd",
            "name": " medium-light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffe",
            "name": " medium-light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udfff",
            "name": " medium-light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffb",
            "name": " medium skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffc",
            "name": " medium skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6d\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffe",
            "name": " medium skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udfff",
            "name": " medium skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffb",
            "name": " medium-dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffc",
            "name": " medium-dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffd",
            "name": " medium-dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6d\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udfff",
            "name": " medium-dark skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffb",
            "name": " dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffc",
            "name": " dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffd",
            "name": " dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc69\ud83c\udffe",
            "name": " dark skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6d\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6b",
        "name": "woman and man holding hands",
        "variants": [
          {
            "unicode": "\ud83d\udc6b\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " light skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium-light skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6b\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " medium-light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " medium-light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium-light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " medium skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6b\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " medium skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium-dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " medium-dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " medium-dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6b\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium-dark skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc69\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " dark skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6b\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6c",
        "name": "men holding hands",
        "variants": [
          {
            "unicode": "\ud83d\udc6c\ud83c\udffb",
            "name": " light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " light skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffb\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium-light skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6c\ud83c\udffc",
            "name": " medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " medium-light skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " medium-light skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffc\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium-light skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " medium skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6c\ud83c\udffd",
            "name": " medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " medium skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffd\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " medium-dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " medium-dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " medium-dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6c\ud83c\udffe",
            "name": " medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udffe\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udfff",
            "name": " medium-dark skin tone, dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffb",
            "name": " dark skin tone, light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffc",
            "name": " dark skin tone, medium-light skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffd",
            "name": " dark skin tone, medium skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc68\ud83c\udfff\u200d\ud83e\udd1d\u200d\ud83d\udc68\ud83c\udffe",
            "name": " dark skin tone, medium-dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          },
          {
            "unicode": "\ud83d\udc6c\ud83c\udfff",
            "name": " dark skin tone",
            "variants": [],
            "version": [
              12,
              1
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc8f",
        "name": "kiss",
        "variants": [
          {
            "unicode": "\ud83d\udc69\u200d\u2764\ufe0f\u200d\ud83d\udc8b\u200d\ud83d\udc68",
            "name": " woman, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\u200d\ud83d\udc8b\u200d\ud83d\udc68",
            "name": " woman, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\u2764\ufe0f\u200d\ud83d\udc8b\u200d\ud83d\udc68",
            "name": " man, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\u2764\u200d\ud83d\udc8b\u200d\ud83d\udc68",
            "name": " man, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\ufe0f\u200d\ud83d\udc8b\u200d\ud83d\udc69",
            "name": " woman, woman",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\u200d\ud83d\udc8b\u200d\ud83d\udc69",
            "name": " woman, woman",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc91",
        "name": "couple with heart",
        "variants": [
          {
            "unicode": "\ud83d\udc69\u200d\u2764\ufe0f\u200d\ud83d\udc68",
            "name": " woman, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\u200d\ud83d\udc68",
            "name": " woman, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\u2764\ufe0f\u200d\ud83d\udc68",
            "name": " man, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\u2764\u200d\ud83d\udc68",
            "name": " man, man",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\ufe0f\u200d\ud83d\udc69",
            "name": " woman, woman",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\u2764\u200d\ud83d\udc69",
            "name": " woman, woman",
            "variants": [],
            "version": [
              2,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc6a",
        "name": "family",
        "variants": [
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc69\u200d\ud83d\udc66",
            "name": " man, woman, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc69\u200d\ud83d\udc67",
            "name": " man, woman, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc66",
            "name": " man, woman, girl, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc69\u200d\ud83d\udc66\u200d\ud83d\udc66",
            "name": " man, woman, boy, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc67",
            "name": " man, woman, girl, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc68\u200d\ud83d\udc66",
            "name": " man, man, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc68\u200d\ud83d\udc67",
            "name": " man, man, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc68\u200d\ud83d\udc67\u200d\ud83d\udc66",
            "name": " man, man, girl, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc68\u200d\ud83d\udc66\u200d\ud83d\udc66",
            "name": " man, man, boy, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc68\u200d\ud83d\udc67\u200d\ud83d\udc67",
            "name": " man, man, girl, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc69\u200d\ud83d\udc66",
            "name": " woman, woman, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc69\u200d\ud83d\udc67",
            "name": " woman, woman, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc66",
            "name": " woman, woman, girl, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc69\u200d\ud83d\udc66\u200d\ud83d\udc66",
            "name": " woman, woman, boy, boy",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc67",
            "name": " woman, woman, girl, girl",
            "variants": [],
            "version": [
              2,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc66",
            "name": " man, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc66\u200d\ud83d\udc66",
            "name": " man, boy, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc67",
            "name": " man, girl",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc67\u200d\ud83d\udc66",
            "name": " man, girl, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc68\u200d\ud83d\udc67\u200d\ud83d\udc67",
            "name": " man, girl, girl",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc66",
            "name": " woman, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc66\u200d\ud83d\udc66",
            "name": " woman, boy, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc67",
            "name": " woman, girl",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc66",
            "name": " woman, girl, boy",
            "variants": [],
            "version": [
              4,
              0
            ]
          },
          {
            "unicode": "\ud83d\udc69\u200d\ud83d\udc67\u200d\ud83d\udc67",
            "name": " woman, girl, girl",
            "variants": [],
            "version": [
              4,
              0
            ]
          }
        ],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udde3\ufe0f",
        "name": "speaking head",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc64",
        "name": "bust in silhouette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc65",
        "name": "busts in silhouette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc63",
        "name": "footprints",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Animals & Nature",
    "emoji": [
      {
        "unicode": "\ud83d\udc35",
        "name": "monkey face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc12",
        "name": "monkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8d",
        "name": "gorilla",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda7",
        "name": "orangutan",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc36",
        "name": "dog face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc15",
        "name": "dog",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddae",
        "name": "guide dog",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc15\u200d\ud83e\uddba",
        "name": "service dog",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc29",
        "name": "poodle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3a",
        "name": "wolf",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8a",
        "name": "fox",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd9d",
        "name": "raccoon",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc31",
        "name": "cat face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc08",
        "name": "cat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd81",
        "name": "lion",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2f",
        "name": "tiger face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc05",
        "name": "tiger",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc06",
        "name": "leopard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc34",
        "name": "horse face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0e",
        "name": "horse",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd84",
        "name": "unicorn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd93",
        "name": "zebra",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8c",
        "name": "deer",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2e",
        "name": "cow face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc02",
        "name": "ox",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc03",
        "name": "water buffalo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc04",
        "name": "cow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc37",
        "name": "pig face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc16",
        "name": "pig",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc17",
        "name": "boar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3d",
        "name": "pig nose",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0f",
        "name": "ram",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc11",
        "name": "ewe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc10",
        "name": "goat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2a",
        "name": "camel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2b",
        "name": "two-hump camel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd99",
        "name": "llama",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd92",
        "name": "giraffe",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc18",
        "name": "elephant",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8f",
        "name": "rhinoceros",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd9b",
        "name": "hippopotamus",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2d",
        "name": "mouse face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc01",
        "name": "mouse",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc00",
        "name": "rat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc39",
        "name": "hamster",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc30",
        "name": "rabbit face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc07",
        "name": "rabbit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3f\ufe0f",
        "name": "chipmunk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd94",
        "name": "hedgehog",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd87",
        "name": "bat",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3b",
        "name": "bear",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc28",
        "name": "koala",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3c",
        "name": "panda",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda5",
        "name": "sloth",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udda6",
        "name": "otter",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udda8",
        "name": "skunk",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udd98",
        "name": "kangaroo",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda1",
        "name": "badger",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc3e",
        "name": "paw prints",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd83",
        "name": "turkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc14",
        "name": "chicken",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc13",
        "name": "rooster",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc23",
        "name": "hatching chick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc24",
        "name": "baby chick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc25",
        "name": "front-facing baby chick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc26",
        "name": "bird",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc27",
        "name": "penguin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd4a\ufe0f",
        "name": "dove",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd85",
        "name": "eagle",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd86",
        "name": "duck",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda2",
        "name": "swan",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd89",
        "name": "owl",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda9",
        "name": "flamingo",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udd9a",
        "name": "peacock",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd9c",
        "name": "parrot",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc38",
        "name": "frog",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0a",
        "name": "crocodile",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc22",
        "name": "turtle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8e",
        "name": "lizard",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0d",
        "name": "snake",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc32",
        "name": "dragon face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc09",
        "name": "dragon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd95",
        "name": "sauropod",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd96",
        "name": "T-Rex",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc33",
        "name": "spouting whale",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0b",
        "name": "whale",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc2c",
        "name": "dolphin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1f",
        "name": "fish",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc20",
        "name": "tropical fish",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc21",
        "name": "blowfish",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd88",
        "name": "shark",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc19",
        "name": "octopus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1a",
        "name": "spiral shell",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc0c",
        "name": "snail",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd8b",
        "name": "butterfly",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1b",
        "name": "bug",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1c",
        "name": "ant",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1d",
        "name": "honeybee",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc1e",
        "name": "lady beetle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd97",
        "name": "cricket",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd77\ufe0f",
        "name": "spider",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd78\ufe0f",
        "name": "spider web",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd82",
        "name": "scorpion",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd9f",
        "name": "mosquito",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udda0",
        "name": "microbe",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc90",
        "name": "bouquet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf38",
        "name": "cherry blossom",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcae",
        "name": "white flower",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff5\ufe0f",
        "name": "rosette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf39",
        "name": "rose",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd40",
        "name": "wilted flower",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3a",
        "name": "hibiscus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3b",
        "name": "sunflower",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3c",
        "name": "blossom",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf37",
        "name": "tulip",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf31",
        "name": "seedling",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf32",
        "name": "evergreen tree",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf33",
        "name": "deciduous tree",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf34",
        "name": "palm tree",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf35",
        "name": "cactus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3e",
        "name": "sheaf of rice",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3f",
        "name": "herb",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2618\ufe0f",
        "name": "shamrock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf40",
        "name": "four leaf clover",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf41",
        "name": "maple leaf",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf42",
        "name": "fallen leaf",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf43",
        "name": "leaf fluttering in wind",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Food & Drink",
    "emoji": [
      {
        "unicode": "\ud83c\udf47",
        "name": "grapes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf48",
        "name": "melon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf49",
        "name": "watermelon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4a",
        "name": "tangerine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4b",
        "name": "lemon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4c",
        "name": "banana",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4d",
        "name": "pineapple",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6d",
        "name": "mango",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4e",
        "name": "red apple",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf4f",
        "name": "green apple",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf50",
        "name": "pear",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf51",
        "name": "peach",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf52",
        "name": "cherries",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf53",
        "name": "strawberry",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd5d",
        "name": "kiwi fruit",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf45",
        "name": "tomato",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd65",
        "name": "coconut",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd51",
        "name": "avocado",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf46",
        "name": "eggplant",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd54",
        "name": "potato",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd55",
        "name": "carrot",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf3d",
        "name": "ear of corn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf36\ufe0f",
        "name": "hot pepper",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd52",
        "name": "cucumber",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6c",
        "name": "leafy green",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd66",
        "name": "broccoli",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc4",
        "name": "garlic",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddc5",
        "name": "onion",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83c\udf44",
        "name": "mushroom",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd5c",
        "name": "peanuts",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf30",
        "name": "chestnut",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5e",
        "name": "bread",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd50",
        "name": "croissant",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd56",
        "name": "baguette bread",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd68",
        "name": "pretzel",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6f",
        "name": "bagel",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd5e",
        "name": "pancakes",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc7",
        "name": "waffle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddc0",
        "name": "cheese wedge",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf56",
        "name": "meat on bone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf57",
        "name": "poultry leg",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd69",
        "name": "cut of meat",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd53",
        "name": "bacon",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf54",
        "name": "hamburger",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5f",
        "name": "french fries",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf55",
        "name": "pizza",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2d",
        "name": "hot dog",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6a",
        "name": "sandwich",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2e",
        "name": "taco",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2f",
        "name": "burrito",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd59",
        "name": "stuffed flatbread",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc6",
        "name": "falafel",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udd5a",
        "name": "egg",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf73",
        "name": "cooking",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd58",
        "name": "shallow pan of food",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf72",
        "name": "pot of food",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd63",
        "name": "bowl with spoon",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd57",
        "name": "green salad",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7f",
        "name": "popcorn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc8",
        "name": "butter",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddc2",
        "name": "salt",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6b",
        "name": "canned food",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf71",
        "name": "bento box",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf58",
        "name": "rice cracker",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf59",
        "name": "rice ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5a",
        "name": "cooked rice",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5b",
        "name": "curry rice",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5c",
        "name": "steaming bowl",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf5d",
        "name": "spaghetti",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf60",
        "name": "roasted sweet potato",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf62",
        "name": "oden",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf63",
        "name": "sushi",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf64",
        "name": "fried shrimp",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf65",
        "name": "fish cake with swirl",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd6e",
        "name": "moon cake",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf61",
        "name": "dango",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd5f",
        "name": "dumpling",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd60",
        "name": "fortune cookie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd61",
        "name": "takeout box",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd80",
        "name": "crab",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd9e",
        "name": "lobster",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd90",
        "name": "shrimp",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd91",
        "name": "squid",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddaa",
        "name": "oyster",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83c\udf66",
        "name": "soft ice cream",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf67",
        "name": "shaved ice",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf68",
        "name": "ice cream",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf69",
        "name": "doughnut",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6a",
        "name": "cookie",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf82",
        "name": "birthday cake",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf70",
        "name": "shortcake",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc1",
        "name": "cupcake",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd67",
        "name": "pie",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6b",
        "name": "chocolate bar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6c",
        "name": "candy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6d",
        "name": "lollipop",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6e",
        "name": "custard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf6f",
        "name": "honey pot",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7c",
        "name": "baby bottle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd5b",
        "name": "glass of milk",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u2615",
        "name": "hot beverage",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf75",
        "name": "teacup without handle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf76",
        "name": "sake",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7e",
        "name": "bottle with popping cork",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf77",
        "name": "wine glass",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf78",
        "name": "cocktail glass",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf79",
        "name": "tropical drink",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7a",
        "name": "beer mug",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7b",
        "name": "clinking beer mugs",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd42",
        "name": "clinking glasses",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd43",
        "name": "tumbler glass",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd64",
        "name": "cup with straw",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddc3",
        "name": "beverage box",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddc9",
        "name": "mate",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddca",
        "name": "ice",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udd62",
        "name": "chopsticks",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf7d\ufe0f",
        "name": "fork and knife with plate",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf74",
        "name": "fork and knife",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd44",
        "name": "spoon",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2a",
        "name": "kitchen knife",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udffa",
        "name": "amphora",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Travel & Places",
    "emoji": [
      {
        "unicode": "\ud83c\udf0d",
        "name": "globe showing Europe-Africa",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf0e",
        "name": "globe showing Americas",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf0f",
        "name": "globe showing Asia-Australia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf10",
        "name": "globe with meridians",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddfa\ufe0f",
        "name": "world map",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddfe",
        "name": "map of Japan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udded",
        "name": "compass",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd4\ufe0f",
        "name": "snow-capped mountain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f0\ufe0f",
        "name": "mountain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf0b",
        "name": "volcano",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddfb",
        "name": "mount fuji",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd5\ufe0f",
        "name": "camping",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd6\ufe0f",
        "name": "beach with umbrella",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfdc\ufe0f",
        "name": "desert",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfdd\ufe0f",
        "name": "desert island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfde\ufe0f",
        "name": "national park",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfdf\ufe0f",
        "name": "stadium",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfdb\ufe0f",
        "name": "classical building",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd7\ufe0f",
        "name": "building construction",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf1",
        "name": "brick",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd8\ufe0f",
        "name": "houses",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfda\ufe0f",
        "name": "derelict house",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe0",
        "name": "house",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe1",
        "name": "house with garden",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe2",
        "name": "office building",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe3",
        "name": "Japanese post office",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe4",
        "name": "post office",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe5",
        "name": "hospital",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe6",
        "name": "bank",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe8",
        "name": "hotel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfe9",
        "name": "love hotel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfea",
        "name": "convenience store",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfeb",
        "name": "school",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfec",
        "name": "department store",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfed",
        "name": "factory",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfef",
        "name": "Japanese castle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff0",
        "name": "castle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc92",
        "name": "wedding",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddfc",
        "name": "Tokyo tower",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddfd",
        "name": "Statue of Liberty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26ea",
        "name": "church",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd4c",
        "name": "mosque",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uded5",
        "name": "hindu temple",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udd4d",
        "name": "synagogue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26e9\ufe0f",
        "name": "shinto shrine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd4b",
        "name": "kaaba",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f2",
        "name": "fountain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26fa",
        "name": "tent",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf01",
        "name": "foggy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf03",
        "name": "night with stars",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd9\ufe0f",
        "name": "cityscape",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf04",
        "name": "sunrise over mountains",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf05",
        "name": "sunrise",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf06",
        "name": "cityscape at dusk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf07",
        "name": "sunset",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf09",
        "name": "bridge at night",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2668\ufe0f",
        "name": "hot springs",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa0",
        "name": "carousel horse",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa1",
        "name": "ferris wheel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa2",
        "name": "roller coaster",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc88",
        "name": "barber pole",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfaa",
        "name": "circus tent",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude82",
        "name": "locomotive",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude83",
        "name": "railway car",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude84",
        "name": "high-speed train",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude85",
        "name": "bullet train",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude86",
        "name": "train",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude87",
        "name": "metro",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude88",
        "name": "light rail",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude89",
        "name": "station",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8a",
        "name": "tram",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9d",
        "name": "monorail",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9e",
        "name": "mountain railway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8b",
        "name": "tram car",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8c",
        "name": "bus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8d",
        "name": "oncoming bus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8e",
        "name": "trolleybus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude90",
        "name": "minibus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude91",
        "name": "ambulance",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude92",
        "name": "fire engine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude93",
        "name": "police car",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude94",
        "name": "oncoming police car",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude95",
        "name": "taxi",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude96",
        "name": "oncoming taxi",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude97",
        "name": "automobile",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude98",
        "name": "oncoming automobile",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude99",
        "name": "sport utility vehicle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9a",
        "name": "delivery truck",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9b",
        "name": "articulated lorry",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9c",
        "name": "tractor",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfce\ufe0f",
        "name": "racing car",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcd\ufe0f",
        "name": "motorcycle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef5",
        "name": "motor scooter",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddbd",
        "name": "manual wheelchair",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddbc",
        "name": "motorized wheelchair",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udefa",
        "name": "auto rickshaw",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udeb2",
        "name": "bicycle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef4",
        "name": "kick scooter",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef9",
        "name": "skateboard",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude8f",
        "name": "bus stop",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee3\ufe0f",
        "name": "motorway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee4\ufe0f",
        "name": "railway track",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee2\ufe0f",
        "name": "oil drum",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26fd",
        "name": "fuel pump",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea8",
        "name": "police car light",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea5",
        "name": "horizontal traffic light",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea6",
        "name": "vertical traffic light",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uded1",
        "name": "stop sign",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea7",
        "name": "construction",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2693",
        "name": "anchor",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f5",
        "name": "sailboat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef6",
        "name": "canoe",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea4",
        "name": "speedboat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef3\ufe0f",
        "name": "passenger ship",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f4\ufe0f",
        "name": "ferry",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee5\ufe0f",
        "name": "motor boat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea2",
        "name": "ship",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2708\ufe0f",
        "name": "airplane",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee9\ufe0f",
        "name": "small airplane",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeeb",
        "name": "airplane departure",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeec",
        "name": "airplane arrival",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude82",
        "name": "parachute",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udcba",
        "name": "seat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude81",
        "name": "helicopter",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude9f",
        "name": "suspension railway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea0",
        "name": "mountain cableway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea1",
        "name": "aerial tramway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef0\ufe0f",
        "name": "satellite",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\ude80",
        "name": "rocket",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef8",
        "name": "flying saucer",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udece\ufe0f",
        "name": "bellhop bell",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf3",
        "name": "luggage",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\u231b",
        "name": "hourglass done",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f3",
        "name": "hourglass not done",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u231a",
        "name": "watch",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f0",
        "name": "alarm clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f1\ufe0f",
        "name": "stopwatch",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f2\ufe0f",
        "name": "timer clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd70\ufe0f",
        "name": "mantelpiece clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5b",
        "name": "twelve o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd67",
        "name": "twelve-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd50",
        "name": "one o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5c",
        "name": "one-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd51",
        "name": "two o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5d",
        "name": "two-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd52",
        "name": "three o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5e",
        "name": "three-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd53",
        "name": "four o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5f",
        "name": "four-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd54",
        "name": "five o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd60",
        "name": "five-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd55",
        "name": "six o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd61",
        "name": "six-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd56",
        "name": "seven o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd62",
        "name": "seven-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd57",
        "name": "eight o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd63",
        "name": "eight-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd58",
        "name": "nine o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd64",
        "name": "nine-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd59",
        "name": "ten o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd65",
        "name": "ten-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd5a",
        "name": "eleven o\u2019clock",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd66",
        "name": "eleven-thirty",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf11",
        "name": "new moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf12",
        "name": "waxing crescent moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf13",
        "name": "first quarter moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf14",
        "name": "waxing gibbous moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf15",
        "name": "full moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf16",
        "name": "waning gibbous moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf17",
        "name": "last quarter moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf18",
        "name": "waning crescent moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf19",
        "name": "crescent moon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1a",
        "name": "new moon face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1b",
        "name": "first quarter moon face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1c",
        "name": "last quarter moon face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf21\ufe0f",
        "name": "thermometer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2600\ufe0f",
        "name": "sun",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1d",
        "name": "full moon face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1e",
        "name": "sun with face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude90",
        "name": "ringed planet",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\u2b50",
        "name": "star",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf1f",
        "name": "glowing star",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf20",
        "name": "shooting star",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf0c",
        "name": "milky way",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2601\ufe0f",
        "name": "cloud",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26c5",
        "name": "sun behind cloud",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26c8\ufe0f",
        "name": "cloud with lightning and rain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf24\ufe0f",
        "name": "sun behind small cloud",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf25\ufe0f",
        "name": "sun behind large cloud",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf26\ufe0f",
        "name": "sun behind rain cloud",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf27\ufe0f",
        "name": "cloud with rain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf28\ufe0f",
        "name": "cloud with snow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf29\ufe0f",
        "name": "cloud with lightning",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2a\ufe0f",
        "name": "tornado",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2b\ufe0f",
        "name": "fog",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf2c\ufe0f",
        "name": "wind face",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf00",
        "name": "cyclone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf08",
        "name": "rainbow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf02",
        "name": "closed umbrella",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2602\ufe0f",
        "name": "umbrella",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2614",
        "name": "umbrella with rain drops",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f1\ufe0f",
        "name": "umbrella on ground",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26a1",
        "name": "high voltage",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2744\ufe0f",
        "name": "snowflake",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2603\ufe0f",
        "name": "snowman",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26c4",
        "name": "snowman without snow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2604\ufe0f",
        "name": "comet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd25",
        "name": "fire",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca7",
        "name": "droplet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf0a",
        "name": "water wave",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Activities",
    "emoji": [
      {
        "unicode": "\ud83c\udf83",
        "name": "jack-o-lantern",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf84",
        "name": "Christmas tree",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf86",
        "name": "fireworks",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf87",
        "name": "sparkler",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde8",
        "name": "firecracker",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\u2728",
        "name": "sparkles",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf88",
        "name": "balloon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf89",
        "name": "party popper",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8a",
        "name": "confetti ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8b",
        "name": "tanabata tree",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8d",
        "name": "pine decoration",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8e",
        "name": "Japanese dolls",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8f",
        "name": "carp streamer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf90",
        "name": "wind chime",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf91",
        "name": "moon viewing ceremony",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde7",
        "name": "red envelope",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf80",
        "name": "ribbon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf81",
        "name": "wrapped gift",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf97\ufe0f",
        "name": "reminder ribbon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf9f\ufe0f",
        "name": "admission tickets",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfab",
        "name": "ticket",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf96\ufe0f",
        "name": "military medal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc6",
        "name": "trophy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc5",
        "name": "sports medal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd47",
        "name": "1st place medal",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd48",
        "name": "2nd place medal",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd49",
        "name": "3rd place medal",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u26bd",
        "name": "soccer ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26be",
        "name": "baseball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4e",
        "name": "softball",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc0",
        "name": "basketball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd0",
        "name": "volleyball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc8",
        "name": "american football",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfc9",
        "name": "rugby football",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfbe",
        "name": "tennis",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4f",
        "name": "flying disc",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb3",
        "name": "bowling",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfcf",
        "name": "cricket game",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd1",
        "name": "field hockey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd2",
        "name": "ice hockey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4d",
        "name": "lacrosse",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfd3",
        "name": "ping pong",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff8",
        "name": "badminton",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4a",
        "name": "boxing glove",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4b",
        "name": "martial arts uniform",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd45",
        "name": "goal net",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u26f3",
        "name": "flag in hole",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26f8\ufe0f",
        "name": "ice skate",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa3",
        "name": "fishing pole",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd3f",
        "name": "diving mask",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83c\udfbd",
        "name": "running shirt",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfbf",
        "name": "skis",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udef7",
        "name": "sled",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd4c",
        "name": "curling stone",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfaf",
        "name": "direct hit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude80",
        "name": "yo-yo",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\ude81",
        "name": "kite",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83c\udfb1",
        "name": "pool 8 ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2e",
        "name": "crystal ball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddff",
        "name": "nazar amulet",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfae",
        "name": "video game",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd79\ufe0f",
        "name": "joystick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb0",
        "name": "slot machine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb2",
        "name": "game die",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde9",
        "name": "puzzle piece",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf8",
        "name": "teddy bear",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\u2660\ufe0f",
        "name": "spade suit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2665\ufe0f",
        "name": "heart suit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2666\ufe0f",
        "name": "diamond suit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2663\ufe0f",
        "name": "club suit",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u265f\ufe0f",
        "name": "chess pawn",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udccf",
        "name": "joker",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udc04",
        "name": "mahjong red dragon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb4",
        "name": "flower playing cards",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfad",
        "name": "performing arts",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddbc\ufe0f",
        "name": "framed picture",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa8",
        "name": "artist palette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf5",
        "name": "thread",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf6",
        "name": "yarn",
        "variants": [],
        "version": [
          11,
          0
        ]
      }
    ]
  },
  {
    "name": "Objects",
    "emoji": [
      {
        "unicode": "\ud83d\udc53",
        "name": "glasses",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd76\ufe0f",
        "name": "sunglasses",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7d",
        "name": "goggles",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7c",
        "name": "lab coat",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddba",
        "name": "safety vest",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc54",
        "name": "necktie",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc55",
        "name": "t-shirt",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc56",
        "name": "jeans",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde3",
        "name": "scarf",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde4",
        "name": "gloves",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde5",
        "name": "coat",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde6",
        "name": "socks",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc57",
        "name": "dress",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc58",
        "name": "kimono",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7b",
        "name": "sari",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\ude71",
        "name": "one-piece swimsuit",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\ude72",
        "name": "briefs",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\ude73",
        "name": "shorts",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc59",
        "name": "bikini",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5a",
        "name": "woman\u2019s clothes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5b",
        "name": "purse",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5c",
        "name": "handbag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5d",
        "name": "clutch bag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udecd\ufe0f",
        "name": "shopping bags",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf92",
        "name": "backpack",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5e",
        "name": "man\u2019s shoe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc5f",
        "name": "running shoe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7e",
        "name": "hiking boot",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\udd7f",
        "name": "flat shoe",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc60",
        "name": "high-heeled shoe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc61",
        "name": "woman\u2019s sandal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude70",
        "name": "ballet shoes",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc62",
        "name": "woman\u2019s boot",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc51",
        "name": "crown",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc52",
        "name": "woman\u2019s hat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa9",
        "name": "top hat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf93",
        "name": "graduation cap",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\udde2",
        "name": "billed cap",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\u26d1\ufe0f",
        "name": "rescue worker\u2019s helmet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcff",
        "name": "prayer beads",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc84",
        "name": "lipstick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc8d",
        "name": "ring",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc8e",
        "name": "gem stone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd07",
        "name": "muted speaker",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd08",
        "name": "speaker low volume",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd09",
        "name": "speaker medium volume",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0a",
        "name": "speaker high volume",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce2",
        "name": "loudspeaker",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce3",
        "name": "megaphone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcef",
        "name": "postal horn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd14",
        "name": "bell",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd15",
        "name": "bell with slash",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfbc",
        "name": "musical score",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb5",
        "name": "musical note",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb6",
        "name": "musical notes",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf99\ufe0f",
        "name": "studio microphone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf9a\ufe0f",
        "name": "level slider",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf9b\ufe0f",
        "name": "control knobs",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa4",
        "name": "microphone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa7",
        "name": "headphone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcfb",
        "name": "radio",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb7",
        "name": "saxophone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb8",
        "name": "guitar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfb9",
        "name": "musical keyboard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfba",
        "name": "trumpet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfbb",
        "name": "violin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude95",
        "name": "banjo",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\udd41",
        "name": "drum",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf1",
        "name": "mobile phone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf2",
        "name": "mobile phone with arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u260e\ufe0f",
        "name": "telephone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcde",
        "name": "telephone receiver",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcdf",
        "name": "pager",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce0",
        "name": "fax machine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0b",
        "name": "battery",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0c",
        "name": "electric plug",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcbb",
        "name": "laptop",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udda5\ufe0f",
        "name": "desktop computer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udda8\ufe0f",
        "name": "printer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2328\ufe0f",
        "name": "keyboard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddb1\ufe0f",
        "name": "computer mouse",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddb2\ufe0f",
        "name": "trackball",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcbd",
        "name": "computer disk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcbe",
        "name": "floppy disk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcbf",
        "name": "optical disk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc0",
        "name": "dvd",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddee",
        "name": "abacus",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa5",
        "name": "movie camera",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf9e\ufe0f",
        "name": "film frames",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcfd\ufe0f",
        "name": "film projector",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfac",
        "name": "clapper board",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcfa",
        "name": "television",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf7",
        "name": "camera",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf8",
        "name": "camera with flash",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf9",
        "name": "video camera",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcfc",
        "name": "videocassette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0d",
        "name": "magnifying glass tilted left",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0e",
        "name": "magnifying glass tilted right",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd6f\ufe0f",
        "name": "candle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca1",
        "name": "light bulb",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd26",
        "name": "flashlight",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfee",
        "name": "red paper lantern",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude94",
        "name": "diya lamp",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udcd4",
        "name": "notebook with decorative cover",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd5",
        "name": "closed book",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd6",
        "name": "open book",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd7",
        "name": "green book",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd8",
        "name": "blue book",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd9",
        "name": "orange book",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcda",
        "name": "books",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd3",
        "name": "notebook",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd2",
        "name": "ledger",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc3",
        "name": "page with curl",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcdc",
        "name": "scroll",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc4",
        "name": "page facing up",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf0",
        "name": "newspaper",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddde\ufe0f",
        "name": "rolled-up newspaper",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd1",
        "name": "bookmark tabs",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd16",
        "name": "bookmark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff7\ufe0f",
        "name": "label",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb0",
        "name": "money bag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb4",
        "name": "yen banknote",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb5",
        "name": "dollar banknote",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb6",
        "name": "euro banknote",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb7",
        "name": "pound banknote",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb8",
        "name": "money with wings",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb3",
        "name": "credit card",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddfe",
        "name": "receipt",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb9",
        "name": "chart increasing with yen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb1",
        "name": "currency exchange",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcb2",
        "name": "heavy dollar sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2709\ufe0f",
        "name": "envelope",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce7",
        "name": "e-mail",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce8",
        "name": "incoming envelope",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce9",
        "name": "envelope with arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce4",
        "name": "outbox tray",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce5",
        "name": "inbox tray",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce6",
        "name": "package",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udceb",
        "name": "closed mailbox with raised flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcea",
        "name": "closed mailbox with lowered flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcec",
        "name": "open mailbox with raised flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udced",
        "name": "open mailbox with lowered flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcee",
        "name": "postbox",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddf3\ufe0f",
        "name": "ballot box with ballot",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u270f\ufe0f",
        "name": "pencil",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2712\ufe0f",
        "name": "black nib",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd8b\ufe0f",
        "name": "fountain pen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd8a\ufe0f",
        "name": "pen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd8c\ufe0f",
        "name": "paintbrush",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd8d\ufe0f",
        "name": "crayon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcdd",
        "name": "memo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcbc",
        "name": "briefcase",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc1",
        "name": "file folder",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc2",
        "name": "open file folder",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddc2\ufe0f",
        "name": "card index dividers",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc5",
        "name": "calendar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc6",
        "name": "tear-off calendar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddd2\ufe0f",
        "name": "spiral notepad",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddd3\ufe0f",
        "name": "spiral calendar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc7",
        "name": "card index",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc8",
        "name": "chart increasing",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcc9",
        "name": "chart decreasing",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcca",
        "name": "bar chart",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udccb",
        "name": "clipboard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udccc",
        "name": "pushpin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udccd",
        "name": "round pushpin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcce",
        "name": "paperclip",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd87\ufe0f",
        "name": "linked paperclips",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udccf",
        "name": "straight ruler",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcd0",
        "name": "triangular ruler",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2702\ufe0f",
        "name": "scissors",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddc3\ufe0f",
        "name": "card file box",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddc4\ufe0f",
        "name": "file cabinet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddd1\ufe0f",
        "name": "wastebasket",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd12",
        "name": "locked",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd13",
        "name": "unlocked",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd0f",
        "name": "locked with pen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd10",
        "name": "locked with key",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd11",
        "name": "key",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udddd\ufe0f",
        "name": "old key",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd28",
        "name": "hammer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude93",
        "name": "axe",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\u26cf\ufe0f",
        "name": "pick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2692\ufe0f",
        "name": "hammer and pick",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee0\ufe0f",
        "name": "hammer and wrench",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udde1\ufe0f",
        "name": "dagger",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2694\ufe0f",
        "name": "crossed swords",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2b",
        "name": "pistol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff9",
        "name": "bow and arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udee1\ufe0f",
        "name": "shield",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd27",
        "name": "wrench",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd29",
        "name": "nut and bolt",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2699\ufe0f",
        "name": "gear",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udddc\ufe0f",
        "name": "clamp",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2696\ufe0f",
        "name": "balance scale",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddaf",
        "name": "probing cane",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udd17",
        "name": "link",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26d3\ufe0f",
        "name": "chains",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf0",
        "name": "toolbox",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf2",
        "name": "magnet",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\u2697\ufe0f",
        "name": "alembic",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddea",
        "name": "test tube",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddeb",
        "name": "petri dish",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddec",
        "name": "dna",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2c",
        "name": "microscope",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2d",
        "name": "telescope",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udce1",
        "name": "satellite antenna",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udc89",
        "name": "syringe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude78",
        "name": "drop of blood",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udc8a",
        "name": "pill",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude79",
        "name": "adhesive bandage",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\ude7a",
        "name": "stethoscope",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udeaa",
        "name": "door",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udecf\ufe0f",
        "name": "bed",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udecb\ufe0f",
        "name": "couch and lamp",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude91",
        "name": "chair",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udebd",
        "name": "toilet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udebf",
        "name": "shower",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec1",
        "name": "bathtub",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83e\ude92",
        "name": "razor",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83e\uddf4",
        "name": "lotion bottle",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf7",
        "name": "safety pin",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddf9",
        "name": "broom",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddfa",
        "name": "basket",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddfb",
        "name": "roll of paper",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddfc",
        "name": "soap",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddfd",
        "name": "sponge",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83e\uddef",
        "name": "fire extinguisher",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83d\uded2",
        "name": "shopping cart",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeac",
        "name": "cigarette",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26b0\ufe0f",
        "name": "coffin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26b1\ufe0f",
        "name": "funeral urn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uddff",
        "name": "moai",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Symbols",
    "emoji": [
      {
        "unicode": "\ud83c\udfe7",
        "name": "ATM sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeae",
        "name": "litter in bin sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb0",
        "name": "potable water",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u267f",
        "name": "wheelchair symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb9",
        "name": "men\u2019s room",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeba",
        "name": "women\u2019s room",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udebb",
        "name": "restroom",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udebc",
        "name": "baby symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udebe",
        "name": "water closet",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec2",
        "name": "passport control",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec3",
        "name": "customs",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec4",
        "name": "baggage claim",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udec5",
        "name": "left luggage",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26a0\ufe0f",
        "name": "warning",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb8",
        "name": "children crossing",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26d4",
        "name": "no entry",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeab",
        "name": "prohibited",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb3",
        "name": "no bicycles",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udead",
        "name": "no smoking",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeaf",
        "name": "no littering",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb1",
        "name": "non-potable water",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udeb7",
        "name": "no pedestrians",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf5",
        "name": "no mobile phones",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1e",
        "name": "no one under eighteen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2622\ufe0f",
        "name": "radioactive",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2623\ufe0f",
        "name": "biohazard",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2b06\ufe0f",
        "name": "up arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2197\ufe0f",
        "name": "up-right arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u27a1\ufe0f",
        "name": "right arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2198\ufe0f",
        "name": "down-right arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2b07\ufe0f",
        "name": "down arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2199\ufe0f",
        "name": "down-left arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2b05\ufe0f",
        "name": "left arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2196\ufe0f",
        "name": "up-left arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2195\ufe0f",
        "name": "up-down arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2194\ufe0f",
        "name": "left-right arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u21a9\ufe0f",
        "name": "right arrow curving left",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u21aa\ufe0f",
        "name": "left arrow curving right",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2934\ufe0f",
        "name": "right arrow curving up",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2935\ufe0f",
        "name": "right arrow curving down",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd03",
        "name": "clockwise vertical arrows",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd04",
        "name": "counterclockwise arrows button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd19",
        "name": "BACK arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1a",
        "name": "END arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1b",
        "name": "ON! arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1c",
        "name": "SOON arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1d",
        "name": "TOP arrow",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\uded0",
        "name": "place of worship",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u269b\ufe0f",
        "name": "atom symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd49\ufe0f",
        "name": "om",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2721\ufe0f",
        "name": "star of David",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2638\ufe0f",
        "name": "wheel of dharma",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u262f\ufe0f",
        "name": "yin yang",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u271d\ufe0f",
        "name": "latin cross",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2626\ufe0f",
        "name": "orthodox cross",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u262a\ufe0f",
        "name": "star and crescent",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u262e\ufe0f",
        "name": "peace symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd4e",
        "name": "menorah",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd2f",
        "name": "dotted six-pointed star",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2648",
        "name": "Aries",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2649",
        "name": "Taurus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264a",
        "name": "Gemini",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264b",
        "name": "Cancer",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264c",
        "name": "Leo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264d",
        "name": "Virgo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264e",
        "name": "Libra",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u264f",
        "name": "Scorpio",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2650",
        "name": "Sagittarius",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2651",
        "name": "Capricorn",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2652",
        "name": "Aquarius",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2653",
        "name": "Pisces",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26ce",
        "name": "Ophiuchus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd00",
        "name": "shuffle tracks button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd01",
        "name": "repeat button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd02",
        "name": "repeat single button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25b6\ufe0f",
        "name": "play button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23e9",
        "name": "fast-forward button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23ed\ufe0f",
        "name": "next track button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23ef\ufe0f",
        "name": "play or pause button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25c0\ufe0f",
        "name": "reverse button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23ea",
        "name": "fast reverse button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23ee\ufe0f",
        "name": "last track button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd3c",
        "name": "upwards button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23eb",
        "name": "fast up button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd3d",
        "name": "downwards button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23ec",
        "name": "fast down button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f8\ufe0f",
        "name": "pause button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23f9\ufe0f",
        "name": "stop button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23fa\ufe0f",
        "name": "record button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u23cf\ufe0f",
        "name": "eject button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udfa6",
        "name": "cinema",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd05",
        "name": "dim button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd06",
        "name": "bright button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf6",
        "name": "antenna bars",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf3",
        "name": "vibration mode",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcf4",
        "name": "mobile phone off",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2640\ufe0f",
        "name": "female sign",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u2642\ufe0f",
        "name": "male sign",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u2695\ufe0f",
        "name": "medical symbol",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\u267e\ufe0f",
        "name": "infinity",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\u267b\ufe0f",
        "name": "recycling symbol",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u269c\ufe0f",
        "name": "fleur-de-lis",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd31",
        "name": "trident emblem",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udcdb",
        "name": "name badge",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd30",
        "name": "Japanese symbol for beginner",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2b55",
        "name": "hollow red circle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2705",
        "name": "check mark button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2611\ufe0f",
        "name": "check box with check",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2714\ufe0f",
        "name": "check mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2716\ufe0f",
        "name": "multiplication sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u274c",
        "name": "cross mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u274e",
        "name": "cross mark button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2795",
        "name": "plus sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2796",
        "name": "minus sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2797",
        "name": "division sign",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u27b0",
        "name": "curly loop",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u27bf",
        "name": "double curly loop",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u303d\ufe0f",
        "name": "part alternation mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2733\ufe0f",
        "name": "eight-spoked asterisk",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2734\ufe0f",
        "name": "eight-pointed star",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2747\ufe0f",
        "name": "sparkle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u203c\ufe0f",
        "name": "double exclamation mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2049\ufe0f",
        "name": "exclamation question mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2753",
        "name": "question mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2754",
        "name": "white question mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2755",
        "name": "white exclamation mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2757",
        "name": "exclamation mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u3030\ufe0f",
        "name": "wavy dash",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u00a9\ufe0f",
        "name": "copyright",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u00ae\ufe0f",
        "name": "registered",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2122\ufe0f",
        "name": "trade mark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "#\ufe0f\u20e3",
        "name": "keycap:  #",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "*\ufe0f\u20e3",
        "name": "keycap:  *",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "0\ufe0f\u20e3",
        "name": "keycap:  0",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "1\ufe0f\u20e3",
        "name": "keycap:  1",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "2\ufe0f\u20e3",
        "name": "keycap:  2",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "3\ufe0f\u20e3",
        "name": "keycap:  3",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "4\ufe0f\u20e3",
        "name": "keycap:  4",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "5\ufe0f\u20e3",
        "name": "keycap:  5",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "6\ufe0f\u20e3",
        "name": "keycap:  6",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "7\ufe0f\u20e3",
        "name": "keycap:  7",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "8\ufe0f\u20e3",
        "name": "keycap:  8",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "9\ufe0f\u20e3",
        "name": "keycap:  9",
        "variants": [],
        "version": [
          0,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd1f",
        "name": "keycap:  10",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd20",
        "name": "input latin uppercase",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd21",
        "name": "input latin lowercase",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd22",
        "name": "input numbers",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd23",
        "name": "input symbols",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd24",
        "name": "input latin letters",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd70\ufe0f",
        "name": "A button (blood type)",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd8e",
        "name": "AB button (blood type)",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd71\ufe0f",
        "name": "B button (blood type)",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd91",
        "name": "CL button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd92",
        "name": "COOL button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd93",
        "name": "FREE button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2139\ufe0f",
        "name": "information",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd94",
        "name": "ID button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u24c2\ufe0f",
        "name": "circled M",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd95",
        "name": "NEW button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd96",
        "name": "NG button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd7e\ufe0f",
        "name": "O button (blood type)",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd97",
        "name": "OK button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd7f\ufe0f",
        "name": "P button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd98",
        "name": "SOS button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd99",
        "name": "UP! button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udd9a",
        "name": "VS button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude01",
        "name": "Japanese \u201chere\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude02\ufe0f",
        "name": "Japanese \u201cservice charge\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude37\ufe0f",
        "name": "Japanese \u201cmonthly amount\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude36",
        "name": "Japanese \u201cnot free of charge\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude2f",
        "name": "Japanese \u201creserved\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude50",
        "name": "Japanese \u201cbargain\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude39",
        "name": "Japanese \u201cdiscount\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude1a",
        "name": "Japanese \u201cfree of charge\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude32",
        "name": "Japanese \u201cprohibited\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude51",
        "name": "Japanese \u201cacceptable\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude38",
        "name": "Japanese \u201capplication\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude34",
        "name": "Japanese \u201cpassing grade\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude33",
        "name": "Japanese \u201cvacancy\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u3297\ufe0f",
        "name": "Japanese \u201ccongratulations\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u3299\ufe0f",
        "name": "Japanese \u201csecret\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude3a",
        "name": "Japanese \u201copen for business\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\ude35",
        "name": "Japanese \u201cno vacancy\u201d button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd34",
        "name": "red circle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udfe0",
        "name": "orange circle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe1",
        "name": "yellow circle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe2",
        "name": "green circle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udd35",
        "name": "blue circle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udfe3",
        "name": "purple circle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe4",
        "name": "brown circle",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\u26ab",
        "name": "black circle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u26aa",
        "name": "white circle",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udfe5",
        "name": "red square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe7",
        "name": "orange square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe8",
        "name": "yellow square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe9",
        "name": "green square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfe6",
        "name": "blue square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfea",
        "name": "purple square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\ud83d\udfeb",
        "name": "brown square",
        "variants": [],
        "version": [
          12,
          1
        ]
      },
      {
        "unicode": "\u2b1b",
        "name": "black large square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u2b1c",
        "name": "white large square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25fc\ufe0f",
        "name": "black medium square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25fb\ufe0f",
        "name": "white medium square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25fe",
        "name": "black medium-small square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25fd",
        "name": "white medium-small square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25aa\ufe0f",
        "name": "black small square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\u25ab\ufe0f",
        "name": "white small square",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd36",
        "name": "large orange diamond",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd37",
        "name": "large blue diamond",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd38",
        "name": "small orange diamond",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd39",
        "name": "small blue diamond",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd3a",
        "name": "red triangle pointed up",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd3b",
        "name": "red triangle pointed down",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udca0",
        "name": "diamond with a dot",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd18",
        "name": "radio button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd33",
        "name": "white square button",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udd32",
        "name": "black square button",
        "variants": [],
        "version": [
          2,
          0
        ]
      }
    ]
  },
  {
    "name": "Flags",
    "emoji": [
      {
        "unicode": "\ud83c\udfc1",
        "name": "chequered flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83d\udea9",
        "name": "triangular flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udf8c",
        "name": "crossed flags",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff4",
        "name": "black flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff3\ufe0f",
        "name": "white flag",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff3\ufe0f\u200d\ud83c\udf08",
        "name": "rainbow flag",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff4\u200d\u2620\ufe0f",
        "name": "pirate flag",
        "variants": [],
        "version": [
          11,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\udde8",
        "name": "flag:  Ascension Island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\udde9",
        "name": "flag:  Andorra",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddea",
        "name": "flag:  United Arab Emirates",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddeb",
        "name": "flag:  Afghanistan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddec",
        "name": "flag:  Antigua & Barbuda",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddee",
        "name": "flag:  Anguilla",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf1",
        "name": "flag:  Albania",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf2",
        "name": "flag:  Armenia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf4",
        "name": "flag:  Angola",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf6",
        "name": "flag:  Antarctica",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf7",
        "name": "flag:  Argentina",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf8",
        "name": "flag:  American Samoa",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddf9",
        "name": "flag:  Austria",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddfa",
        "name": "flag:  Australia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddfc",
        "name": "flag:  Aruba",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddfd",
        "name": "flag:  \u00c5land Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde6\ud83c\uddff",
        "name": "flag:  Azerbaijan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\udde6",
        "name": "flag:  Bosnia & Herzegovina",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\udde7",
        "name": "flag:  Barbados",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\udde9",
        "name": "flag:  Bangladesh",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddea",
        "name": "flag:  Belgium",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddeb",
        "name": "flag:  Burkina Faso",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddec",
        "name": "flag:  Bulgaria",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\udded",
        "name": "flag:  Bahrain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddee",
        "name": "flag:  Burundi",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddef",
        "name": "flag:  Benin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf1",
        "name": "flag:  St. Barth\u00e9lemy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf2",
        "name": "flag:  Bermuda",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf3",
        "name": "flag:  Brunei",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf4",
        "name": "flag:  Bolivia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf6",
        "name": "flag:  Caribbean Netherlands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf7",
        "name": "flag:  Brazil",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf8",
        "name": "flag:  Bahamas",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddf9",
        "name": "flag:  Bhutan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddfb",
        "name": "flag:  Bouvet Island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddfc",
        "name": "flag:  Botswana",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddfe",
        "name": "flag:  Belarus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde7\ud83c\uddff",
        "name": "flag:  Belize",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\udde6",
        "name": "flag:  Canada",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\udde8",
        "name": "flag:  Cocos (Keeling) Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\udde9",
        "name": "flag:  Congo - Kinshasa",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddeb",
        "name": "flag:  Central African Republic",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddec",
        "name": "flag:  Congo - Brazzaville",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\udded",
        "name": "flag:  Switzerland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddee",
        "name": "flag:  C\u00f4te d\u2019Ivoire",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf0",
        "name": "flag:  Cook Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf1",
        "name": "flag:  Chile",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf2",
        "name": "flag:  Cameroon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf3",
        "name": "flag:  China",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf4",
        "name": "flag:  Colombia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf5",
        "name": "flag:  Clipperton Island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddf7",
        "name": "flag:  Costa Rica",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddfa",
        "name": "flag:  Cuba",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddfb",
        "name": "flag:  Cape Verde",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddfc",
        "name": "flag:  Cura\u00e7ao",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddfd",
        "name": "flag:  Christmas Island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddfe",
        "name": "flag:  Cyprus",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde8\ud83c\uddff",
        "name": "flag:  Czechia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddea",
        "name": "flag:  Germany",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddec",
        "name": "flag:  Diego Garcia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddef",
        "name": "flag:  Djibouti",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddf0",
        "name": "flag:  Denmark",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddf2",
        "name": "flag:  Dominica",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddf4",
        "name": "flag:  Dominican Republic",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udde9\ud83c\uddff",
        "name": "flag:  Algeria",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\udde6",
        "name": "flag:  Ceuta & Melilla",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\udde8",
        "name": "flag:  Ecuador",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddea",
        "name": "flag:  Estonia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddec",
        "name": "flag:  Egypt",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\udded",
        "name": "flag:  Western Sahara",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddf7",
        "name": "flag:  Eritrea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddf8",
        "name": "flag:  Spain",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddf9",
        "name": "flag:  Ethiopia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddea\ud83c\uddfa",
        "name": "flag:  European Union",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddee",
        "name": "flag:  Finland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddef",
        "name": "flag:  Fiji",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddf0",
        "name": "flag:  Falkland Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddf2",
        "name": "flag:  Micronesia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddf4",
        "name": "flag:  Faroe Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddeb\ud83c\uddf7",
        "name": "flag:  France",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\udde6",
        "name": "flag:  Gabon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\udde7",
        "name": "flag:  United Kingdom",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\udde9",
        "name": "flag:  Grenada",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddea",
        "name": "flag:  Georgia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddeb",
        "name": "flag:  French Guiana",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddec",
        "name": "flag:  Guernsey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\udded",
        "name": "flag:  Ghana",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddee",
        "name": "flag:  Gibraltar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf1",
        "name": "flag:  Greenland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf2",
        "name": "flag:  Gambia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf3",
        "name": "flag:  Guinea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf5",
        "name": "flag:  Guadeloupe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf6",
        "name": "flag:  Equatorial Guinea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf7",
        "name": "flag:  Greece",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf8",
        "name": "flag:  South Georgia & South Sandwich Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddf9",
        "name": "flag:  Guatemala",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddfa",
        "name": "flag:  Guam",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddfc",
        "name": "flag:  Guinea-Bissau",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddec\ud83c\uddfe",
        "name": "flag:  Guyana",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddf0",
        "name": "flag:  Hong Kong SAR China",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddf2",
        "name": "flag:  Heard & McDonald Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddf3",
        "name": "flag:  Honduras",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddf7",
        "name": "flag:  Croatia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddf9",
        "name": "flag:  Haiti",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udded\ud83c\uddfa",
        "name": "flag:  Hungary",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\udde8",
        "name": "flag:  Canary Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\udde9",
        "name": "flag:  Indonesia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddea",
        "name": "flag:  Ireland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf1",
        "name": "flag:  Israel",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf2",
        "name": "flag:  Isle of Man",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf3",
        "name": "flag:  India",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf4",
        "name": "flag:  British Indian Ocean Territory",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf6",
        "name": "flag:  Iraq",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf7",
        "name": "flag:  Iran",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf8",
        "name": "flag:  Iceland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddee\ud83c\uddf9",
        "name": "flag:  Italy",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddef\ud83c\uddea",
        "name": "flag:  Jersey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddef\ud83c\uddf2",
        "name": "flag:  Jamaica",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddef\ud83c\uddf4",
        "name": "flag:  Jordan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddef\ud83c\uddf5",
        "name": "flag:  Japan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddea",
        "name": "flag:  Kenya",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddec",
        "name": "flag:  Kyrgyzstan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\udded",
        "name": "flag:  Cambodia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddee",
        "name": "flag:  Kiribati",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddf2",
        "name": "flag:  Comoros",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddf3",
        "name": "flag:  St. Kitts & Nevis",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddf5",
        "name": "flag:  North Korea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddf7",
        "name": "flag:  South Korea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddfc",
        "name": "flag:  Kuwait",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddfe",
        "name": "flag:  Cayman Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf0\ud83c\uddff",
        "name": "flag:  Kazakhstan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\udde6",
        "name": "flag:  Laos",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\udde7",
        "name": "flag:  Lebanon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\udde8",
        "name": "flag:  St. Lucia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddee",
        "name": "flag:  Liechtenstein",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddf0",
        "name": "flag:  Sri Lanka",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddf7",
        "name": "flag:  Liberia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddf8",
        "name": "flag:  Lesotho",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddf9",
        "name": "flag:  Lithuania",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddfa",
        "name": "flag:  Luxembourg",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddfb",
        "name": "flag:  Latvia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf1\ud83c\uddfe",
        "name": "flag:  Libya",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\udde6",
        "name": "flag:  Morocco",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\udde8",
        "name": "flag:  Monaco",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\udde9",
        "name": "flag:  Moldova",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddea",
        "name": "flag:  Montenegro",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddeb",
        "name": "flag:  St. Martin",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddec",
        "name": "flag:  Madagascar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\udded",
        "name": "flag:  Marshall Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf0",
        "name": "flag:  North Macedonia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf1",
        "name": "flag:  Mali",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf2",
        "name": "flag:  Myanmar (Burma)",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf3",
        "name": "flag:  Mongolia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf4",
        "name": "flag:  Macao SAR China",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf5",
        "name": "flag:  Northern Mariana Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf6",
        "name": "flag:  Martinique",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf7",
        "name": "flag:  Mauritania",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf8",
        "name": "flag:  Montserrat",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddf9",
        "name": "flag:  Malta",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddfa",
        "name": "flag:  Mauritius",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddfb",
        "name": "flag:  Maldives",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddfc",
        "name": "flag:  Malawi",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddfd",
        "name": "flag:  Mexico",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddfe",
        "name": "flag:  Malaysia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf2\ud83c\uddff",
        "name": "flag:  Mozambique",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\udde6",
        "name": "flag:  Namibia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\udde8",
        "name": "flag:  New Caledonia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddea",
        "name": "flag:  Niger",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddeb",
        "name": "flag:  Norfolk Island",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddec",
        "name": "flag:  Nigeria",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddee",
        "name": "flag:  Nicaragua",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddf1",
        "name": "flag:  Netherlands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddf4",
        "name": "flag:  Norway",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddf5",
        "name": "flag:  Nepal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddf7",
        "name": "flag:  Nauru",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddfa",
        "name": "flag:  Niue",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf3\ud83c\uddff",
        "name": "flag:  New Zealand",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf4\ud83c\uddf2",
        "name": "flag:  Oman",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\udde6",
        "name": "flag:  Panama",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddea",
        "name": "flag:  Peru",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddeb",
        "name": "flag:  French Polynesia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddec",
        "name": "flag:  Papua New Guinea",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\udded",
        "name": "flag:  Philippines",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf0",
        "name": "flag:  Pakistan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf1",
        "name": "flag:  Poland",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf2",
        "name": "flag:  St. Pierre & Miquelon",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf3",
        "name": "flag:  Pitcairn Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf7",
        "name": "flag:  Puerto Rico",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf8",
        "name": "flag:  Palestinian Territories",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddf9",
        "name": "flag:  Portugal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddfc",
        "name": "flag:  Palau",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf5\ud83c\uddfe",
        "name": "flag:  Paraguay",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf6\ud83c\udde6",
        "name": "flag:  Qatar",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf7\ud83c\uddea",
        "name": "flag:  R\u00e9union",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf7\ud83c\uddf4",
        "name": "flag:  Romania",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf7\ud83c\uddf8",
        "name": "flag:  Serbia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf7\ud83c\uddfa",
        "name": "flag:  Russia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf7\ud83c\uddfc",
        "name": "flag:  Rwanda",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\udde6",
        "name": "flag:  Saudi Arabia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\udde7",
        "name": "flag:  Solomon Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\udde8",
        "name": "flag:  Seychelles",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\udde9",
        "name": "flag:  Sudan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddea",
        "name": "flag:  Sweden",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddec",
        "name": "flag:  Singapore",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\udded",
        "name": "flag:  St. Helena",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddee",
        "name": "flag:  Slovenia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddef",
        "name": "flag:  Svalbard & Jan Mayen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf0",
        "name": "flag:  Slovakia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf1",
        "name": "flag:  Sierra Leone",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf2",
        "name": "flag:  San Marino",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf3",
        "name": "flag:  Senegal",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf4",
        "name": "flag:  Somalia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf7",
        "name": "flag:  Suriname",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf8",
        "name": "flag:  South Sudan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddf9",
        "name": "flag:  S\u00e3o Tom\u00e9 & Pr\u00edncipe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddfb",
        "name": "flag:  El Salvador",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddfd",
        "name": "flag:  Sint Maarten",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddfe",
        "name": "flag:  Syria",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf8\ud83c\uddff",
        "name": "flag:  Eswatini",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\udde6",
        "name": "flag:  Tristan da Cunha",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\udde8",
        "name": "flag:  Turks & Caicos Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\udde9",
        "name": "flag:  Chad",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddeb",
        "name": "flag:  French Southern Territories",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddec",
        "name": "flag:  Togo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\udded",
        "name": "flag:  Thailand",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddef",
        "name": "flag:  Tajikistan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf0",
        "name": "flag:  Tokelau",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf1",
        "name": "flag:  Timor-Leste",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf2",
        "name": "flag:  Turkmenistan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf3",
        "name": "flag:  Tunisia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf4",
        "name": "flag:  Tonga",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf7",
        "name": "flag:  Turkey",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddf9",
        "name": "flag:  Trinidad & Tobago",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddfb",
        "name": "flag:  Tuvalu",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddfc",
        "name": "flag:  Taiwan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddf9\ud83c\uddff",
        "name": "flag:  Tanzania",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\udde6",
        "name": "flag:  Ukraine",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddec",
        "name": "flag:  Uganda",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddf2",
        "name": "flag:  U.S. Outlying Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddf3",
        "name": "flag:  United Nations",
        "variants": [],
        "version": [
          4,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddf8",
        "name": "flag:  United States",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddfe",
        "name": "flag:  Uruguay",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfa\ud83c\uddff",
        "name": "flag:  Uzbekistan",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\udde6",
        "name": "flag:  Vatican City",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\udde8",
        "name": "flag:  St. Vincent & Grenadines",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\uddea",
        "name": "flag:  Venezuela",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\uddec",
        "name": "flag:  British Virgin Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\uddee",
        "name": "flag:  U.S. Virgin Islands",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\uddf3",
        "name": "flag:  Vietnam",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfb\ud83c\uddfa",
        "name": "flag:  Vanuatu",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfc\ud83c\uddeb",
        "name": "flag:  Wallis & Futuna",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfc\ud83c\uddf8",
        "name": "flag:  Samoa",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfd\ud83c\uddf0",
        "name": "flag:  Kosovo",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfe\ud83c\uddea",
        "name": "flag:  Yemen",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddfe\ud83c\uddf9",
        "name": "flag:  Mayotte",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddff\ud83c\udde6",
        "name": "flag:  South Africa",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddff\ud83c\uddf2",
        "name": "flag:  Zambia",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\uddff\ud83c\uddfc",
        "name": "flag:  Zimbabwe",
        "variants": [],
        "version": [
          2,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc65\udb40\udc6e\udb40\udc67\udb40\udc7f",
        "name": "flag:  England",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc73\udb40\udc63\udb40\udc74\udb40\udc7f",
        "name": "flag:  Scotland",
        "variants": [],
        "version": [
          5,
          0
        ]
      },
      {
        "unicode": "\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc77\udb40\udc6c\udb40\udc73\udb40\udc7f",
        "name": "flag:  Wales",
        "variants": [],
        "version": [
          5,
          0
        ]
      }
    ]
  }
];
