import os
import re
import enum


HERE = os.path.dirname(os.path.abspath(__file__))
DATA_FILE = os.path.join(
    HERE,
    "management",
    "emoji-test.txt"
)

STATIC_ROOT = os.path.join(HERE, "static")


class EmojiData:
    lineparse = re.compile(
        r"(?P<group>^# (?P<group_type>(?:sub)?group): (?P<group_name>.*)\n$)|" +
        r"(?P<emoji>^" +
        r"(?P<emoji_points>[0-9A-F ]+)\s*; (?P<emoji_status>[-a-z]+)\s+" +
        r"# [^E]+E(?P<emoji_version>[0-9.]+) (?P<emoji_name>[^:]+)(?::(?P<emoji_name_extra>.*))?"
        r"\n$)"
    )

    def load(self, data_file=DATA_FILE):
        self.groups = []
        self.names = {}
        current_group = None
        current_subgroup = None
        #lineno = 0
        with open(data_file) as infile:
            for line in infile:
                #lineno += 1
                match = self.lineparse.match(line)
                if match:
                    if match.group("group"):
                        group = EmojiGroup(match.group("group_name"))
                        if current_group is None or match.group("group_type") == "group":
                            self.groups.append(group)
                            current_group = group
                        else:
                            current_subgroup = match.group("group_name")
                    elif match.group("emoji"):
                        self._emoji(match, current_group, current_subgroup)

    def _emoji(self, match, current_group, subgroup):
        points = [int(p, 16) for p in match.group("emoji_points").strip().split(" ")]
        status = EmojiStatus[match.group("emoji_status").title().replace("-", "")]
        version = tuple(map(int, match.group("emoji_version").split(".")))
        name = match.group("emoji_name")
        name_extra = match.group("emoji_name_extra")

        if name in {"flag", "keycap"}:
            name += ": %s" % name_extra
            name_extra = ""

        if name_extra:
            base = self.names[name]
            name = name_extra
        else:
            base = None

        emo = Emoji(points, status, version, name, subgroup)

        if base:
            base.variants.append(emo)
        else:
            current_group.emoji.append(emo)
            self.names[emo.name] = emo

    def compile(self):
        groups = []
        for group in self.groups:
            compiled = group.compile()
            if compiled:
                groups.append(compiled)
        return groups


class EmojiGroup:
    def __init__(self, name):
        self.name = name
        self.emoji = []

    def __str__(self):
        return self.name

    def compile(self):
        emoji = [
            e.compile()
            for e in self.emoji
            if e.status == EmojiStatus.FullyQualified
        ]
        if emoji:
            return {
                "name": self.name,
                "emoji": emoji
            }
        return None


class EmojiStatus(enum.Enum):
    Component = enum.auto()
    FullyQualified = enum.auto()
    MinimallyQualified = enum.auto()
    Unqualified = enum.auto()


class Emoji:
    def __init__(self, points, status, version, name, subgroup):
        self.points = points
        self.unicode = "".join(map(chr, points))
        self.status = status
        self.version = version
        self.name = name
        self.variants = []
        self.subgroup = subgroup

    def compile(self):
        return {
            "unicode": self.unicode,
            "name": self.name,
            "variants": [v.compile() for v in self.variants],
            "version": self.version
        }

    def __str__(self):
        return self.name
