from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import cached_property

from . import api


class Token(models.Model):
    user = models.OneToOneField(User, models.CASCADE, related_name="trello_token")
    token = models.CharField(max_length=65, db_index=True, unique=True)

    @cached_property
    def api(self):
        return api.AuthorizedTrello(self.token)

    @cached_property
    def me(self):
        return self.api.me
