from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from apps.social_media.integration import Integration, view


class TrelloConfig(Integration):
    name = 'apps.trello'
    is_connected_attr = "trello_token"

    def integration_ready(self):
        from . import api
        self.api = api

    @view()
    def connect(self, request):
        if "token" in request.GET:
            token = request.GET["token"]
            trello = self.api.AuthorizedTrello(token)
            if not trello.me.valid:
                messages.error("Could not connect to trello")
            else:
                self.models_module.Token.objects.update_or_create(user=request.user, token=token)
                return redirect("trello:manage")
        return render(request, "trello/connect.html", {})

    @view("")
    def manage(self, request):
        token = getattr(request.user, "trello_token", None)
        ctx = {
            "token": token,
            "trello": self.api.Trello(),
        }
        return render(request, "trello/manage.html", ctx)

    @view()
    def disconnect(self, request):
        token = get_object_or_404(self.models_module.Token, user=request.user)
        if token.api.revoke():
            token.delete()
        return redirect("trello:manage")
