import urllib.request
import json

from django.urls import reverse
from django.conf import settings
from django.utils.functional import cached_property

from apps.httputils import urljoin, urlopen


class Trello:
    api_url = "https://trello.com/1/"

    def __init__(self, key=None):
        self.key = key or settings.TRELLO_KEY

    def get(self, method, args={}):
        url = self.get_url(method, args)
        data = urlopen(url)
        if data.error:
            return None
        return json.load(data)

    def get_url(self, method, args):
        return urljoin(self.api_url, method, args)

    def authorized(self, token):
        return AuthorizedTrello(self.key, token)


class AuthorizedTrello(Trello):
    def __init__(self, token, key=None):
        super().__init__(key)
        self.token = token

    def get_url(self, method, args):
        args = dict(args)
        args["key"] = self.key
        args["token"] = self.token
        return super().get_url(method, args)

    def revoke(self):
        req = urllib.request.Request(
            super().get_url("tokens/%s/" % self.token, {"key": self.key}),
            method="DELETE"
        )
        resp = urlopen(req)
        return True

    @property
    def me(self):
        return Member(self, "me")


class TrelloFetchable:
    def __init__(self, client: AuthorizedTrello, data=None):
        self.client = client
        if data is not None:
            self.from_data(data)

    def from_data(self, data):
        raise NotImplementedError()

    def refresh(self):
        raise NotImplementedError()


class Member(TrelloFetchable):
    def __init__(self, client, id):
        super().__init__(client)
        self._fetch_id = id
        self.refresh()

    def from_data(self, data):
        if data is None:
            self.valid = False
            return
        self.valid = True
        self.id = data["id"]
        self.avatar_url = data["avatarUrl"]+"/170.png"
        self.full_name = data["fullName"]
        self.url = data["url"]
        self.username = data["username"]
        self._boards = None
        if "boards" in data:
            self._boards = [
                Board(self.client, bdata)
                for bdata in data["boards"]
            ]

    @cached_property
    def boards(self):
        #import pdb; pdb.set_trace(); pass
        if self._boards is None:
            self.from_data(self.client.get("members/%s" % self._fetch_id, {
                "boards": "open",
                "board_fields": ",".join(Board.nested_fields)
            }))
        return self._boards

    def refresh(self):
        self.from_data(self.client.get("members/%s" % self._fetch_id))


class Board(TrelloFetchable):
    nested_fields = ["name", "desc", "url", "prefs", "starred"]

    def __init__(self, client, id):
        if isinstance(id, dict):
            super().__init__(client, id)
        else:
            self.id = id
            self.refresh()

    def refresh(self):
        self.from_data(self.client.get("boards/%s" % self.id))

    def from_data(self, data):
        self.id = data["id"]
        self.name = data["name"]
        self.desc = data["desc"]
        self.url = data["url"]
        self.starred = data["starred"]
        self.background_image = data["prefs"]["backgroundImage"]
        self.background_color = data["prefs"]["backgroundColor"]
        self.background_brightness = data["prefs"]["backgroundBrightness"]

