from django import template
from django.urls import reverse
from .. import api

register = template.Library()


@register.simple_tag(takes_context=True)
def trello_authorize_url(context, auth_url, trello=None, **kwargs):
    if not trello:
        trello = api.Trello()

    url = reverse(auth_url, kwargs=kwargs)
    url = context["request"].build_absolute_uri(url)

    return trello.get_url("authorize", {
            "callback_method": "fragment",
            "return_url": url,
            "scope": "read,write",
            "expiration": "never",
            "name": "GlaxComm",
            "key": trello.key,
            "response_type": "token",
        })
