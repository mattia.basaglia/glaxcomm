from django.core.management import BaseCommand
import nltk


class Command(BaseCommand):
    def download(self, what):
        self.stdout.write("Dowloading %s\n" % what)
        nltk.download(what)

    def handle(self, **options):
        self.download("vader_lexicon")
        self.download("punkt")

