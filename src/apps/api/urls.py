from django.urls import path
from . import views

app_name = "api"
urlpatterns = [
    path('sentiment/', views.sentiment, name="sentiment"),
]
