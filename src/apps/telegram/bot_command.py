import os
import traceback
import importlib
import telethon.tl.types

_commands = {}
_loaded = False


class BotCommandList:
    def __init__(self):
        self.commands = []
        self.inline = None
        self.button_callback = None

    def add_command(self, command):
        self.commands.append(command)

    async def run_command(self, update):
        if update.type == "inline_query":
            if self.inline:
                await self.inline(update)
                return True
            return False
        elif update.type == "callback_query":
            if self.button_callback:
                await self.button_callback(update)
                return True
            return False

        for cmd in self.commands:
            if cmd.match(update):
                await cmd.run(update)
                return True
        return await self.run_unknown_command(update)

    def get_command_data(self, include_hidden=False):
        return [
            c.to_data()
            for c in self.commands
            if isinstance(c, BotCommand) and (include_hidden or not c.hidden)
        ]

    async def run_unknown_command(self, event):
        return False

    async def on_loaded(self, client):
        pass

    def enabled(self, client):
        pass

    def disabled(self, client):
        pass


class BotCommand:
    def __init__(self, trigger, function, description, hidden):
        self.trigger = trigger
        self.function = function
        self.description = description
        self.hidden = hidden

    def match(self, update):
        if update.type == "message" and update.command == self.trigger:
            return True
        return False

    async def run(self, update):
        await self.function(update)

    def to_data(self):
        return telethon.tl.types.BotCommand(
            self.trigger,
            self.description or self.trigger
        )

    def __repr__(self):
        return "<BotCommand %s>" % self.trigger


class MediaCommand:
    def __init__(self, function):
        self.function = function

    def match(self, event):
        if event.type == "message" and event.event.message.media and not event.event.sender.is_self:
            return True
        return False

    async def run(self, update):
        await self.function(update)

    def __repr__(self):
        return "<MediaCommand>"


def register_command(bot_username, command):
    if bot_username not in _commands:
        _commands[bot_username] = BotCommandList()
    _commands[bot_username].add_command(command)


def register_inline(bot_username, function):
    if bot_username not in _commands:
        _commands[bot_username] = BotCommandList()
    _commands[bot_username].inline = function


def register_button_callback(bot_username, function):
    if bot_username not in _commands:
        _commands[bot_username] = BotCommandList()
    _commands[bot_username].button_callback = function


def bot_command(bot_username, trigger=None, desc=None, hidden=False):
    def deco(fn):
        name = trigger or fn.__name__
        description = desc or fn.__doc__ or ""
        register_command(bot_username, BotCommand(name, fn, description.strip(), hidden))
        return fn
    return deco


def bot_media(bot_username, **kwargs):
    def deco(fn):
        register_command(bot_username, MediaCommand(fn, **kwargs))
        return fn
    return deco


def bot_inline(bot_username):
    def deco(fn):
        register_inline(bot_username, fn)
        return fn
    return deco


def bot_button_callback(bot_username):
    def deco(fn):
        register_button_callback(bot_username, fn)
        return fn
    return deco


def load_commands():
    global _loaded
    if _loaded:
        return
    _loaded = True

    def _collect_files(path, package):
        for file in os.listdir(path):
            fullpath = os.path.join(path, file)
            if file.endswith(".py") and os.path.isfile(fullpath):
                importlib.import_module(package+"."+file[:-3])
            elif os.path.isdir(fullpath) and file != "__pycache__" and file != "assets":
                _collect_files(fullpath, package+"."+file)

    _collect_files(
        os.path.join(os.path.dirname(__file__), "bot_commands"),
        __name__.rsplit(".", 1)[0] + ".bot_commands"
    )


async def run_command(update, runner: BotCommandList):
    load_commands()
    username = update.client.me.username

    debug = update.client.debug
    if debug and debug.debug_enabled:
        if debug.debug_as is None:
            if update.type == "message" and update.command:
                for k, cmdlist in _commands.items():
                    if await cmdlist.run_command(update):
                        return True
            return False

        username = debug.debug_as.me.username

    return await runner.run_command(update)


def get_bot_runner(bot_username):
    load_commands()
    return _commands.get(bot_username, BotCommandList())


def get_command_argument(update):
    if update.inline_query:
        return update.inline_query.query
    elif update.message:
        t = update.message.text
        if " " in t:
            return t.split(" ", 1)[1]
    return ""
