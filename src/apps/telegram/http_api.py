import json

from django.conf import settings

from apps.httputils import urljoin, urlopen, MultiPartEncoder


class HttpApi:
    def __init__(self, token, api_url="https://api.telegram.org/"):
        self.token = token
        self.api_url = api_url

    def get(self, method, args={}):
        url = urljoin(self.api_url, "bot%s" % self.token, method, args)
        return self._process_request(url, timeout=2)

    def post(self, method, fields):
        url = urljoin(self.api_url, "bot%s" % self.token, method)
        data = b""
        encoder = MultiPartEncoder()
        for name, value in fields.items():
            if isinstance(value, InputFile):
                data += encoder.format_file(name, value.value, value.filename, value.content_type)
            else:
                data += encoder.format_field(name, value)
        data += encoder.format_end()
        return self._process_request(url, data, headers={
            "Content-Type": encoder.content_type
        })

    def _process_request(self, *a, **kw):
        data = urlopen(*a, **kw)

        try:
            return json.load(data)
        except json.JSONDecodeError:
            if settings.DEBUG:
                raise
            return {}


class InputFile:
    def __init__(self, filename, value, content_type="application/octet-stream"):
        self.filename = filename
        self.value = value
        self.content_type = content_type
