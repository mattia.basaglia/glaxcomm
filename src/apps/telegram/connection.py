import os
import io
import sys
import time
import hmac
import signal
import asyncio
import hashlib
import datetime
import threading
import traceback
import contextlib

from django.conf import settings
from django.utils import timezone
from django.contrib.auth import login
from django.http.request import QueryDict
from django.core.files.storage import FileSystemStorage, File
from django.utils.autoreload import file_changed


from telethon import TelegramClient, events, functions
from telethon.tl.types import (
    DocumentAttributeFilename,
    BotCommand, BotCommand, BotCommandScopeDefault,
    KeyboardButtonUrl, KeyboardButtonCallback,
    MessageEntityMention, MessageEntityMentionName,
    ChatAdminRights
)
import telethon.tl.types
from telethon.tl.functions.users import GetFullUserRequest
from telethon.helpers import add_surrogate

import lottie

from PIL import Image
from lottie.exporters.core import export_tgs
from lottie.exporters.cairo import export_png

from .bot_command import run_command, get_bot_runner

# For some reason these people are DOSsing my bots, so ban them
global_ban_list = {
    1495361012,  # @ikrom0904
    5178633268,  # @M_zaxrati_qaysar
    5393660082,  # @Azamat_Maqsetbaev
    5420325279,  # @Am1r4ek
    5453359632,  # @MSXL07
    5459236548,  # @Bakhad1rovnass08
    5189273820,
    5420172050,  # @I_Am_UnKn0wN
    2142843576,  # @Bituruvchii
    5029115175,  # @keragimsan_006
    293173601,
    991829687,   # @Prohibited_04
    5387057546,  # @OZOD_0709
    5446675841,
    1866047519,
    5518332721,  # @Dani_el_stars
    5869043427,
    1102593946,  # @DONOCHAM
    5581095649,  # @Shahzodaa1631
    5855422970,
    893768882,   # @tiny_marvel_05_06
    1551325979,
    2059565218,  # @sabirova_k9
    5253982034,  # @Jahon_1127
    5483796574,  # @Bonucha_003
    5094503426,
    1831771636,
    1158618618,  # @sss77ww
    2013313620,
    5310373609,  # @Shoxruz_001
    5806185964,
    873568732,   # @YangiBoyev_DiyorBek_Official
    5529514131,  # @vakhobov_4242
    837043977,
    1621543720,  # @MRX1007
    5143463260,
    5107454669,  # @Sharaf0331
    1514894646,  # @e_Davaee_dafshaee
    5978215416,  # @akramovich_2113
    5555890511,  # @Mukhtorov_55_09
    198205602,   # @Akow2451
    5876770752,
    1594715124,  # @Mechatronics_7080
    1217836422,  # @Q_A_A_0106
    5266082534,  # @Insta_1zzatullayev_b
    5585164981,  # @Abdujalil_5600
    1336929361,
    1081222902,  # @Leyli_06
    1818594927,
    5858633573,  # @Yakhyoof
    5790462810,  #  @Miss_doctor_qizaloq
    1030383863,  #  @Atoyev_Furqatbek
    1059263585,  #  @miraxmatov98
    1086436035,  #
    1095198348,  #
    1118915264,  #
    1176258018,  #
    1176877134,  #
    1178174796,  #  @Oper_0_6
    1207593726,  #
    1239871320,  #  @all_thing_have_a_secter
    1278667724,  #  @Khasanboyev50
    1283832621,  #  @Samad_of
    1337644290,  #  @NAWRIZBEKARALBAEV
    1404308266,  #
    1405632512,  #  @ibrokhim_shokhimardonov_7770
    1454217930,  #
    1455945724,  #  @TKTI_05_22
    1471899524,  #
    1477342411,  #  @Rejim_qori_rostan
    1477444844,  #  @online_7_25
    1491349027,  #
    1509938989,  #  @The_Richest_Man
    1511799750,  #
    1568756583,  #  @Xasanboy0912
    1601069774,  #
    1626001804,  #  @oxun6699
    163242038,  #  @RealNomer21
    1636901237,  #  @Jahongir1108
    1657846461,  #  @Otashrepoficall
    1697023311,  #
    1707892176,  #  @uz_qurbonov
    1723547987,  #
    1744951830,  #  @Farxodjon0802
    1747205388,  #  @islom_abduxoliqov
    1755122124,  #  @F1eV6807Dnki4Q043785s
    1762526096,  #  @muxammadbilol571
    1769116867,  #  @Frs023
    1793669374,  #  @navruzov1ch
    1794028205,  #  @Kocha_bolasi_04_8224
    1807818974,  #  @Xuligan_0302
    1821193414,  #  @az_0715
    1861599948,  #  @OTAM_BOYLIGIM_ONAM_JANNATIM_571
    1875636473,  #  @Sardor_Oken98
    1892009147,  #  @iranech_09_02
    1935324469,  #  @Lord_o7o
    1944908698,  #  @Qaqnus22
    1957503901,  #
    1966278506,  #  @AMIRXON_521
    1969784486,  #
    1971556929,  #  @poka_tixoo
    1978055482,  #
    2007926197,  #
    2019745321,  #  @Stom_fak122
    2037102935,  #  @Sh_06_10_a
    2048056086,  #  @top_stones3
    2078419917,  #  @shakh_ake77
    2092412493,  #
    2121169395,  #  @Ache_Polloo_7
    2125025240,  #  @Doston_800
    2137672493,  #  @JasurbekMirkhamitov
    2140628050,  #
    214882089,  #  @OscaRRoocK
    366365615,  #  @Shermatov_Orzubek
    450179032,  #  @Munojatxon777
    5011612584,  #  @murad_khalikov
    5050839978,  #  @Xisravali
    5051422182,  #  @saydakbarov_s
    5078173366,  #  @Nomalum_ayolga_maktub
    5080437158,  #  @kdrrvv
    5101983405,  #  @odiw03
    5106146401,  #  @elnur_749
    511404484,  #  @ogabek_me7
    5129503412,  #  @fara_ft001
    5135980805,  #  @Turdibayeva_Nafisa07
    5137166170,  #  @akocai_arsenad
    5140434061,  #  @Hamroyev_O5
    5149259141,  #
    5156314727,  #
    5164121897,  #  @Alehandr_12
    5203722372,  #
    5207965211,  #  @by_1ql1ma_05_07
    5234599898,  #  @Eldor_Khudoyqulov
    5236421939,  #
    5245021609,  #  @umarov_4147
    5248553686,  #
    5279954949,  #  @k_jahongir
    5280817371,  #
    5290109752,  #
    5321514735,  #
    5321819848,  #  @Darmensarsenov21
    5330235141,  #  @lyudka_04_4
    5336897157,  #  @MaratovAsadbek
    5337593752,  #  @Jalilov_N2004
    5346236664,  #
    5361948422,  #  @uwuahhgmhahaha
    5390029064,  #
    5408087192,  #
    5411982827,  #  @Hero2005
    5415407737,  #  @asran_rap21
    5416672884,  #  @matematika_admin
    5422322181,  #  @Sardor_816
    5422732115,  #  @kayina_fairy
    5426990454,  #
    5432319827,  #  @Stroong_1
    5445447023,  #
    5445828467,  #
    5450449617,  #  @Turdaliyev_Axror
    5451069662,  #  @sol1yev_27
    5457206253,  #  @SENPAI758
    5475667601,  #  @LUCIFER_MATh
    5479727842,  #
    5480360905,  #  @baxod1r0vna_08
    5491673398,  #  @lalalilam_04
    5499656757,  #  @wittyboy03
    5499960604,  #  @ergvshev
    5505470695,  #
    5507481395,  #
    5515523350,  #  @Jaxa_jizzaxiski
    5525088781,  #
    5526734242,  #
    5537879571,  #
    5539251357,  #
    5548185866,  #  @Amir2018_Mustafo2022
    5575276945,  #  @abduazimv
    5576456525,  #  @Mn00700070
    557820556,  #  @bi1o1
    5581841624,  #  @TheKhasan01
    5589639146,  #
    5611487557,  #
    5627724943,  #
    5662917576,  #  @barn0_1
    5678600266,  #
    5696186414,  #  @Jennet_0605
    5708473009,  #  @necromantgirl
    5709830386,  #
    5723703811,  #
    5725595755,  #
    5739452770,  #  @laziz_0605
    5740457322,  #
    5744363879,  #
    5747627899,  #  @Mahkamova_077
    5773400273,  #  @Bunyodxon1029
    5784522018,  #
    5795954259,  #  @student_79
    5846788601,  #
    5905357363,  #  @jiyenbaeva_gulshad
    5907315015,  #
    5936235535,  #  @d1or71
    650664880,  #  @asta_sekin_tuproqgaxam_organasan
    672891315,  #  @Sayyid_Xorazmiy
    693468250,  #  @AlhamdulillahAllah_is_number_one
    703065792,  #  @Bek_0423
    725701988,  #  @abdulloh_mufid_01
    730842560,  #  @mirjaxons
    760912345,  #  @Ashirov_001
    768688385,  #  @Yakubjanoff_007
    771357422,  #  @anvarov05
    775320189,  #
    776889924,  #
    877630062,  #  @abdulaz1zzzz
    959269219,  #  @greklon
    70232804,  #  @Jumayev789
    5522352532,  #  @S_rasulov001
    5307380128,  #  @S171817
    5854239220,  #  @A_B_U_Y_I_M
    770133488,  #  @Mr_MUR0D
    5829033529,  #
    1250482004,  #  @WARRIORIZZATILLO
    5324639205,  #  @Developer_2025
    747662652,  #  @azmiddinovich_Q
    1944028553,  #  @Murod_125
    5565440135,  #  @M_luna_oo3
    5705382392,  #  @jam_006
    866742666,  #  @insta_prosta_az1kk
    730594257,  #  @September_08_09
    5243117439,  #
    1577276663,  #  @edinstevennayaa
    5537618312,  #  @davidkhan_1998
    5956236747,  #  @Abu_Axiy
    5456782913,  #
    5418490861,  #  @joker_2901
    944322234,  #  @Abdugani_Abdullayev
    5142990145,  #
    561792344,  #  @shodmonov_700
    5473185264,  #
    5694468648,  #
    1968665626,  #  @Shohruhbek_1709
    1886129590,  #
    5336557602,  #  @Invincible_Xn
    5481645127,  #
    1147920815,  #
    553767859,  #  @Mirvohid0212
    1319446033,  #  @Fhsuhc
    5680989917,  #  @bat1rov1ch
    1990235786,  #  @He_6eCnoKouT
    5750788107,  #
    5103347592,  #  @NABIJON_OBLOBERDIYEV
    144713036,  #  @jamol_0102
    546047378,  #  @AXROR3034
    1612032931,  #
    5831820594,  #
    177253673,  #
    2018085961,  #
    1428212181,  #  @SenseofpurposeS
    1842826513,  #  @oyat_illo_7
    1943000576,  #  @uzbek_001sila
    5681590564,  #
    5797091605,  #
    1351736123,  #  @Markeloff4341
    1784609174,  #
    5086212400,  #  @weikh_Legenda
    5230921639,  #  @H_Asadbey09
    5532362570,  #  @Akbar_2168
    1661950317,  #  @Goldt_tm
    5271798104,  #  @nazarbek_0108
    5507110093,  #  @Usmon_dobriy
    5601019129,  #  @Solih_2o07
    5618364195,  #  @XuDoJnIk_0827
    1532506735,  #
    5642532092,  #
    632370534,  #  @Shokhrukh_Kuchkarov
    886755043,  #
    1746698738,  #  @abrorali_99
    1803418239,  #  @abbosbekpattoyev
    2005038871,  #
    1966185626,  #  @Jewelry2503
    1237662833,  #
    1418166047,  #
    2100700024,  #  @arabec228
    5497715448,  #  @Nishonboyev_official
    859814909,  #
    5243248975,  #  @Yoosuf_2005
    919420517,  #  @erzod_1208
    1265390796,  #  @murodova_m_o
    1439063105,  #  @Erkinjonov_2931
    5566660390,  #  @nodirovich_mladsh1y
    5760053833,  #  @QOMA_RUN
    5154296333,  #
    960344599,  #  @iSoMiDDiNoViCH
    5612082750,  #  @mamajonov001
    1303894365,  #  @de_SAMURAI7O
    5083885307,  #  @odamlar_uchun_bizns_qlas
    5989030029,  #
    1219634804,  #  @Ri1jol
    1290892029,  #  @S_TDIU_1
    5592562909,  #  @sanjarbek_09052
    841124609,  #  @kdkskls
    5893715154,  #
    785681532,  #
    1925809114,  #  @podsos_lll000xxx
    5002177019,  #  @Fazil_off
    1454415984,  #  @Qarshilik_boy
    5634990569,  #
    5703097388,  #  @farrux_akee6
    209124037,  #
    361884808,  #  @Adham0101600
    906982592,  #
    1590577981,  #
    5647628043,  #
    1005728919,  #  @narbekovas_1
    1290694476,  #  @Alhamdulillah8313
    5545210303,  #  @Farishtam_0321
    5923878461,  #  @rustamoff05
    652743439,  #
    802961386,  #  @Umar_Azima
    829440449,  #
    906428904,  #
    1237501290,  #  @shohjahon3165
    2032030639,  #
    2064367115,  #
    5241917829,  #  @Narmuradov
    5380447078,  #  @feliz_1200
    5755093069,  #  @A313A_2
    5863283146,  #  @sukhrob_isayev
    1032650080,  #  @bmatlubov
    5401721382,  #
    5324182675,  #
    5333301956,  #  @Yusupovich_111
    5252882481,  #
    5802216802,  #
    5574654360,  #  @BEK1zod_007
    5121771410,  #  @PaNdaa2707
    5664336751,  #  @shax1na_o7
    1621986829,  #  @jaxonaliyevv
    2089691456,  #
    2095674382,  #
    5015897241,  #  @kamoljon_off
    5272642623,  #
    5201641151,  #  @simplyboy06
    1627514220,  #  @sevgi_va_dustlikx
    2024822266,  #  @SUNATILLA_AVIATSA04
    2103836508,  #  @KingCR_7
    1451023326,  #  @Axtamov_Sarvar
    5279437517,  #  @DiyorbekYT
    220564473,  #  @ruby1739
    1994158607,  #  @Shukurlayevna
    5050130226,  #  @Zukhriddin_Abdukarimov
    5786730637,  #  @Bek_oken_05
    5148634143,  #
    804005734,  #  @makhamatovva1
    1870030186,  #
    5367219007,  #  @Uzbek_banda
    5675123163,  #  @ibroh1m_ceek
    688868946,  #  @dilmurod05_09
    1005731480,  #  @Karimoff717
    5331318742,  #
    1828687256,  #
    2116099362,  #  @Asl1dd1n_4574
    1832828602,  #
    5534084533,  #  @Siroj_ake_011
    5594740068,  #  @ahmadjonov_16_08
    5669445382,  #
    5768174337,  #
    5033372517,  #  @ya_HELLIX17
    5228115249,  #  @s_makhmudov_o4
    5350806936,  #  @hasanboy_06_07
    5386008296,  #  @S_a_r_v_a_r_10
    5475643256,  #  @K1hujayevaa
    1700634712,  #
    1892116997,  #  @Stom_01_27
    1295680207,  #  @Gdbriel
    1394645740,  #
    1721103487,  #  @maxsumaxon1979
    5408746151,  #  @XAYOTSUVI
    1456488013,  #  @Solehbola_2003
    1573635239,  #  @dolleye11037
    1102952983,  #  @Fakhr1ddinovich
    1707172593,  #
    1919022177,  #  @Pulatovogabek
    1943427186,  #  @Hamroqulova_08
    2076824959,  #  @KILE4KA_OE
    5068397392,  #
    5303361087,  #  @uzbdominator
    5451175475,  #  @PUBG_Time_1
    5676027660,  #
    333459859,  #
    786720157,  #  @H_7o1
    1165295624,  #
    1390919585,  #
    1664816830,  #
    1672178001,  #  @iiiloveyouso
    1756203927,  #  @melon_mods45
    1780947535,  #  @umed2003
    1817967451,  #
    1818574192,  #  @EBPOnA
    1984386325,  #  @Kraffton
    2110787270,  #
    5056867470,  #  @Neuep
    5064410910,  #
    5369419093,  #
    5383838478,  #  @axii_fozil
    5715967371,  #
    1949532735,  #  @ilhom_Farmon0v
    5670685496,  #  @abc_29_27
    5769605788,  #  @Radik0206
    1956093079,  #  @HUM0YUN
    1830836796,  #  @Sardoroffical18
    1388347592,  #
    5484589386,  #  @sayid_muxtorov
    5270593175,  #  @Sol1kham_oo5
    1794073562,  #
    1224846322,  #  @Zuba0303
    522060551,  #
    1671586691,  #  @Jonik_14
    5205002879,  #  @AKBARKHON_T
    5211499302,  #
    5281218328,  #  @Zavq1dd1N05
    5313070559,  #
    5645825851,  #
    125824689,  #  @vohidjon_abduvaliyev
    429146065,  #  @Abdumalik_Abdujalilov
    1147718413,  #  @rustam_ibragimovic
    5735544740,  #
    1647675461,  #
    696307383,  #
    1807193358,  #  @SHOX_7_707_7
    5203471645,  #  @aliiiwkrisa
    5782988491,  #  @Yuldashoff
    5804244493,  #
    5369767743,  #  @SUktamov0
    5542503044,  #  @Otaw_kuv
    5813124359,  #  @xasanov0711
    5528917170,  #
    1338699452,  #  @sardorchik080
    945761894,  #  @yoshaktyor
    1954195632,  #  @abdu_halim123
    5050581773,  #  @bakhtiyarovc_oo1
    5943561974,  #
    1939956921,  #
    5784265808,  #
    5807192844,  #
    5351598245,  #
    1430796058,  #  @Dildora_8118
    5349920674,  #
    1792090113,  #
    5014165902,  #
    1073991364,  #  @jalilbekjalilov
    2073245748,  #  @Roziboyev_SOBIRADDIN_2010
    5604515154,  #
    2085578010,  #
    5300032019,  #  @OYATILLO02
    5373136487,  #  @zxc_koko
    1559320202,  #  @u_sokh1bjon
    1815652177,  #  @Ozimmi_soxtalarm
    5091774005,  #
    240094905,  #  @ImDenis3_9
    1752537858,  #  @wwjwal1fuckyou
    5134497151,  #  @Melanholik_03
    5514275092,  #  @Abubakr2005_05
    874514258,  #  @Khalimov_j
    1488118147,  #  @TH3YRE_L00K1NG_4T_Y0UU
    1758751268,  #  @Alloh_uAkbar
    1879126559,  #  @shatzhenya
    1898611187,  #  @Yoquboff17
    2144424100,  #  @Ibragimov_Jurabek
    5119871195,  #
    5702366703,  #  @Dam1roooov
    5745698810,  #  @Dilnoz_03_12
    5929394907,  #
    733022142,  #  @ilxam_010
    1279630937,  #  @t_xushnud
    1433004313,  #
    1471280055,  #  @EVGEHIY_RU
    1591608073,  #
    1630521925,  #  @Maxsutali_0
    1684891514,  #
    1806488946,  #
    1865620513,  #
    1961484804,  #  @HasbiyAllah_71
    2135649327,  #  @senyorita_006
    5089394408,  #  @ONASH_3D
    5210623539,  #  @AsliddinDeveloper
    5351941585,  #
    5571417662,  #  @Goulsman
    5588659557,  #  @aPuSHeR09
    5606722011,  #
    5644788584,  #  @ALinaDobraxx
    5649567380,  #
    5747015493,  #  @Jaxe_ake_0797
    773152414,  #
    5392985580,  #  @JSHM_005
    185786666,  #  @Joldibek
    5318214492,  #  @marjon_narzullaeva
    5219427292,  #
    690738091,  #  @Uzbeksila838
    5188938287,  #  @S_matyakubovna
    1684724197,  #
    5343648504,  #
    5538395833,  #
    5545519724,  #  @axi_antahu
    5107228488,  #  @Tojibouev
    5618548011,  #
    198404652,  #
    5881097673,  #
    2010602218,  #  @jabborov_b9
    5418341887,  #  @baxt1yorov_sardor_121
    1593895734,  #  @FotimakhonZ_F_Z
    5653665051,  #
    5688312702,  #
    5721674509,  #  @Ozodbek15_04
    1454781423,  #  @lifeinchill
    5049235414,  #  @shar1pov_kuu1
    544356296,  #
    827456830,  #  @nusratlv
    1013182498,  #  @Always_cheerful13
    1024843329,  #  @Onamdan_boshqasi_bir_tiyinn
    1178626475,  #  @Shakhzod_obloyev
    839407985,  #  @AGD377703
    5266509543,  #
    2136418994,  #  @Kitten_ksks
    5009823609,  #  @Azizbek_2662
    5191380712,  #
    5767792844,  #
    2061314023,  #
    5097179275,  #  @firdavs_top_garant
    5585147587,  #  @Binance_UCDT
    1772121813,  #
    5299657836,  #  @ARMEEN1404
    5575833100,  #  @Akxmedoff_27
    5960401362,  #
    883959698,  #
    1785523944,  #  @iamshox5
    537357602,  #  @defxx10
    1885891601,  #  @Bahodirjon007
    1817859977,  #
    5160322266,  #  @shavkatbekov_7o1
    5602150308,  #
    1653554565,  #
    1879147824,  #  @WeirD_o7
    969048483,  #
    1373795608,  #
    1674334252,  #  @B777G
    5805656439,  #
    1801190892,  #  @abubakir2111
    5253167912,  #  @Hazratbek_Mardonov
    825864849,  #
    1316264885,  #
    1771399353,  #
    5174771426,  #  @Avgan_1_1
    449834589,  #
    5668518979,  #  @Xolmamat_off_18
    5828759324,  #
    5828418712,  #
    728428694,  #
    2137693430,  #  @Yakubjanoff_6x9
    5717386191,  #  @tme_akhmadjonov_as
    5455707336,  #  @annur_2oo5
    5422666048,  #  @FARZINgang
    1043960498,  #  @k_matyakubovna
    1478140758,  #  @N000000N
    1753693867,  #  @abdu_rakhmanov01
    1930259184,  #  @amr1dinov
    2007435679,  #
    5095915538,  #
    611550584,  #
    1141982607,  #  @erkinov_umar
    1479016106,  #  @marufjanov_ms
    5374989863,  #  @shoh_0704
    5899329403,  #
    787040419,  #  @Yona_kulova
    1013246308,  #  @baxtiyor130
    1240752083,  #  @koteika_maay
    5532601942,  #  @Thomas_sheellby
    5561434769,  #  @xs_tursunovf
    1308358547,  #  @d_musicha
    5632663527,  #  @future_successfully_person
    5686953610,  #
    5092769929,  #
    1178115284,  #  @Suyunbek04_16
    5339402906,  #
    5854256330,  #  @sofiyatun_bintu_tolqin
    669873461,  #  @U_M_A_R_B_E_Y
    5397221624,  #  @murtozayev_phenomenal
    5477105084,  #  @polatbekov_1
    5627347768,  #  @Adham8006
    550088349,  #  @djuraeva_sanobar
    573783158,  #  @AlhamdulillahBRR
    1078843013,  #  @Abduvali_07
    1305728374,  #  @Xojiakbar012
    5702540085,  #  @hohuuv
    1303262407,  #  @ST02Y
    2090513582,  #  @B0t1rbe_01
    5245484706,  #  @Tixo1noch
    5387504014,  #  @azizjan2305
    5398339181,  #
    5689148424,  #  @raxmona1iyev
    5796476321,  #
    711448444,  #  @Bakiyev_pro
    1860617919,  #
    5370773716,  #
    5374203258,  #  @Validjanov
    5662678389,  #  @Redshad0w758
    5766117079,  #  @MvdN1
    5822445848,  #
    464747865,  #
    1284109214,  #  @Sanjarbek_27
    1439952983,  #
    1738358113,  #  @ake_fx0001
    5074585686,  #
    5980096478,  #
    811572377,  #  @bekhruz_0430
    826713034,  #
    1130066028,  #
    1749854753,  #  @RiChuz001
    5278793258,  #  @ADT_22_01_03
    621518273,  #  @UZB9712
    2064548243,  #  @diyor_bek0923
    5305353949,  #  @ibrokhimov_0506
    5604131246,  #  @Shakx_mee
    5979886924,  #
    5580219311,  #  @ke0mik
    126277366,  #  @Dilrabo_2201
    862893817,  #
    1397347102,  #  @xushnazarov_farrux
    2102538058,  #  @OzOdjOn_AkEE
    5208892613,  #  @finickym
    5286651906,  #  @Kenanbey07
    5815819711,  #
    5950845522,  #
    635212704,  #
    1058967534,  #  @davronov11_05
    1731027341,  #  @Dj_Siroj
    5005048700,  #  @MR20066
    5208677100,  #  @javohir00006
    5459193407,  #
    5722843632,  #  @tojiboyev13
    1197612015,  #  @kabul_janoff
    1488477388,  #  @W_lxxxl_W
    1782784406,  #
    1832402817,  #
    5143693420,  #  @moliya_2023
    5331812045,  #
    468909110,  #  @CEMETERY_002
    625918533,  #  @uncleflexxxxx
    1078214904,  #  @od1lbey_678
    1238794659,  #  @SARDORAKAMuuu
    1417585455,  #  @abdumalikov_aa
    1754949769,  #
    1831646246,  #  @WF_CREEDOV
    5359160043,  #  @RoVa_4240
    5522183122,  #  @Ukra1n_ka
    5534755495,  #  @alogarh_7
    5836894247,  #
    1809376887,  #  @soliyev_yaxshi_bola
    5101168278,  #
    5337331810,  #  @Lone1y_pubger
    996608783,  #  @Akhi_almuslim
    1839084287,  #  @R1zA_WOLF_07
    1870847922,  #  @Ergashyev_700
    1880609932,  #  @Drowner_cat
    2128957074,  #  @abdurahmon_302
    5234597539,  #  @molchi_uje
    5782074401,  #  @Sonikovich228
    5881349723,  #
    5884213839,  #  @ibrohim_0724
    978379139,  #  @Ulugbek_Matkarimov
    1072713018,  #  @SAMBIS6862
    1258314274,  #  @narkulv
    1287787315,  #  @Sobirov_solo
    1320358653,  #
    1341892991,  #  @Muci_29
    1714800819,  #
    5242385343,  #  @Haydo57
    5301167140,  #  @Iknoweverything_originally
    5342407076,  #  @d_hotamov_2
    5480257632,  #  @Bilmeqopman
    5679176701,  #
    974233260,  #
    1321511031,  #
    1814197788,  #  @dobriy5650
    5557327091,  #  @Kit0p
    5603064451,  #
    5947864536,  #  @anta_fiy_RUHIY
    498904957,  #  @Xamid2930
    976009462,  #  @doni_155
    1470653244,  #
    1696660808,  #  @Boom_3765
    5032540639,  #  @Sharofiddinov_Sarodor
    5554437001,  #  @Kucha_bolasi007
    5563047782,  #
    880535502,  #  @the_zaripoff
    1094284469,  #
    1147368254,  #  @nnastww
    1372598956,  #
    1951583952,  #  @fakhriddinnn
    5155018863,  #  @aslerzs
    5427422143,  #  @Antoni1123
    5513462749,  #
    1336484080,  #  @Azizbek07007
    1995166610,  #  @WoN_NaMaNGaN
    2142516868,  #
    5453828791,  #
    1757487612,  #  @Oyning_qizlari
    5045011319,  #  @shodmonovasror
    5165431053,  #
    5732054213,  #  @musobekfarha
    1037093495,  #
    1142336069,  #
    1436943160,  #  @Asadbek_usmonov
    1475659710,  #  @schadofwieend
    1646457756,  #  @Deimos_fans
    1731821043,  #  @gnjnvm
    1746796767,  #  @TERREN_24_43
    1926062333,  #  @rizayev00
    5093145281,  #  @Im_a_loner
    5307619291,  #  @SOLIHvs
    5371322174,  #
    5493549778,  #
    5539578572,  #
    5647762484,  #
    5709865337,  #
    5761776543,  #  @samarqand_1004
    5823056025,  #
    5877637059,  #
    340266706,  #  @AGENT_ROMA
    739847051,  #  @Benjamin_Dc2700
    868228830,  #  @Mehrojbek_27
    921767647,  #  @Ibrohim0223
    939119338,  #  @mr_abdulazizov
    1227488610,  #  @geograflar_choyxonasi
    1237816675,  #  @Olovv_001
    1257217444,  #  @hisssq
    1411994110,  #  @nncrusher
    1506184619,  #  @SonikovayaFederaciya
    1614838347,  #  @Sogingan_qalblar_manzil1
    1669952456,  #  @Eva_Dobrotivnaya
    1717862169,  #  @Sogingan_qalblar_manz1li
    1744714318,  #
    5015878907,  #
    5087071778,  #
    5129683110,  #  @Kolobikpon
    5138827356,  #  @xnoVENOM707
    5158538372,  #  @Islom_14_08
    5195684842,  #
    5254562102,  #  @Ki_ng2001
    5271449638,  #
    5296546396,  #  @AnotherWorld13
    5308969903,  #  @Xayrulloh0070
    5346119040,  #  @Zukhriddin_Abdupattoyevich
    5422264712,  #  @n_imron_j
    5431112717,  #
    5463896830,  #  @Gusecham
    5511246702,  #  @M_Toraboyeva
    5532856791,  #  @hungry_2
    5593857148,  #
    5645135290,  #
    5652888029,  #  @diller9909
    5845657324,  #  @Rustamovnas
    1592701634,  #  @LuckyBoy_2004
    5750845627,  #  @zimjon_06
    5271171753,  #  @Ali_8383
    5812443478,  #
    731087684,  #
    1340918459,  #
    1839709894,  #  @ophlic
    5264679816,  #  @Kasha_000
    754153254,  #
    1039784929,  #  @Xurshidbek_9595
    2100336996,  #  @idovud
    5355818461,  #
    5017981716,  #
    443867637,  #  @Mukhsinbakhriddinov
    975316761,  #
    2063875389,  #  @humaiira_sh
    158658404,  #  @asror_ENEMY
    411192446,  #  @Saidjonov_24
    1889160231,  #
    5565790731,  #
    5749237396,  #  @Banda3040
    146786796,  #
    5014574419,  #  @abr0r_me
    5744014049,  #
    289417222,  #  @Salixi012
    1931943307,  #  @SaMuraI_46
    1314278556,  #  @mirzobek_mirzo
    5122311260,  #  @chorniy_delfin_00
    5386905095,  #
    5554361950,  #  @Abdullo_11_01
    5547779632,  #
    826508302,  #  @Rameshovich
    5188975586,  #  @Samarkanski_geroy
    5436426968,  #  @Eynshteyn1
    5473637551,  #
    5603123122,  #
    232642941,  #  @Salokhidinov_777
    5374175029,  #  @UAENAo
    5739650473,  #  @valiyev_2000
    5279579873,  #
    5730995471,  #  @khasan_007
    1095129177,  #
    766574122,  #
    1264487238,  #  @G0lden_g1rl
    1476159444,  #  @fatkhullox
    1925180246,  #  @khudoyberdiyev_017
    2033723185,  #  @Fedya_6068
    5212471557,  #  @Bekh_0881
    5416344305,  #
    887455800,  #
    902863705,  #  @Azizbek_20023003
    1303642600,  #
    5592140211,  #  @shorax06
    5899386827,  #  @MuxiddinovaMadinabonu1
    1057359869,  #  @mrjamolibodullayevich
    5663180769,  #
    1318255167,  #
    1463304116,  #
    5115021610,  #  @qo6ul10n
    5320674372,  #
    5754353202,  #  @Surikenchik_2
    730942129,  #  @komijonov_nodir_001
    5093055537,  #
    5560904103,  #  @usa_039
    5705548152,  #
    5525497432,  #  @nizomiddinov_0
    5757398066,  #
    1054866090,  #
    5497600093,  #  @Rustamovna_4868
    5972200965,  #  @SpartanTagUA
    626398733,  #  @buhgalteriya_xizmatlari
    2048806213,  #
    5082261335,  #  @sarvarbek0vich
    1069976168,  #
    5079988952,  #  @muxammad_ake_07
    5470962201,  #
    5728745604,  #
    5987317304,  #
    858895711,  #
    974267783,  #  @szufar_0001
    5676946888,  #  @Mad1na_oo5
    5961675210,  #
    5231613246,  #  @Elbek_1114
    5421902019,  #  @n1kotin_o7
    5474598820,  #  @Prog_08
    1769618176,  #
    5443483169,  #  @sila_786
    5735003204,  #
    1687501021,  #  @Solihamni_solihimanda
    5839852418,  #  @Zukhra_442
    1935940305,  #  @sabiroff_abu
    750431351,  #  @S_Mominova
    1489493075,  #  @azizhan_005
    5516664968,  #  @xoroshkamaa
    5670683210,  #  @Mamadaliyev_Aziz
    696060621,  #  @ta1atov_13
    912928274,  #
    965377330,  #  @Usmonov_228
    1387382324,  #  @azimov_0124
    1933541482,  #
    1726724023,  #  @BAKHRILLAEV
    1988712299,  #  @Azim_1115
    506680546,  #  @Sherzod_0101
    5192474165,  #  @musulmanka_vd
    5237127345,  #  @Denovred
    5774574566,  #
    5782018488,  #
    1406133561,  #  @MADAMINBEK_NARZULLAYEV_7878
    5280671111,  #  @Shar1pov_Jora
    5716488392,  #  @Shuhratoc
    5987103460,  #
    951305027,  #  @e_sherzod
    1007930422,  #  @DrAbdulGoffar
    1116695741,  #  @LetinantTojiyev
    1123155908,  #  @javohir_haminjonov
    5767406537,  #  @Kaktus_24_10
    5344805682,  #  @SBA705
    5502325690,  #  @Xd_luchshiy
    5514594820,  #
    610051843,  #  @May_o2
    5710992293,  #  @D1yorbe_005
    5986005274,  #  @abduganiev_07
    1426979065,  #  @boburmirzo_axmadjonov
    1475810753,  #
    5347586640,  #
    5531484740,  #  @UKTAMBEK003
    5708494819,  #
    5969127351,  #  @Dahoman_04
    529086710,  #  @No_name_14_12k
    1141352542,  #  @ALoNe_Okeng
    1400546781,  #
    5062788219,  #  @Muhsinbey_av
    5207875942,  #
    5434231622,  #
    5675777147,  #
    5914024139,  #  @Muheyyatun
    546154049,  #
    625657172,  #
    717703701,  #  @shoxdeee
    754992743,  #  @E1muroD
    823632664,  #
    5653993197,  #
    5694452246,  #  @ominas_kitchen
    528958270,  #  @Mironshoh9013
    1043822700,  #  @Usoma571
    1058063966,  #  @Cila_00
    1282177804,  #  @Dalerbek2006
    1403692653,  #  @Abu_1_zar
    5267366121,  #  @https_gulomovz004
    5525806672,  #  @marifovm2
    5669129720,  #
    645625368,  #  @JOLDASBAEV2023
    2027217381,  #  @filarx_08
    5245450205,  #
    5605678192,  #  @Bobur_Gofurovich
    642640494,  #
    5650027221,  #  @Sevgim_sen_bilan_J_D
    5663933547,  #  @Umid_Otanazarovich
    2024738812,  #
    5678691165,  #  @unknown_o5
    5700724038,  #
    5770560709,  #
    5796651409,  #  @FAZLI_PUBG
    847152203,  #  @Lord707
    1283093704,  #  @Samatchk
    1937205594,  #  @diyorbek_04_29
    2120633633,  #  @Yaramas_111
    5245270255,  #  @joxa_baxmaliski
    5512674055,  #
    5610360238,  #
    652738543,  #  @ABDULBORIY1111
    936870947,  #  @sardor_30
    1076038726,  #  @Abdurahimov_07_29
    1492241513,  #  @Muzaffar1910
    1682970568,  #
    1919791740,  #  @Kzenox_1
    1933853762,  #  @Dr_Fozil0v
    5297363979,  #  @urakbaev_07
    5305109163,  #
    5587250036,  #
    5785256920,  #  @XSHD_0211
    948923137,  #  @M1rjamol_03_30
    968031214,  #  @dizayner_ezoza_lotos_646
    1916694554,  #  @Bekmurodovna
    5106811574,  #  @bakxtyarovic
    1187234305,  #  @avazbek_handsome_1105
    1339219960,  #  @muxtorovich_1
    1481003470,  #  @itsmakhira
    1629533661,  #  @A_A_571
    1759594554,  #  @odilzhonov
    5377848403,  #
    5570173111,  #  @Malluriem
    5650134708,  #  @abboskhuja_01
    5650358566,  #
    5834497533,  #  @brigada_50
    5838281053,  #
    338276710,  #  @Doniyor_1110
    1135544512,  #
    2005314077,  #  @sarvarbek_2_0_0_7
    5451099036,  #  @arz1kulov07
    5560519596,  #
    5642487722,  #
    5697764123,  #  @Qudratova_S
    5889248421,  #  @temur_0555
    894165273,  #  @ALFAFARRUX
    1460669562,  #  @MD_3750
    5050413499,  #
    5297743017,  #  @ShaxloD555
    5472795023,  #  @Boburc1k_13o8
    5535790861,  #  @malita01
    5670797626,  #  @S_zokirov
    5972112411,  #
    449429871,  #  @tg_maks1
    1047739525,  #  @Boboxojayev
    1080881879,  #  @Jamila1812
    2015847551,  #  @Anasx0n
    5337292361,  #  @BFS_2555
    5431558279,  #  @Ozodboyev
    5492621314,  #  @Moliyachi04
    5574091539,  #  @its_bek
    462272715,  #  @asqarov_98
    5332772258,  #  @xasanboy0309
    5638340742,  #  @taiirov_m
    5297322986,  #  @megaway111
    5930367518,  #
    1150549653,  #
    1279598605,  #  @Kamoliddin_Bektemirov
    1455070327,  #  @jamilov_05
    5011541818,  #
    5261246542,  #
    5504686166,  #  @snayper_dxx_chq_7_31
    5544204793,  #
    5581181781,  #
    104054992,  #  @Abu_737
    819591177,  #
    1228440702,  #  @Abdujabborov06
    5259841265,  #  @Diyor_5474
    5776795908,  #  @mansurj09
    831984582,  #  @MRG_9229
    1662707284,  #
    5543051277,  #
    5669054830,  #  @hayotim0930
    1663303279,  #
    5134030207,  #  @alligarx_0603
    2067718242,  #
    5311372062,  #  @M_I_R_O_N_28
    5580440945,  #
    5715101971,  #
    1378853167,  #
    1730670804,  #  @ed_w1nner
    5192461854,  #  @AC130_DEXTER
    5790818085,  #
    1704764320,  #  @World_Den20888
    1217549899,  #  @aliff_zdes
    1318449068,  #  @quettte
    1506394348,  #  @diary_intizor_02
    5625958865,  #  @Turaboyev_7771
    5854950707,  #
    505876317,  #  @ib0dullayev_i
    1657563924,  #  @EgorGonki
    5636402796,  #  @Nasridinov_712
    5751713459,  #
    1459862067,  #  @mainkraft1248
    1753109802,  #  @Jamshidbek_Tuxtasinov
    5498505819,  #  @GalaGamerX
    5578581385,  #
    5788668986,  #  @BesTboy1120
    657479061,  #  @stup1d_creazy
    1416347807,  #  @Qalbim_nuri_sen_02
    1639163189,  #  @Ibrohimbek_Xolmatov
    5098057048,  # @Rick_and_morty_top
    5028933874,  # @SHAVKATCHEK_3797
    2132420671,  # @lacoste_ccc
    5327888505,  # @Oybekjon_okeng1
    5403730187,  # @Shaxrizoda_23_07_005
    1721306171,  # @o8_fucking_craig_8o
    5521578253,  # @Mitiska0
    858429515,  # @Rasulxon111
    1741943323,  # @blevotnii_krisomet
    5542155675,  # @d1ya_mee
    5604323737,  # @Jumayev_04_45
    1930535811,
    5520162331,  # @doston_akmalov
    5598547114,
    5825337677,
    5805849910,  # @hayothalioldinda_04
    921260666,  # @ibluecup
    1480890740,  # @VitoriaRFreitas
    5782736362,
    1395659069,  # @Mister_Mc
    5110811319,  # @Iymonliyigit00
    237112893,  # @Raufxonjon
    1303726738,  # @Nurbolat2601
    1681117508,  # @Ibragimov_Aziz
    5355937564,  # @BEKH_ruz_BEKH
    1648083094,  # @A70G71G98J97GULI01Mubina21
    1836576856,
    1871181631,  # @nonsonoiod
    5859782112,  # @prosta_sabr_02
    5354284285,  # @akow_02_04
    738705870,  # @resilient717
}


def _ensure_dir(dirname, filename=""):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    return os.path.join(dirname, filename)


def get_client(client):
    if client is None:
        return BotClient.instance()
    if isinstance(client, BotClient):
        return client
    if isinstance(client, SyncClient):
        return client.async_client
    if isinstance(client, TelegramObject):
        return client.client
    return BotClient.instance(client)


def animated_sticker_file(animation):
    fileobj = io.BytesIO()
    export_tgs(animation, fileobj)
    fileobj.seek(0)
    return fileobj


def static_sticker_file(image):
    return photo_file(image, "WebP")


def photo_file(image, format, background=(0, 0, 0, 0)):
    if isinstance(image, lottie.objects.Animation):
        data_png = io.BytesIO()
        export_png(image, data_png)
        data_png.seek(0)
        image = Image.open(data_png)

    out_image = io.BytesIO()
    image.save(
        out_image,
        format=format,
        background=background,
    )
    out_image.seek(0)
    out_image.name = "file." + format.lower()
    return out_image



class TelegramObject:
    def __init__(self, client: TelegramClient):
        self.client = client


class TelegramEvent(TelegramObject):
    type = "unknown"

    def __init__(self, client, event):
        super().__init__(client)
        self.event = event
        self.query = None

    def __str__(self):
        return "Telegram Event: %s" % self.type

    @property
    def chat(self):
        return self.event.chat

    def __str__(self):
        return "Telegram Message Event: %s %s" % (self.type, self.query)


class MessageChunk(TelegramObject):
    def __init__(self, client, text, entity):
        super().__init__(client)
        self.text = text
        self.entity = entity
        self.is_mention_name = isinstance(self.entity, MessageEntityMentionName)
        self.is_mention_username = isinstance(self.entity, MessageEntityMention)
        self.is_mention = self.is_mention_name or self.is_mention_username
        self.is_text = entity is None

    async def load(self):
        if isinstance(self.entity, MessageEntityMentionName):
            self.is_mention = True
            self.mentioned_user = await self.client.get_entity(self.entity.user_id)
            self.mentioned_id = self.entity.user_id
            self.mentioned_name = self.text
        elif isinstance(self.entity, MessageEntityMention):
            try:
                self.mentioned_user = await self.client.get_entity(self.text)
                self.mentioned_id = self.mentioned_user.id
                self.mentioned_name = self.client.user_name(self.mentioned_user)
            except Exception:
                self.mentioned_user = None
                self.mentioned_id = None
                self.mentioned_name = self.text

    def __repr__(self):
        return "<MessageChunk %r%s>" % (
            self.text,
            " " + self.entity.__class__.__name__ if self.entity else ""
        )


class NewMessageEvent(TelegramEvent):
    type = "message"

    def __init__(self, client, event):
        super().__init__(client, event)
        self.text = event.text
        self.command = ""
        self.query = ""

        if self.text.startswith("/"):
            command = self.text[1:]
            if " " in self.text:
                command, self.query = self.text[1:].split(" ", 1)
            self.command = command.split("@", 1)[0]

    async def reply(self, *a, **kw):
        return await self.event.reply(*a, **kw)

    async def parse_text(self):
        """
        Returns a list of MessageChunk
        """
        if not self.event.entities:
            return [MessageChunk(self.client, self.text, None)]
        chunks = []
        start_index = 0
        for entity in self.event.entities:
            if start_index < entity.offset:
                chunks.append(MessageChunk(self.client, self.text[start_index:entity.offset], None))
            end_index = entity.offset + entity.length
            text = self.text[entity.offset:end_index]
            start_index = end_index
            chunks.append(MessageChunk(self.client, text, entity))

        if start_index < len(self.text):
            chunks.append(MessageChunk(self.client, self.text[start_index:], None))
        return chunks


class InlineQueryEvent(TelegramEvent):
    type = "inline_query"

    def __init__(self, client, event):
        super().__init__(client, event)
        self.builder = event.builder
        self.query = event.text
        self.results = []

    ## see telethon.tl.custom.inlinebuilder.InlineBuilder for info
    def article(self, *a, **kw):
        self.results.append(self.builder.article(*a, **kw))

    def photo(self, *a, **kw):
        self.results.append(self.builder.photo(*a, **kw))

    def document(self, *a, **kw):
        self.results.append(self.builder.document(*a, **kw))

    def game(self, *a, **kw):
        self.results.append(self.builder.game(*a, **kw))

    def animated_sticker(self, file):
        self.results.append(self.builder.document(
            file, "",
            mime_type="application/x-tgsticker",
            type="sticker",
            attributes=[
                DocumentAttributeFilename("sticker.tgs")
            ]
        ))

    def sticker(self, file):
        file.name = "sticker.webp"
        self.results.append(self.builder.document(
            file, "",
            mime_type="image/webp",
            type="sticker",
            attributes=[
                DocumentAttributeFilename("sticker.webp")
            ]
        ))


class CallbackQueryEvent(TelegramEvent):
    type = "callback_query"

    def __init__(self, client, event):
        super().__init__(client, event)
        self.query = self.event.data.decode("utf-8")
        self._message = None

    async def edit(self, *args, **kwargs):
        return await self.event.edit(*args, **kwargs)

    async def load_source_message(self):
        """
        Returns the message object the button was attached to
        """
        if not self._message:
            self._message = await self.event.get_message()
        return self._message

    @property
    def message_text(self):
        """
        Returns the text of the message the button was attached to
        @pre: load_source_message() completed
        """
        return self._message.text


class BotExceptionLog:
    def __init__(self, exception, trace):
        self.exception = exception
        self.time = datetime.datetime.now()
        file = io.StringIO()
        traceback.print_tb(trace, file=file)
        self.traceback = file.getvalue()

    def __str__(self):
        return "%s Bot Exception: %s\n%s\n%s" % (
            self.time.isoformat(),
            self.exception.__class__,
            self.exception,
            self.traceback
        )


class ExceptionManager:
    def __init__(self):
        self.exceptions = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, trace):
        self.log_exception(exc_type, exc_value, trace)

    def log_exception(self, exc_type, exc_value, trace):
        if exc_value:
            log = BotExceptionLog(exc_value, trace)
            sys.stdout.write(str(log))
            sys.stdout.flush()
            self.exceptions.append(log)

    def log_current_exception(self):
        self.log_exception(*sys.exc_info())


class Message:
    def __init__(self, client, recipient, text=""):
        self.client = client
        self.recipient = recipient
        self.text = text
        self.reply_markup = None
        self.reply_to = None

    def send(self):
        kwargs = {}

        if self.reply_to:
            kwargs["reply_to"] = self.reply_to

        if self.reply_markup:
            kwargs["buttons"] = self.reply_markup.to_data()

        return self.client.sync.send_message(
            self.recipient,
            self.text,
            **kwargs
        )

    def add_inline_keyboard(self):
        self.reply_markup = InlineKeyboard()
        return self.reply_markup


class MessageMarkup:
    def to_data(self):
        raise NotImplementedError


class InlineKeyboard(MessageMarkup):
    def __init__(self):
        self.rows = []

    def add_row(self):
        self.rows.append([])

    def add_button(self, button, row):
        if not self.rows:
            self.add_row()

        self.rows[row].append(button)

    def add_button_url(self, *args, row=-1, **kwargs):
        self.add_button(KeyboardButtonUrl(*args, **kwargs), row)

    def add_button_callback(self, text, data, row=-1):
        self.add_button(KeyboardButtonCallback(text, data), row)

    def to_data(self):
        return self.rows


class SyncClient:
    def __init__(self, async_client: "BotClient"):
        self.async_client = async_client

    @property
    def token(self):
        return self.async_client.token

    @property
    def me(self):
        return self.async_client.me

    def send_message(self, *a, **kw):
        result = controller.get_result(self.async_client.send_message(*a, **kw))
        if result:
            controller.get_result(result.get_chat())
        return result

    def request_result(self, request):
        return controller.get_result(self.async_client(request))

    def get_my_commands(self):
        r = self.request_result(functions.users.GetFullUserRequest(self.me))
        return r.bot_info.commands

    def set_my_commands(self, commands):
        self.request_result(functions.bots.SetBotCommandsRequest(BotCommandScopeDefault(), "en", commands))


class FloodStats:
    def __init__(self, sender):
        self.count = 1
        self.id = sender.id
        self.username = ("@" + sender.username) if getattr(sender, "username", "") else ""
        self.first = time.time()
        self.last = self.first
        self.rate_count = 1
        self.rate = 0.01
        self.blocked = False
        self.queries = {}

    def __lt__(self, other):
        return (-self.count, self.id) < (-other.count, other.id)

    def add(self, query):
        self.count += 1
        self.rate_count += 1
        self.last = time.time()

        self.queries[query] = self.queries.get(query, 0) + 1

        if self.first == self.last:
            self.rate = 100
        else:
            delta = self.last - self.first
            rate = self.rate_count / delta
            if rate > self.rate:
                self.rate = rate
            elif delta > 10:
                self.rate_count = 1
                self.first = self.last

        if self.rate > 5:
            self.blocked = True
            global_ban_list.add(self.id)
            return False

        return True

    def top_queries(self):
        return sorted(self.queries.items(), key=lambda it: -it[1])[:5]


class BotClient(TelegramClient):
    _auto_instance = {}
    _default_media_path = os.path.join(settings.MEDIA_ROOT, "telegram")
    _default_media_url = os.path.join(settings.MEDIA_URL, "telegram")
    _default_storage = FileSystemStorage(_default_media_path, _default_media_url)
    do_not_call_in_templates = True

    def check_flood(self, sender, query):
        return True

        if not sender:
            return False

        if sender.id == 240009458:
            return True

        if sender.id in global_ban_list:
            return False

        if sender.id not in self._stats:
            self._stats[sender.id] = FloodStats(sender)
        elif not self._stats[sender.id].add(query):
            # Added to the ban list
            #self._stats.pop(sender.id)
            return False

        return True

    def query_stats(self):
        return list(sorted(self._stats.values()))

    def __init__(self, bot_token, bot_runner, storage=_default_storage):
        session_file = None # _ensure_dir("/tmp/sessions", bot_token + ".session")
        super().__init__(session_file, settings.TELEGRAM["API_ID"], settings.TELEGRAM["API_HASH"])
        self.bot_id = id(self)
        self.token = bot_token
        self.storage = storage
        self.sync = SyncClient(self)
        self._debug = False
        self._lock = threading.Lock()
        self.me = None
        self.bot_runner = bot_runner
        self._must_enable = False
        self.exception_manager = ExceptionManager()
        self._stats = {}

    @property
    def exceptions(self):
        return self.exception_manager.exceptions

    def __repr__(self):
        return "<BotClient %s>" % self.bot_id

    async def connect(self):
        await super().connect()
        me = await self.get_me()
        if me:
            await self._on_me(me)

    async def sign_in(self, *a, **kw):
        me = await super().sign_in(*a, **kw)
        await self._on_me(me)
        return me

    async def _on_me(self, me):
        self.me = me
        self.bot_id = self.me.username

        filename = "profiles/%s.jpg" % self.me.id
        self.storage.delete(filename)
        _ensure_dir(os.path.dirname(self.storage.path("profiles")))
        self.me.profile_photo = self.storage.open(filename, "wb")

        await self.download_profile_photo(self.me, self.me.profile_photo)
        self.me.profile_photo.close()
        self.me.profile_photo.url = self.storage.url(filename)
        self.me.client = self
        self.me.link = "https://t.me/%s" % self.me.username

        if self.bot_runner is None:
            self.bot_runner = get_bot_runner(self.me.username)

        await self.bot_runner.on_loaded(self)
        if self._must_enable:
            self._must_enable = False
            self.bot_runner.enabled(self)

        commands = self.bot_runner.get_command_data()
        await self(functions.bots.ResetBotCommandsRequest(BotCommandScopeDefault(), "en"))
        await self(functions.bots.SetBotCommandsRequest(BotCommandScopeDefault(), "en", commands))
        print("Bot Loaded %s %s" % (self.me.username, "*" if self.debug else ""))

    def command_list(self):
        return get_command_data(self.me.username)

    async def _on_new_message(self, event):
        with self.exception_manager:
            if not self.check_flood(event.message.sender, event.text):
                return
            await run_command(NewMessageEvent(self, event), self.bot_runner)

    async def _on_inline_query(self, event):
        with self.exception_manager:
            if not self.check_flood(event.sender, event.text):
                return
            hlev = InlineQueryEvent(self, event)
            await run_command(hlev, self.bot_runner)
            if hlev.results:
                await event.answer(hlev.results)

    async def _on_callback_query(self, event):
        with self.exception_manager:
            await run_command(CallbackQueryEvent(self, event), self.bot_runner)

    def enable(self):
        self.add_event_handler(self._on_new_message, events.NewMessage)
        self.add_event_handler(self._on_inline_query, events.InlineQuery)
        self.add_event_handler(self._on_callback_query, events.CallbackQuery)
        if self.bot_runner:
            self.bot_runner.enabled(self)
        else:
            self._must_enable = True

    def disable(self):
        self.remove_event_handler(self._on_new_message, events.NewMessage)
        self.remove_event_handler(self._on_inline_query, events.InlineQuery)
        self.remove_event_handler(self._on_callback_query, events.CallbackQuery)
        self._must_enable = False
        if self.bot_runner:
            self.bot_runner.disabled(self)

    async def prepare_run(self, max_attempts=10):
        for attempt in range(max_attempts):
            try:
                if attempt > 0:
                    print("Start attempt %s of %s" % (attempt+1, max_attempts))
                await self.start(bot_token=self.token)
                return True
            except Exception as e:
                if hasattr(e, "seconds"):
                    wait = e.seconds
                    print("Sleeping for %s before retrying (attempt %s of %s)" % (wait, attempt+1, max_attempts))
                    await asyncio.sleep(wait)
                    continue

                print("Bot initialization error: %s: %s" % (self.bot_id, e))
                import traceback
                traceback.print_exc()
                return False

        print("Bot could not initialize")
        return False

    async def prepare_and_run(self):
        if await self.prepare_run():
            await self.run()

    async def run(self):
        await self.run_until_disconnected()

    @staticmethod
    def instance(name="default"):
        return BotClient._auto_instance[name]

    @staticmethod
    def load_settings():
        from .models import BotDebugMode
        for name, settings_dict in settings.TELEGRAM.items():
            if not isinstance(settings_dict, dict):
                continue

            media_path = settings_dict.get("MEDIA_ROOT", BotClient._default_media_path)
            media_url = settings_dict.get("MEDIA_URL", BotClient._default_media_url)
            bot = BotClient(
                settings_dict["TOKEN"],
                None,
                FileSystemStorage(media_path, media_url),
            )
            bot = controller.add_bot(bot)
            bot.enable()
            BotClient._auto_instance[name] = bot
            bot.debug = BotDebugMode.objects.get_or_create(token=bot.token)[0]

    async def send_animated_sticker(self, chat, file, *a, **kw):
        return await self.send_file(chat, file, attributes=[
            DocumentAttributeFilename("sticker.tgs")
        ], *a, **kw)

    async def send_sticker(self, chat, file):
        file.name = "sticker.webp"
        return await self.send_file(chat, file, force_document=False, attributes=[
            DocumentAttributeFilename("sticker.webp")
        ])

    @property
    def debug(self):
        with self._lock:
            return self._debug

    @debug.setter
    def debug(self, value):
        with self._lock:
            self._debug = value

    def user_name(self, user):
        name = user.first_name or ""
        if user.last_name:
            name += " " + user.last_name

        name = name.strip()
        if name:
            return name

        if user.username:
            return user.username

        return "Unnamed user"

    async def set_admin_title(self, chat, user, title):
        user = await self.get_input_entity(user)
        entity = await self.get_input_entity(chat)
        rights = ChatAdminRights(other=True)
        return await self(functions.channels.EditAdminRequest(entity, user, rights, rank=title))

    def digest(self, string):
        secret_key = hashlib.sha256(self.token.encode("ascii")).digest()
        return hmac.new(secret_key, string.encode("utf-8"), hashlib.sha256)

    def auth_check(self, data, max_age_seconds=24*60*60):
        if "id" not in data or "hash" not in data or "auth_date" not in data:
            return False

        if isinstance(data, QueryDict):
            data = data.dict()
        hash = data.pop('hash')

        data_check = sorted(
            "%s=%s" % (key, value)
            for key, value in data.items()
        )

        data_check_string = "\n".join(data_check)

        if hash != self.digest(data_check_string).hexdigest():
            return False

        now = timezone.now()
        if max_age_seconds > 0 and time.mktime(now.timetuple()) - float(data['auth_date']) > max_age_seconds:
            return False

        return True

    def auth_get_user(self, data, max_age_seconds=24*60*60):
        if not self.auth_check(data, max_age_seconds):
            return None

        photo = data.get("photo_url", "")
        return TelegramAuthedUser(self, data["id"], photo, data)

    @staticmethod
    def instances():
        return list(BotClient._auto_instance.values())


class TelegramAuthedUser(TelegramObject):
    def __init__(self, client, user_id, photo_url, data):
        super().__init__(client)
        self.user_id = user_id
        self.from_description(data)

        self.display_name = ("%s %s" % (self.first_name, self.last_name)).strip()
        if not self.display_name:
            if self.username:
                self.display_name = "Anonymous User #%s" % self.id
            else:
                self.display_name = self.username

        self.icon_url = photo_url
        self.data = data

    def _refresh(self):
        pass

    def login(self, request):
        from .models import TelegramLogin
        tglogin = TelegramLogin.objects.filter(telegram_id=self.user_id).first()
        if tglogin:
            tglogin.user.login()
            login(request, tglogin.user)
            return tglogin.user
        return None

    def from_description(self, description):
        self.is_bot = description.get("is_bot", False)

        self.first_name = description.get("first_name", None)
        self.last_name = description.get("last_name", None)

        if "username" in description:
            self.username = description["username"]
            self.link = "https://t.me/%s" % self.username
        else:
            self.username = None
            self.link = None


class Controller(threading.Thread):
    def __init__(self):
        super().__init__()
        self._bots = {}
        self._running = False
        self._loop = asyncio.get_event_loop()
        #self._loop = asyncio.new_event_loop()
        self._lock = threading.Lock()
        self._cor = {}

    def set_loop(self):
        asyncio.set_event_loop(self._loop)

    def get_loop(self):
        return self._loop

    def add_bot(self, bot, bot_runner=None):
        if isinstance(bot, BotClient):
            token = bot.token
        else:
            token = bot
            asyncio.set_event_loop(self._loop)
            bot = BotClient(token, bot_runner)

        with self._lock:
            if token in self._bots:
                return self._bots[token]

            self._bots[token] = bot
            if self._running and threading.get_ident() != self.ident:
                self.get_result(bot.prepare_run())
                self._cor[token] = self.execute(bot.run())
            else:
                cor = bot.prepare_and_run()
                self._cor[token] = self.execute(cor)

        return bot

    def run(self):
        print("Bot Controller Started")
        from . import models
        asyncio.set_event_loop(self._loop)
        BotClient.load_settings()
        for bot in models.TelegramUserBot.objects.filter():
            advanced_bot = getattr(bot, "advanced_bot", None)
            bot_runner = None
            if advanced_bot:
                bot_runner = advanced_bot.get_runner()

            bot_client = controller.add_bot(bot.token, bot_runner)

            if bot.enabled:
                bot_client.enable()

        with self._lock:
            self._running = True

        self._loop.run_forever()

        with self._lock:
            self._running = False
        print("Bot Controller Finished")

    def stop(self):
        print("Bot Controller Stopping")
        with self._lock:
            for bot in self._bots.values():
                self.execute(bot.disconnect())
            self._bots = {}

        self._loop.call_soon_threadsafe(self._loop.stop)

    def bot(self, token):
        with self._lock:
            return self._bots.get(token, None)

    def remove_bot(self, bot):
        with self._lock:
            self._bots.pop(bot.token)
            self._cor.pop(bot.token, 0)
        self.execute(bot.disconnect())

    @property
    def bots(self):
        with self._lock:
            return list(self._bots.values())

    def execute(self, coroutine):
        if threading.get_ident() == self.ident:
            return asyncio.ensure_future(coroutine, loop=self._loop)
        else:
            return asyncio.run_coroutine_threadsafe(coroutine, self._loop)

    def get_result(self, coroutine):
        return self.execute(coroutine).result()


class MessageFormatter:
    class EntityTag:
        def __init__(self, entity_type, formatter, **kwargs):
            self.type = getattr(telethon.tl.types, "MessageEntity" + entity_type)
            self.formatter = formatter
            self.offset = 0
            self.kwargs = kwargs

        def open(self):
            self.offset = self.formatter.offset

        def close(self):
            self.formatter.entities.append(self.type(
                offset=self.offset,
                length=self.formatter.offset - self.offset
            ))

        def __enter__(self):
            self.open()
            return self

        def __exit__(self, ex_type, exc, trace):
            self.close()

    def __init__(self):
        self.text = ""
        self.entities = []
        self.offset = 0

    def __iadd__(self, text):
        self.text += text
        self.offset += self.text_length(text)
        return self

    def _add_entity(self, type, text, **kwargs):
        length = self.text_length(text)
        self.entities.append(type(
            offset=self.offset,
            length=length,
            **kwargs
        ))
        self.text += text
        self.offset += length

    def bold(self, text):
        self._add_entity(telethon.tl.types.MessageEntityBold, text)

    def italic(self, text):
        self._add_entity(telethon.tl.types.MessageEntityItalic, text)

    def strike(self, text):
        self._add_entity(telethon.tl.types.MessageEntityStrike, text)

    def underline(self, text):
        self._add_entity(telethon.tl.types.MessageEntityUnderline, text)

    def code(self, text):
        self._add_entity(telethon.tl.types.MessageEntityCode, text)

    def pre(self, text):
        self._add_entity(telethon.tl.types.MessageEntityPre, text, language="")

    def mention(self, text, user):
        self._add_entity(telethon.tl.types.InputMessageEntityMentionName, text, user_id=user)

    def block_quote(self):
        return self.EntityTag("Blockquote", self)

    @staticmethod
    def text_length(text):
        return len(add_surrogate(text))


controller = Controller()


def autoreload_stop(**kw):
    controller.stop()


file_changed.connect(autoreload_stop)
