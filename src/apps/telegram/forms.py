from django import forms
from django.core.validators import ValidationError
from . import models
from . import connection


class BotChoiceWidget(forms.RadioSelect):
    option_template_name = 'telegram/bot_radio_option.html'

    def __init__(self, attrs=None, choices=()):
        attrs = attrs or {}
        attrs.setdefault("class", "bot-choice")
        super().__init__(attrs, choices)


class BotChoiceField(forms.ChoiceField):
    widget = BotChoiceWidget

    def set_user(self, user):
        self.choices = [
            ("", connection.BotClient.instance())
        ] + [
            (bot.client.token, bot.client)
            for bot in models.TelegramUserBot.objects.filter(user=user)
        ]

    def to_python(self, value):
        for tok, tg in self.choices:
            if tok == value:
                return tg
        return None

    def validate(self, value):
        if not isinstance(value, connection.BotClient):
            raise ValidationError("Invalid bot")


class ComposeForm(forms.Form):
    group = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)
    bot = BotChoiceField()

    def __init__(self, data, user, *a, **kw):
        super().__init__(data or None, *a, **kw)
        self.fields["bot"].set_user(user)


def label_attr_field(label, field, **kw):
    kw["widget"] = field.widget(attrs={"placeholder": label, "title": label})
    return field(**kw)


class InlineKeyboardButtonForm(forms.Form):
    text = label_attr_field("Text", forms.CharField, required=False)
    url = label_attr_field("Link", forms.URLField, required=True)
    new_row = forms.BooleanField(required=False)


InlineKeyboardButtonFormSet = forms.formset_factory(InlineKeyboardButtonForm, can_order=True, can_delete=True, extra=0)


DebugModeForm = forms.modelform_factory(models.BotDebugMode, exclude=["token"])

AdvancedBotForm = forms.modelform_factory(models.AdvancedBot, exclude=["bot"])
