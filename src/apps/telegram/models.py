import inspect
from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import cached_property
from django.utils.module_loading import import_string
from django.core.exceptions import ValidationError

from . import connection


TELEGRAM_ID_BYTES = 32


class TelegramLogin(models.Model):
    user = models.OneToOneField(User, models.CASCADE, related_name="telegram_login")
    telegram_id = models.CharField(max_length=TELEGRAM_ID_BYTES, db_index=True, unique=True)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    username = models.CharField(max_length=128)
    icon_url = models.CharField(max_length=128)

    def get_api_object(self, client=None):
        data = {
            "id": self.telegram_id,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
        if self.username:
            data["username"] = self.username

        return connection.TelegramAuthedUser(
            connection.get_client(client),
            self.telegram_id,
            self.icon_url,
            data
        )


class TelegramUserBot(models.Model):
    user = models.ForeignKey(User, models.CASCADE, related_name="telegram_bots")
    token = models.CharField(max_length=64)
    enabled = models.BooleanField(default=True)

    @cached_property
    def client(self):
        return connection.controller.bot(self.token)

    @property
    def me(self):
        if not self.client:
            return None
        return self.client.me

    class Meta:
        unique_together = [
            ["user", "token"]
        ]

    def __str__(self):
        try:
            return "@" + self.me.username
        except AttributeError:
            return "Bot by " + self.user.username


class BotDebugMode(models.Model):
    token = models.CharField(max_length=64, unique=True)
    debug_enabled = models.BooleanField(blank=True, default=False)
    debug_as = models.ForeignKey(TelegramUserBot, models.SET_NULL, null=True, blank=True)


def _bot_class(name):
    try:
        cls = import_string(name)
    except Exception:
        raise ValidationError("%s is not importable")

    if not inspect.isclass(cls):
        raise ValidationError("%s is not a class")

    from .advanced_bot import AdvancedBot

    if not issubclass(cls, AdvancedBot):
        raise ValidationError("%s is not a subclass of AdvancedBot")


class AdvancedBot(models.Model):
    bot = models.OneToOneField(TelegramUserBot, models.CASCADE, related_name="advanced_bot")
    python_class = models.CharField(max_length=128, validators=[_bot_class], default="apps.telegram.advanced_bot.AdvancedBot")

    def get_runner(self):
        cls = import_string(self.python_class)
        return cls(self)


class AdvancedBotGroup(models.Model):
    telegram_id = models.CharField(max_length=TELEGRAM_ID_BYTES)
    name = models.CharField(max_length=128)
    bot = models.ForeignKey(AdvancedBot, models.CASCADE, related_name="groups")
    approved = models.DateTimeField(null=True)
    admin = models.BooleanField(default=False)

    class Meta:
        unique_together = [
            ["bot", "telegram_id"]
        ]


class AdvancedBotAdmin(models.Model):
    bot = models.ForeignKey(AdvancedBot, models.CASCADE, related_name="admins")
    telegram_id = models.CharField(max_length=TELEGRAM_ID_BYTES)
    name = models.CharField(max_length=128)

    class Meta:
        unique_together = [
            ["bot", "telegram_id"]
        ]
