import inspect
from types import MethodType

from django.db.models import signals
from django.utils.timezone import now

from telethon import events
from telethon.tl import functions, types
from telethon.errors.rpcerrorlist import ChatAdminRequiredError

from .bot_command import BotCommandList, BotCommand
from . import models
from .connection import NewMessageEvent, BotClient, InlineQueryEvent, CallbackQueryEvent, TelegramEvent


class AdvancedBot(BotCommandList):
    @staticmethod
    def _tag_command(fn, trigger, hidden):
        """
        Helper for tagging methods as commands
        """
        fn._bot = "command"
        fn._command_trigger = trigger
        fn._command_hidden = hidden
        return fn

    @staticmethod
    def _tag_admin_command(fn, trigger):
        """
        Helper for tagging methods as admin commands
        """
        async def on_admin_command(self: AdvancedBot, event: NewMessageEvent):
            if not await self.is_admin(event):
                return

            return await fn(self, event)

        AdvancedBot._tag_command(on_admin_command, trigger or fn.__name__, True)
        on_admin_command.__doc__ = inspect.getdoc(fn)
        return on_admin_command

    @staticmethod
    def command(trigger=None, hidden=False):
        """
        Decorator tagging commands
        """
        def deco(fn):
            AdvancedBot._tag_command(fn, trigger or fn.__name__, hidden)
            return fn
        return deco

    @staticmethod
    def admin_command(trigger=None):
        """
        Decorator tagging admin commands
        """
        def deco(fn):
            return AdvancedBot._tag_admin_command(fn, trigger)
        return deco

    def __init__(self, db_model: models.AdvancedBot):
        self.commands = []
        self.db_model = db_model

        for cmd in vars(self.__class__).values():
            if getattr(cmd, "_bot", None) == "command":
                self.add_command(BotCommand(
                    cmd._command_trigger,
                    MethodType(cmd, self),
                    inspect.getdoc(cmd) or "",
                    cmd._command_hidden
                ))

        self._admin_chat_id = getattr(self.db_model.groups.filter(admin=True).first(), "telegram_id", None)
        self._admin_chat = None
        self.chats = set(self.db_model.groups.values_list("telegram_id", flat=True))
        self.admins = set(self.db_model.admins.values_list("telegram_id", flat=True))
        self.owner = self.db_model.bot.user.telegram_login.telegram_id

        signals.pre_save.connect(self._on_group_remove, models.AdvancedBotGroup)
        signals.post_save.connect(self._on_group_add, models.AdvancedBotGroup)
        signals.pre_delete.connect(self._on_group_remove, models.AdvancedBotGroup)

        signals.pre_save.connect(self._on_admin_remove, models.AdvancedBotAdmin)
        signals.post_save.connect(self._on_admin_add, models.AdvancedBotAdmin)
        signals.pre_delete.connect(self._on_admin_remove, models.AdvancedBotAdmin)

    def enabled(self, client: BotClient):
        """
        Enables extra events
        """
        client.add_event_handler(self._on_chat_action, events.ChatAction)

    def disabled(self, client):
        """
        Disables extra events
        """
        client.remove_event_handler(self._on_chat_action, events.ChatAction)

    async def _on_chat_action(self, event):
        """
        Handles chat actions
        """
        with event.client.exception_manager:
            user = await event.get_user()
            chat = await event.get_chat()
            chat_id = str(chat.id)
            joined = event.user_joined or event.user_added

            if event.client.me.id == user.id:
                if event.user_kicked:
                    self.db_model.groups.filter(id=chat_id).delete()
                elif joined:
                    models.AdvancedBotGroup(
                        telegram_id=chat_id,
                        name=chat.title,
                        bot=self.db_model,
                        approved=None
                    ).save()
                return

            if joined:
                await self.welcome(user, chat, event)

    def is_allowed_chat(self, chat):
        return str(chat.id) in self.chats

    def do_add_chat(self, chat):
        chat_id = str(chat.id)
        self.chats.add(chat_id)
        return models.AdvancedBotGroup.objects.update_or_create(
            telegram_id=chat_id,
            bot=self.db_model,
            defaults=dict(
                name=chat.title,
                approved=now()
            )
        )[1]

    def do_remove_chat(self, chat):
        chat_id = str(chat.id)
        self.chats.discard(chat_id)
        return models.AdvancedBotGroup.objects.filter(
            telegram_id=chat_id,
            bot=self.db_model
        ).delete()[0]

    def _on_group_add(self, instance: models.AdvancedBotGroup, **kw):
        """
        Handles groups saved on the model
        """
        if instance.approved and instance.bot_id == self.db_model.id:
            if instance.admin and instance.telegram_id != self._admin_chat_id:
                self._admin_chat_id = instance.telegram_id
                self._admin_chat = None
            self.chats.add(instance.telegram_id)

    def _on_group_remove(self, instance: models.AdvancedBotGroup, **kw):
        """
        Handles groups deleted or modified in the model
        """
        if instance.bot_id == self.db_model.id:
            if instance.telegram_id == self._admin_chat_id:
                self._admin_chat_id = self._admin_chat = None
            self.chats.discard(instance.telegram_id)

    def _on_admin_add(self, instance: models.AdvancedBotAdmin, **kw):
        """
        Handles admins saved on the model
        """
        if instance.bot_id == self.db_model.id:
            self.admins.add(instance.telegram_id)

    def _on_admin_remove(self, instance: models.AdvancedBotAdmin, **kw):
        """
        Handles admin deleted or modified in the model
        """
        if instance.bot_id == self.db_model.id:
            self.admins.discard(instance.telegram_id)

    def is_admin_user(self, user):
        sender_id = str(user.id)
        return sender_id in self.admins or sender_id == self.owner

    async def is_admin(self, event: NewMessageEvent):
        """
        Checks if the event was triggered by an admin
        """
        sender = await event.event.get_sender()
        return self.is_admin_user(sender)

    async def do_show_hidden_commands(self, event: NewMessageEvent):
        """
        Shows hidden commands
        """
        if not await self.is_admin(event):
            return

        reply = "Available admin commands:\n\n"

        for command in sorted(self.commands, key=lambda cmd: cmd.trigger):
            if command.hidden:
                reply += "/{trigger} {description}\n".format(
                    trigger=command.trigger,
                    description=command.description
                )

        reply += "\nAdmins:\n\n"
        for id in self.admins | {self.owner}:
            try:
                user = await event.client(functions.users.GetFullUserRequest(int(id)))
                reply += event.client.user_name(user.user)
            except Exception:
                db_object = models.AdvancedBotAdmin.objects.filter(bot=self.db_model, telegram_id=id).first()
                if db_object:
                    reply += db_object.name
                else:
                    reply += id
            reply += "\n"

        await self.reply_in_admin_chat(event, reply)

    async def get_chats(self, client: BotClient):
        chats = []
        for chat_id in self.chats:
            chat = await client.get_entity(types.PeerChannel(int(chat_id)))
            chats.append(chat)
        return chats

    async def do_list_chats(self, event: NewMessageEvent):
        """
        Shows allowed groups
        """
        if not await self.is_admin(event):
            return

        groups = models.AdvancedBotGroup.objects.filter(bot=self.db_model)

        reply = "Chats this bot operates in:\n\n"
        for group in groups:
            try:
                chat = await event.client.get_entity(types.PeerChannel(int(group.telegram_id)))
                name = chat.title
            except Exception:
                name = group.name

            reply += name
            reply += " -"

            if group.approved:
                reply += " Approved"
            else:
                reply += " Needs Approval"

            if bool(group.approved) != (group.telegram_id in self.chats):
                reply += " Out of Sync!"

            if group.telegram_id == self._admin_chat_id:
                reply += " Admin"

            reply += "\n"

        await self.reply_in_admin_chat(event, reply)

    async def mentions_from_message(self, event: NewMessageEvent):
        """
        Returns a dict of id => name from mentions in event
        """
        ids = {}
        for entity in await event.parse_text():
            if entity.is_mention:
                await entity.load()
                ids[str(entity.mentioned_id)] = entity.mentioned_name

        if len(ids) == 0 and event.event.message.is_reply:
            message = await event.event.message.get_reply_message()
            sender = await message.get_sender()
            ids = {str(sender.id): event.client.user_name(sender)}
        return ids

    async def do_add_admin(self, event: NewMessageEvent):
        """
        Adds admins
        """
        if not await self.is_admin(event):
            return

        ids = await self.mentions_from_message(event)
        reply = ""
        for id, name in ids.items():
            if id not in self.admins and id != self.owner:
                models.AdvancedBotAdmin(
                    bot=self.db_model,
                    telegram_id=id,
                    name=name
                ).save()
                reply += "Added " + name + "\n"
            else:
                reply += name + " is already an admin\n"

        await self.reply_in_admin_chat(event, reply)

    async def do_remove_admin(self, event: NewMessageEvent):
        if not await self.is_admin(event):
            return

        mentions = await self.mentions_from_message(event)
        to_remove = set(mentions.keys())

        old_admins = set(self.admins)
        models.AdvancedBotAdmin.objects.filter(telegram_id__in=to_remove).delete()

        reply = ""
        for id, name in mentions.items():
            if id == self.owner:
                reply += "%s is the bot owner, cannot remove\n" % name
            elif id not in old_admins:
                reply += "%s is not an admin\n" % name
            else:
                reply += "Removed %s\n" % name
        await self.reply_in_admin_chat(event, reply)

    async def do_admin_check(self, event: NewMessageEvent):
        """
        Used to check if the current user is an admin
        """
        if await self.is_admin(event):
            await self.reply_in_admin_chat(event, "%s is an admin" % event.client.user_name(event.event.sender))
        else:
            await self.reply_in_admin_chat(event, "%s is not an admin" % event.client.user_name(event.event.sender))

    async def inline(self, event: InlineQueryEvent):
        """
        Executed on inline query
        """
        pass

    async def button_callback(self, event: CallbackQueryEvent):
        """
        Executed when a user clicks on a bot button
        """
        pass

    async def welcome(self, user, chat, event):
        """
        Executed when a user enters a chat
        """
        pass

    async def admin_chat(self, client):
        if self._admin_chat is None:
            self._admin_chat = await client.get_entity(types.PeerChannel(int(self._admin_chat_id)))
        return self._admin_chat

    async def reply_in_admin_chat(self, event, *args, **kwargs):
        if isinstance(event, TelegramEvent):
            event = event.event

        if isinstance(getattr(event, "chat", None), types.User):
            chat = event.chat
        elif self._admin_chat_id is None:
            chat = event.chat
        else:
            chat = await self.admin_chat(event.client)

        return await event.client.send_message(chat, *args, **kwargs)

    async def toggle_mute(self, event: NewMessageEvent, mentions, mute):
        """
        Mutes/unmutes based on mentions
        """
        chats = await self.get_chats(event.client)

        reply = ""
        action = "mute" if mute else "unmute"
        for idstr, name in mentions.items():
            id = int(idstr)
            for chat in chats:
                try:
                    await event.client.edit_permissions(
                        chat,
                        id,
                        send_messages=not mute,
                    )
                    reply += "%s %s in %s\n" % (name, action + "d", chat.title)
                except ChatAdminRequiredError:
                    reply += "I don't have enough permissions in %s to %s %s\n" % (chat.title, action, name)
                except Exception as e:
                    event.client.exception_manager.log_current_exception()

        if not reply:
            reply = "Nothing to do"
        await self.reply_in_admin_chat(event, reply)

    async def on_loaded(self, client):
        if self._admin_chat_id is None:
            return

        chat = await self.admin_chat(client)
        input = await client.get_input_entity(chat)
        scope = types.BotCommandScopePeer(input)
        commands = self.get_command_data(True)
        await client(functions.bots.SetBotCommandsRequest(scope, "en", commands))
