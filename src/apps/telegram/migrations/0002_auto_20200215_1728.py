# Generated by Django 3.0.3 on 2020-02-15 17:28

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('telegram', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telegramuserbot',
            name='token',
            field=models.CharField(max_length=64),
        ),
        migrations.AlterUniqueTogether(
            name='telegramuserbot',
            unique_together={('user', 'token')},
        ),
    ]
