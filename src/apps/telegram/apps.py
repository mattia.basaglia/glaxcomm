import json

from django.shortcuts import render, redirect, get_object_or_404
from django.utils.http import url_has_allowed_host_and_scheme
from django.http.response import HttpResponseRedirect, HttpResponse, Http404
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db.utils import OperationalError, ProgrammingError
from django.utils.timezone import now

from apps.social_media.integration import Integration, view
from . import http_api


class TelegramConfig(Integration):
    name = 'apps.telegram'
    is_connected_attr = "telegram_login"
    awesome_icon = ("telegram-plane", "fab")

    def integration_ready(self):
        from . import connection, forms
        self.api = connection
        self.forms = forms

    @view(login_required=False)
    def login(self, request):
        telegram_user = self.api.BotClient.instance().auth_get_user(request.GET)
        if telegram_user:
            telegram_user.login(request)

        next_url = request.GET.get("next", "/")

        url_is_safe = url_has_allowed_host_and_scheme(
            url=next_url,
            allowed_hosts=[request.get_host()],
            require_https=request.is_secure(),
        )

        if not url_is_safe:
            next_url = "/"

        return HttpResponseRedirect(next_url)

    @view()
    def connect(self, request):
        telegram_user = self.api.BotClient.instance().auth_get_user(request.GET)
        if telegram_user:
            existing = self.models_module.TelegramLogin.objects.filter(telegram_id=telegram_user.user_id).first()
            if existing:
                if existing.user == request.user:
                    messages.add_message(request, messages.INFO, "User already connected")
                else:
                    messages.add_message(request, messages.ERROR, "Telegram account connected to a different user")
            else:
                self.models_module.TelegramLogin.objects.update_or_create(
                    user=request.user,
                    defaults={
                        "telegram_id": telegram_user.user_id,
                        "first_name": telegram_user.first_name,
                        "last_name": telegram_user.last_name,
                        "username": telegram_user.username or "",
                        "icon_url": telegram_user.icon_url
                    }
                )
        else:
            messages.add_message(request, messages.ERROR, "Telegram login failed")

        return redirect("telegram:manage")

    @view("")
    def manage(self, request):
        tl = getattr(request.user, "telegram_login", None)
        ctx = {
            "connected_user": tl.get_api_object() if tl else None,
            "bots": request.user.telegram_bots.all(),
            "builtin_bots": self.api.BotClient.instances() if request.user.is_superuser else [],
        }
        return render(request, "telegram/manage.html", ctx)

    @view()
    def disconnect(self, request):
        self.models_module.TelegramLogin.objects.filter(user=request.user).delete()
        return redirect("telegram:manage")

    @view("bot/add/")
    def bot_add(self, request):
        token = request.POST.get("token", "")
        if not token:
            messages.add_message(request, messages.ERROR, "Missing token")
        else:
            me = self.api.controller.add_bot(token).me
            if not me:
                messages.add_message(request, messages.ERROR, "Invalid token")
            else:
                created = self.models_module.TelegramUserBot.objects.get_or_create(user=request.user, token=token)[1]
                if created:
                    messages.add_message(request, messages.SUCCESS, "Added %s" % me.first_name)
                else:
                    messages.add_message(request, messages.WARNING, "Bot %s was already registered" % me.first_name)
        return redirect("telegram:manage")

    @view("bot/<str:token>/remove/")
    def bot_remove(self, request, token):
        bot = self.models_module.TelegramUserBot.objects.filter(token=token, user=request.user).first()
        if bot:
            me = bot.me
            if not bot.client.debug:
                self.api.controller.remove_bot(bot.client)
            bot.delete()
            messages.add_message(request, messages.SUCCESS, "Removed %s" % me.first_name)
        else:
            messages.add_message(request, messages.ERROR, "Bot not registered")

        return redirect("telegram:manage")

    @view("bot/<str:token>/disable/")
    def bot_disable(self, request, token):
        bot = self.models_module.TelegramUserBot.objects.filter(token=token, user=request.user).first()
        if bot:
            me = bot.me
            bot.enabled = False
            bot.client.disable()
            bot.save()
            messages.add_message(request, messages.SUCCESS, "Disabled %s" % me.first_name)
        else:
            messages.add_message(request, messages.ERROR, "Bot not registered")

        return redirect("telegram:manage")

    @view("bot/<str:token>/enable/")
    def bot_enable(self, request, token):
        bot = self.models_module.TelegramUserBot.objects.filter(token=token, user=request.user).first()
        if bot:
            me = bot.me
            bot.enabled = True
            bot.client.enable()
            bot.save()
            messages.add_message(request, messages.SUCCESS, "Enabled %s" % me.first_name)
        else:
            messages.add_message(request, messages.ERROR, "Bot not registered")

        return redirect("telegram:manage")

    @view("bot/<str:token>/reconnect/")
    def bot_reconnect(self, request, token):
        bot = self.models_module.TelegramUserBot.objects.filter(token=token, user=request.user).first()
        if bot:
            me = bot.me
            self.api.controller.remove_bot(bot.client)
            self.api.controller.add_bot(bot.client)
        else:
            messages.add_message(request, messages.ERROR, "Bot not registered")

        return redirect("telegram:manage")

    @view("bot/message/")
    def bot_compose(self, request):
        if "token" in request.GET:
            bot = get_object_or_404(
                self.models_module.TelegramUserBot.objects,
                token=request.GET["token"],
                user=request.user
            )
            initial = {"bot": bot.api}
        else:
            initial = {}

        form = self.forms.ComposeForm(request.POST, request.user, initial=initial)
        button_formset = self.forms.InlineKeyboardButtonFormSet(request.POST or None)

        if form.is_valid() and button_formset.is_valid():
            msg = self.api.Message(form.cleaned_data["bot"], form.cleaned_data["group"])
            msg.text = form.cleaned_data["message"]
            for btnform in button_formset:
                if btnform.cleaned_data:
                    if msg.reply_markup is None:
                        msg.add_inline_keyboard()
                    if btnform.cleaned_data["new_row"]:
                        msg.reply_markup.add_row()
                    text = btnform.cleaned_data["text"]
                    url = btnform.cleaned_data["url"] or None
                    msg.reply_markup.add_button_url(text, url)

            received = msg.send()
            if received:
                chan_name = received.chat.id
                if received.chat.username:
                    chan_name = "https://t.me/%s" % received.chat.username
                messages.add_message(request, messages.SUCCESS, "Message sent %s/%s" % (
                    chan_name, received.id
                ))
                return redirect("telegram:bot_compose")
            else:
                messages.add_message(request, messages.ERROR, "Could not send message: %s" % received)

        ctx = {
            "form": form,
            "button_formset": button_formset,
        }
        return render(request, "telegram/compose.html", ctx)

    def _get_api_or_404(self, request, token):
        admin_bot = None
        if request.user.is_superuser:
            for bot in self.api.BotClient.instances():
                if bot.token == token:
                    admin_bot = bot
                    break

        dbot = self.models_module.TelegramUserBot.objects.filter(token=token, user=request.user).first()

        if not dbot and not admin_bot:
            raise Http404

        if admin_bot:
            client = self.api.controller.bot(admin_bot.token) or admin_bot
        else:
            client = self.api.controller.bot(token) or dbot.api
        return (client, dbot, admin_bot is not None)

    @view("bot/<str:token>/")
    def bot_details(self, request, token):
        debug_mode_form = None

        client, dbot, is_builtin = self._get_api_or_404(request, token)

        if is_builtin:
            debug_mode_form = self.forms.DebugModeForm(request.POST or None, instance=client.debug)
            if request.POST and debug_mode_form.is_valid:
                obj = debug_mode_form.save(False)
                obj.save()
                client.debug = obj
                messages.add_message(request, messages.SUCCESS, "Debug mode changed")
                return redirect("telegram:bot_details", token)

        httpapi = http_api.HttpApi(client.token)

        advanced_bot = getattr(dbot, "advanced_bot", None)
        advanced_form = self.forms.AdvancedBotForm(
            instance=advanced_bot,
            initial={} if advanced_bot else {"username": "@" + client.me.username}
        )

        try:
            commands = client.sync.get_my_commands()
        except Exception:
            commands = None

        ctx = {
            "dbot": dbot,
            "debug_mode_form": debug_mode_form,
            "bot": client.me,
            "exceptions": client.exceptions,
            "commands": commands,
            "hook": httpapi.get("getWebhookInfo"),
            "advanced_bot": advanced_bot,
            "advanced_form": advanced_form,
        }
        return render(request, "telegram/bot_details.html", ctx)

    @view("bot/<str:token>/delete_hook")
    def bot_delete_hook(self, request, token):
        api, dbot, is_builtin = self._get_api_or_404(request, token)
        httpapi = http_api.HttpApi(api.token)
        httpapi.get("deleteWebhook")
        return redirect("telegram:bot_details", token)

    @view("bot/<str:token>/exceptions_clear")
    def bot_exceptions_clear(self, request, token):
        api, dbot, is_builtin = self._get_api_or_404(request, token)
        api.exception_manager.exceptions = []
        return redirect("telegram:bot_details", token)

    @view("bot/<str:token>/test")
    def bot_test(self, request, token):
        bot, _, _ = self._get_api_or_404(request, token)
        api = http_api.HttpApi(bot.token)

        data = ""
        method = ""
        field_info = []
        n_fields = 0

        if request.method == "POST":
            http = "get"
            n_fields = int(request.POST.get("n_fields"))
            method = request.POST.get("method")
            fields = {}

            for i in range(n_fields):
                name = request.POST.get("field_name_%s" % i)
                value_key = "field_value_%s" % i
                if value_key in request.FILES:
                    file = request.FILES[value_key]
                    value = http_api.InputFile(file.name, file.read())
                    http = "post"
                else:
                    value = request.POST.get(value_key)
                field_info.append((i, name, value))
                if name:
                    fields[name] = value

            data = json.dumps(getattr(api, http)(method, fields), indent="  ")

        ctx = {
            "bot": bot,
            "data": data,
            "field_info": field_info,
            "method": method,
            "n_fields": n_fields,
        }
        return render(request, "telegram/bot_test.html", ctx)

    def _get_advanced_or_404(self, request, token):
        api, dbot, is_builtin = self._get_api_or_404(request, token)
        advanced = getattr(dbot, "advanced_bot", None)
        if not advanced:
            raise Http404()
        return api, dbot, advanced

    @view("bot/<str:token>/advanced")
    def bot_advanced(self, request, token):
        api, dbot, is_builtin = self._get_api_or_404(request, token)

        instance = getattr(dbot, "advanced_bot", None)
        advanced_form = self.forms.AdvancedBotForm(request.POST, instance=instance)

        if advanced_form.is_valid:
            instance = advanced_form.save(False)
            instance.bot = dbot
            instance.save()
            messages.add_message(request, messages.SUCCESS, "Updated advanced bot settings")
        else:
            messages.add_message(request, messages.ERROR, "Error updating the advanced bot settings")

        return redirect("telegram:bot_details", token)

    @view("bot/<str:token>/advanced/group/<int:id>/approve")
    def bot_advanced_group_approve(self, request, token, id):
        api, dbot, advanced = self._get_advanced_or_404(request, token)
        group = get_object_or_404(advanced.groups, id=id)
        group.approved = now()
        group.save()
        messages.add_message(request, messages.SUCCESS, "Group Approved")
        return redirect("telegram:bot_details", token)

    @view("bot/<str:token>/advanced/group/<int:id>/delete")
    def bot_advanced_group_delete(self, request, token, id):
        api, dbot, advanced = self._get_advanced_or_404(request, token)
        group = get_object_or_404(advanced.groups, id=id)
        group.delete()
        messages.add_message(request, messages.SUCCESS, "Group deleted")
        return redirect("telegram:bot_details", token)

    @view("bot/<str:token>/advanced/group/<int:id>/make_admin")
    def bot_advanced_group_make_admin(self, request, token, id):
        api, dbot, advanced = self._get_advanced_or_404(request, token)
        advanced.groups.filter(admin=True).update(admin=False)
        group = get_object_or_404(advanced.groups, id=id)
        group.admin = True
        group.save()
        messages.add_message(request, messages.SUCCESS, "Admin group changed")
        return redirect("telegram:bot_details", token)
