from django import template
from django.urls import reverse
from django.utils import html
from django.utils.safestring import mark_safe
from .. import connection

register = template.Library()


@register.simple_tag(takes_context=True)
def telegram_login_widget(context, auth_url, bot=None, send_messages=True, size="large", radius=20, **kwargs):
    url = reverse(auth_url, kwargs=kwargs)
    url = context["request"].build_absolute_uri(url)
    bot_me = connection.get_client(bot).me

    attrs = {
        "async": "async",
        "src": "https://telegram.org/js/telegram-widget.js?4",
        "data-telegram-login": html.escape(bot_me.username),
        "data-size": str(size),
        "data-radius": str(radius),
        "data-auth-url": html.escape(url),
    }
    if send_messages:
        attrs["data-request-access"] = "write"

    return mark_safe("<script %s></script>" % " ".join("%s='%s'" % it for it in attrs.items()))
