from . import models
from . import connection


class TelegramMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

        connection.controller.start()
        connection.controller.set_loop()

    def __call__(self, request):
        return self.get_response(request)

