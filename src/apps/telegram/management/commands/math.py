from PIL import Image

from django.core.management import BaseCommand

from lottie.importers.core import import_tgs

from ...bot_commands.math import BlackBoard


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("latex")
        parser.add_argument("--output", "-o", default="/tmp/foo.png")
        parser.add_argument("--image", action="store_true")
        parser.add_argument("--lottie", action="store_true")

    def handle(self, latex, output, image, lottie, **options):
        board = BlackBoard()

        if image:
            board.image(Image.open(latex))
        elif lottie:
            board.lottie(import_tgs(latex))
        else:
            board.parse(latex)

        board.render().save(output)
