from django.core.management import BaseCommand
from django.conf import settings
from ...models import TelegramUserBot
from ...connection import BotClient


class Command(BaseCommand):
    def execute(self, *args, **options):
        templ = "%-10s|%s"

        for name in settings.TELEGRAM.keys():
            bot = BotClient.instance(name)
            self.stdout.write(templ % (
                "*" + name,
                bot.token
            ))

        for bot in TelegramUserBot.objects.all():
            self.stdout.write(templ % (
                bot.user.username,
                bot.token
            ))
