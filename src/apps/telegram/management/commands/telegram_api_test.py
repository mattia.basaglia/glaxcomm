import json
from pprint import pprint
from django.core.management import BaseCommand
from django.conf import settings
from ...models import TelegramUserBot
from ...http_api import HttpApi


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--bot", "-b",
            default="default",
            help="Bot name as in settings or token",
            dest="bot_name",
        )

        parser.add_argument(
            "method",
            help="API method. See https://core.telegram.org/bots/api#available-methods",
        )

        parser.add_argument(
            "--param", "-p",
            default=[],
            action="append",
            nargs=2,
            help="Arguments",
        )

    def execute(self, bot_name, method, param, **options):
        if bot_name in settings.TELEGRAM:
            token = settings.TELEGRAM[bot_name]["TOKEN"]
        else:
            token = bot_name

        pprint(HttpApi(token).get(method, dict(param)))
