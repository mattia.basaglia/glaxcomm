from django.core.management import BaseCommand

from lottie.exporters.core import export_embedded_html
from lottie.exporters.cairo import export_png

from ...bot_commands.poes import Parser, Renderer


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--ast", action="store_true", dest="print_ast")
        parser.add_argument("poescont")
        #parser.add_argument("--output", "-o", default="/tmp/foo.html")

    def handle(self, poescont, print_ast, **options):
        ast = Parser().parse(poescont)
        print(ast)
        if print_ast:
            print(ast.dump())

        renderer = Renderer()
        animation = renderer.render(ast)
        for w in renderer.warnings:
            print(w)

        export_embedded_html(animation, "/tmp/poes.html")
        export_png(animation, "/tmp/poes.png")
