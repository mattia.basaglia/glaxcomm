from django.core.management import BaseCommand

from lottie.utils.font import EmojiRenderer

from ...bot_commands.animations import emoji_path


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("text")
        parser.add_argument("--emoji-dir", "-e", default=emoji_path)

    def handle(self, text, emoji_dir, **options):
        renderer = EmojiRenderer(None, emoji_dir)
        for char in EmojiRenderer.emoji_split(text):
            print("%s %s %s" % (
                char,
                renderer.emoji_basename(char),
                renderer.emoji_filename(char) or ""
            ))

