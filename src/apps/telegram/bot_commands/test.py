from ..bot_command import bot_command, bot_button_callback
from ..connection import NewMessageEvent, InlineKeyboard, CallbackQueryEvent


@bot_command("GlaxBot")
async def test(event: NewMessageEvent):
    """
    Sends a pointless greeting
    """
    buttons = InlineKeyboard()
    buttons.add_button_callback("foo", "foo")
    buttons.add_button_callback("bar", "bar")
    await event.client.send_message(event.chat, "Hello, this is a test", buttons=buttons.to_data())


@bot_button_callback("GlaxBot")
async def _on_button(event: CallbackQueryEvent):
    buttons = InlineKeyboard()

    if event.query != "foo":
        buttons.add_button_callback("foo", "foo")
    if event.query != "bar":
        buttons.add_button_callback("bar", "bar")
    if event.query != "baz":
        buttons.add_button_callback("baz", "baz")

    await event.edit("You clicked " + event.query, buttons=buttons.to_data())
