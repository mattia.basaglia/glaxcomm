#!/usr/bin/env python3
import os
import json
import urllib.request
import pathlib
import argparse
from lottie.importers.raster import import_raster
from lottie.exporters.core import export_lottie

here = pathlib.Path(__file__).parent

parser = argparse.ArgumentParser()
parser.add_argument("--download", action="store_true")
parser.add_argument("--trace", action="store_true")
parser.add_argument("--words", nargs="+", default=[])
ns = parser.parse_args()


with open(here / "data.json") as data_file:
    data = json.load(data_file)


for what in ["characters", "words"]:
    print(what)

    for name, info in data[what].items():
        for url_key in ["text"]:
            if url_key in info and "url" in info[url_key]:
                url = info[url_key]["url"]
                basename = os.path.basename(url)
                file = here / what / basename
                json_file = file.parent / (file.stem + ".json")

                download = not file.exists() or (ns.words and name in ns.words)
                trace = not json_file.exists() or (ns.words and name in ns.words)

                if download or trace:
                    print("    %s - %s" % (name, url_key.split("-")[0]))

                if download and ns.download:
                    print("        downloading %s" % url)
                    try:
                        with open(file, "wb") as f:
                            f.write(urllib.request.urlopen(url).read())
                    except Exception as e:
                        print("        download failed: %s" % e)
                        file.unlink()
                        continue

                if ns.trace and trace:
                    print("        tracing")
                    animation = import_raster([str(file)], 0, [], "pixel")
                    export_lottie(animation, str(json_file))
