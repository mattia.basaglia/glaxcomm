import gzip
import json
import codecs
import inspect
from io import BytesIO
from PIL import Image

from django.conf import settings
from telethon.tl.types import DocumentAttributeFilename
from lottie.importers.raster import import_raster

from ..connection import NewMessageEvent, InlineQueryEvent, InlineKeyboard, CallbackQueryEvent
from ..bot_command import bot_command, bot_inline, bot_button_callback
from . import baba
from .animations import anim_common


def baba_sticker_file(map):
    data = map.to_data()
    writer = baba.BabaWriter()
    writer.set_map(map)
    writer.finalize()

    file = BytesIO()
    with gzip.open(file, "wb") as gzfile:
        lottie_dict = writer.animation.to_dict()
        lottie_dict["tgs"] = 1
        lottie_dict["baba"] = data
        json.dump(lottie_dict, codecs.getwriter('utf-8')(gzfile))
    file.seek(0)
    return file


async def get_baba_map(event, text, all_is_color=True):
    baba_map = baba.BabaMap()

    if "user!" in text or "USER!" in text:
        pfp = await get_pfp(event)
        if pfp:
            baba_map.add_custom_object("USER", pfp)

    for sentence in text.splitlines():
        baba_map.add_sentence(sentence, all_is_color)

    return baba_map


@bot_command("GlaxMakeRobot", trigger="baba")
async def baba_cmd(event: NewMessageEvent):
    """
    ROBOT MAKE TEXT
    """
    if not event.query.strip():
        await event.client.send_message(event.chat, "TEXT IS MISSING")
        return

    baba_map = await get_baba_map(event, event.query)
    await event.client.send_animated_sticker(event.chat, baba_sticker_file(baba_map))


@bot_inline("GlaxMakeRobot")
async def baba_inline(event: InlineQueryEvent):
    if event.query:
        baba_map = await get_baba_map(event, event.query)
        event.animated_sticker(baba_sticker_file(baba_map))


@bot_command("GlaxMakeRobot")
async def start(event: NewMessageEvent):
    """
    Show help
    """
    text = inspect.cleandoc("""
    **INTRO**

    You can use /baba here or as an inline bot in any chat

    This bot renders text in the style of [Baba is You](https://hempuli.com/baba/).

    **BASICS**

    Type words to make them show up in the Baba style

    `baba is you`

    **PICTURES**

    For certain words you can use a `!` at the end to use an image instead of text:

    `baba!`

    To list the words with a picture, check /words

    Some emoji can be used as a shorthand for pictures, check /emoji for a list.

    **COLORS**

    You can also the default color using `:`

    `baba:red`

    And combine the two

    `baba!:red`

    Colors can be specified by name (`red`, `blue` etc) and by HTML hex code (`#f00`, `#0000ff`)

    **DIRECTIONS**

    Similarly to colors, you can specify the orientation (this is only for pictures, not words).
    Directions and colors can be mixed together, separated by commas (order doesn't matter).

    `baba!:left baba!:right baba!:left,red`

    **SPACING**

    You can have multiple lines of text, this will result into multiple rows in the sticker.

    To have an empty cell use `empty!` or `-`.

    `- - baba!`

    To have two objects in the same cell, use a `+` between them

    `tile! + baba!`

    Note that there must be spaces around the `+`.

    **CUSTOM PROPERTY WORDS**

    For custom words (ie: that are not in the game), you can make them use a background using square brackets

    `[hello] [hello]:red`
    """)
    await event.client.send_message(event.chat, text, parse_mode='md')


@bot_command("GlaxMakeRobot")
async def words(event: NewMessageEvent):
    """
    Shows available words
    """
    baba_words = []
    extra_words = []
    for word, info in baba.BabaData.get_data()["words"].items():
        if "picture" in info and "text" in info:
            baba_words.append(word)
        elif "picture" in info:
            extra_words.append(word)

    text = "Words with pictures from the game: \n"
    for word in sorted(baba_words):
        text += " - " + word + "\n"

    text += "\n\nCustom words with pictures: \n"
    for word in sorted(extra_words):
        text += " - " + word + "\n"

    await event.client.send_message(event.chat, text, parse_mode='md')


@bot_command("GlaxMakeRobot")
async def credits(event: NewMessageEvent):
    """
    Show credits
    """
    animation = baba_is_text("\n".join([
        "-",
        "@ glax make robot - robot make text",
        "-",
        "- - - glax! love! robot!",
        "-",
        "- b:grey o:grey t:grey - b y",
        "@:glax M:glax a:glax t:glax t:glax B:glax a:glax s:glax",
        "-"
    ]))
    await event.client.send_animated_sticker(event.chat, baba_sticker_file(animation))


@bot_command("GlaxMakeRobot")
async def emoji(event: NewMessageEvent):
    """
    Shows available emoji aliases
    """
    text = ""

    for word, data in sorted(baba.BabaData.get_data()["words"].items()):
        if "aliases" in data:
            text += "%s\n%s\n\n" % (word, "".join(data["aliases"]))

    await event.client.send_message(event.chat, text, parse_mode='md')


@bot_command("GlaxMakeRobot")
async def rules(event: NewMessageEvent):
    """
    Shows the in-game rules given some baba level layout
    """
    recipient = event.chat
    if not event.query.strip():
        await event.client.send_message(event.chat, "TEXT IS MISSING")
        return

    map = baba.BabaMap()
    for sentence in text.split("\n"):
        map.add_sentence(sentence)

    rules = map.gather_rules()

    await event.client.send_message(event.chat, "\n".join(map(str, rules)))


@bot_command("GlaxMakeRobot")
async def level(event: NewMessageEvent):
    """
    Creates a playable level
    """
    text = event.query.strip()
    if not text:
        await event.client.send_message(event.chat, "TEXT IS MISSING")
        return

    baba_map = await get_baba_map(event, text, False)

    compiled = baba.CompiledMap(baba_map)
    if compiled.width > 16 or compiled.height > 16:
        await event.client.send_message(event.chat, "Level is too large, keep it within 16x16")
        return
    elif len(compiled.instances) > 100:
        await event.client.send_message(event.chat, "Level is too complex, keep it with fewer than 100 objects")
        return

    try:
        rules = compiled.gather_rules()
        compiled.apply_rules(rules, False, True)
    except baba.TooComplex:
        await event.client.send_message(event.chat, "Rules are too complex")
        return

    await baba_level_send_file(event, compiled)


async def baba_level_send_file(event, compiled: baba.BabaMap):
    try:
        sender = await event.event.get_sender()
        sender_name = sender.first_name
    except Exception:
        sender_name = " "

    buttons = InlineKeyboard()

    buttons.add_row()
    buttons.add_button_callback(sender_name, "-")
    buttons.add_button_callback("⬆️", "up")
    buttons.add_button_callback(" ", "-")

    buttons.add_row()
    buttons.add_button_callback("⬅️", "left")
    buttons.add_button_callback("🕐️", "static")
    buttons.add_button_callback("➡️", "right")

    buttons.add_row()
    buttons.add_button_callback(" ", "-")
    buttons.add_button_callback("⬇️", "down")
    buttons.add_button_callback("Rules", "rules")

    if settings.DEBUG_BOTS:
        buttons.add_row()
        buttons.add_button_callback("Debug", "debug")
        buttons.add_button_callback("Duplicate", "copy")

    await baba_level_do_send_file(event, compiled, buttons)


async def baba_level_do_send_file(event, compiled: baba.BabaMap, buttons: InlineKeyboard):
    data = compiled.to_data()

    writer = baba.BabaWriter()
    writer.set_map(compiled)
    writer.finalize()

    file = BytesIO()
    with gzip.open(file, "wb") as gzfile:
        lottie_dict = writer.animation.to_dict()
        lottie_dict["tgs"] = 1
        lottie_dict["baba"] = data
        json.dump(lottie_dict, codecs.getwriter('utf-8')(gzfile))
    file.seek(0)

    await event.client.send_file(
        event.chat,
        file,
        buttons=buttons.to_data(),
        attributes=[
            DocumentAttributeFilename("sticker.tgs")
        ]
    )


@bot_button_callback("GlaxMakeRobot")
async def on_button(event: CallbackQueryEvent):
    if event.query == "-":
        await event.event.answer()
        return

    command = event.query

    message = await event.event.get_message()
    file = BytesIO()
    await message.download_media(file)

    file.seek(0)
    with gzip.open(file, "rb") as gzfile:
        lottie = json.load(gzfile)
        data = lottie["baba"]

    baba_map = await get_baba_map(event, data, False)

    if command == "rules":
        text = "\n".join(map(str, baba_map.gather_rules(False)))
        await event.client.send_message(event.chat, text+" ")
        await event.event.answer()
        return
    elif command == "debug":
        await event.client.send_message(event.chat, baba_map.to_data())
        await event.event.answer()
        return
    elif command == "copy":
        await baba_level_send_file(event, baba.CompiledMap(baba_map))
        await event.event.answer()
        return

    compiled = baba.CompiledMap(baba_map)
    try:
        compiled.step(baba.Direction[command])
    except baba.TooComplex:
        await event.client.send_message(event.chat, "Rules are too complex")
        await event.event.answer()
        return

    if compiled.won:
        buttons = InlineKeyboard()
        buttons.add_button_callback("Congratulations", "-")
        await baba_level_do_send_file(event, compiled, buttons)
    else:
        await baba_level_send_file(event, compiled)

    await message.delete()
    await event.event.answer()


async def get_pfp(event):
    sender = await event.event.get_sender()
    file = BytesIO()
    result = await event.client.download_profile_photo(sender, file, download_big=False)
    if result is None:
        return None

    file.seek(0)

    try:
        img = Image.open(file)
    except Exception:
        return None

    img = img.quantize(16).resize((24, 24), Image.NEAREST)

    animation = import_raster([img], 0, [], "pixel", frame_delay=36)

    return animation
