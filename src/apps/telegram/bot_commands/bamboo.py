import datetime
import urllib.request
from dataclasses import dataclass
from django.conf import settings

from ..bot_command import bot_command
from ..connection import NewMessageEvent

@dataclass
class Out:
    who: str = None
    description: str = None
    start: datetime.date = None
    end: datetime.date = None


def get_date(line: str):
    return datetime.datetime.strptime(line.rsplit(":", 1)[1], "%Y%m%d")


def get_text(line: str):
    return line.rsplit(":", 1)[1].split("(")[0].strip()


def get_whos_out(url, people):
    now = datetime.datetime.now()


    response = urllib.request.urlopen(urllib.request.Request(
        url,
        headers={
            "User-Agent": "Who's Out Bot"
        }
    ))

    data = response.read().decode(response.headers.get_content_charset())

    everyone = []
    event = Out()
    for line in data.splitlines():
        if line == "END:VEVENT":
            if event.who in people and event.start <= now < event.end:
                everyone.append(event)
            event = Out()
        elif line.startswith("DTSTART"):
            event.start = get_date(line)
        elif line.startswith("DTEND"):
            event.end = get_date(line)
        elif line.startswith("DESCRIPTION"):
            event.description = get_text(line)
        elif line.startswith("SUMMARY"):
            event.who = get_text(line)

    return everyone


async def send_out_list(client):
    cfg = settings.BAMBOO
    out = get_whos_out(cfg["calendar"], cfg["people"])
    formatted = ""
    for who in out:
        formatted += "- %s (%s)\n" % (who.who, who.description)
    await client.send_message(cfg["chat"], formatted)


@bot_command("LFBamboo_bot")
async def out(event: NewMessageEvent):
    """
    Shows who's out of the basement
    """
    await send_out_list(event.client)
