import random
import inspect

from ..connection import NewMessageEvent
from ..bot_command import bot_command


deranged_quotes = [
    "My penis is like a photon because it would go inside both slits",
    "What if I want to cook pasta in my ass?",
    "I'm like sonic. Gotta cum fast",
    "Shove pizza up the ass? 🤔",
    "Everything is porn with enough imagination",
    "My personality is a form of house arrest",
    "Your paws are so musky they bought Twitter",
    "Boobs are the balls of the chest",
    "Why get shit-faced when you can get sit-faced",
    "Does cum hydrate?",
    "I steal dragoness eggs so I can volunteer to help them make more",
    "I'm not an experienced birder but I do reliably jizz in the field",
    "Lamassu is breaking the magic",
]



@bot_command("GlaxBot")
async def start(event: NewMessageEvent):
    """
    Shows the start message
    """
    text = inspect.cleandoc("""
    This bot provides wisdom from Glax the Dragon.
    Please note that many of the quotes might be NSFW , use at your own risk
    """)
    await event.client.send_message(event.chat, text, parse_mode='md')



@bot_command("GlaxBot")
async def wisdom(event: NewMessageEvent):
    """
    Gives a pearl of wisdom
    """
    await event.client.send_message(event.chat, random.choice(deranged_quotes))
