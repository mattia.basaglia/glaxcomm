import json
import asyncio
from PIL import Image

from ...httputils import urljoin, urlopen
from ..bot_command import bot_command, bot_inline
from ..connection import NewMessageEvent, InlineQueryEvent, photo_file

api_base = "https://dragon.best/api/"


async def image_common(event, path, get_arg):
    recipient = event.chat
    if not event.query:
        await event.client.send_message(event.chat, "You have to send me some text with that command")
        return

    await event.client.send_sticker(event.chat, get_image(event, path, get_arg))


def get_response(event, path, get_arg, extra_get={}, base=None):
    if not get_arg:
        get = {}
    else:
        get = {
            get_arg: event.query
        }

    get.update(extra_get)
    url = urljoin(base if base else api_base, path, get)
    return urlopen(url)


def get_image(event, path, get_arg, extra_get={}, base=None, format="WebP"):
    in_image = Image.open(get_response(event, path, get_arg, extra_get, base))
    return photo_file(in_image, format)


async def async_get_image(*a, **kw):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(None, get_image, *a, **kw)


@bot_command("GlaxFlagBot")
async def flag(event: NewMessageEvent):
    """
    Shows Glax holding one or two flags
    """
    await image_common(event, "flag.png", "flag")


@bot_inline("GlaxFlagBot")
async def inline_flag(event: InlineQueryEvent):
    if event.query:
        event.sticker(get_image(event, "flag.png", "flag"))


@bot_command("DurgTimeBot")
@bot_command("GlaxTimeBot")
async def time(event: NewMessageEvent):
    """
    Shows Glax's clock
    """

    await event.client.send_sticker(event.chat, get_image(event, "clock.png", "time"))


@bot_inline("DurgTimeBot")
@bot_inline("GlaxTimeBot")
async def inline_time(event: InlineQueryEvent):
    if event.query:
        event.sticker(get_image(event, "clock.png", "time"))
        event.sticker(get_image(event, "clock.png", "time", {"flip": 1}))
        event.sticker(get_image(event, "clock.png", "time", {"sleepy": 1}))
        event.sticker(get_image(event, "clock.png", "time", {"flip": 1, "sleepy": 1}))


says_faces = [
    "angry",
    "awoo",
    "bamboozled",
    #"blep",
    #"bored",
    #"clock",
    #"cloud",
    "cyborg",
    "derp",
    "facepalm",
    "fire",
    "hiss",
    "hugs",
    "innocent",
    "laughing",
    "not-sure-if",
    "party",
    "pissed off",
    "pointing",
    #"rawrdical",
    "run",
    "sleepy",
    "smirk",
    "tongue out",
    "triggered",
    "unimpressed",
    "yawn",
]


@bot_inline("GlaxSaysBot")
async def inline_says(event: InlineQueryEvent):
    if event.query:
        images = await asyncio.gather(*[
            async_get_image(event, "glax_says.png", "text", {"face": face})
            for face in says_faces
        ])
        for image in images:
            event.sticker(image)


def shield_impl(event):
    return get_image(event, "drawshield.php", "blazon", {
            "outputformat": "png",
            "palette": "drawshield",
            "shape": "heater",
            "effect": "plain",
            "size": "512",
            "asfile": "1",
            "webcols": "yes",
            #"tartancols": "yes",
            "customPalette[heraldic/azure]": "#0f47af",
        }, "https://drawshield.dragon.best/"
    )


@bot_inline("GlaxShieldBot")
async def inline_shield(event: InlineQueryEvent):
    if event.query:
        event.sticker(shield_impl(event))


@bot_command("GlaxShieldBot")
async def shield(event: InlineQueryEvent):
    """
    Renders a shield
    """
    recipient = event.chat
    image = shield_impl(event)
    await event.client.send_sticker(event.chat, image)


@bot_command("GlaxWeatherBot")
async def weather(event: NewMessageEvent):
    """
    Get current weather from the specified location
    """
    data = json.load(get_response(event, "glax_weather.json", "location"))
    image = data["icon"]
    message = data["message"]
    await event.client.send_file(event.chat, image, caption=message)


@bot_inline("GlaxWeatherBot")
async def inline_weather(event: InlineQueryEvent):
    if event.query:
        data = json.load(get_response(event, "glax_weather.json", "location"))
        image = data["icon"]
        message = data["message"]
        event.photo(image, text=message)


@bot_inline("SomeDragonsBot")
async def inline_some_dragons(event: InlineQueryEvent):
    event.photo(get_image(event, "some_dragons.jpg", "what", format="JPEG"))


@bot_inline("GlaxSealBot")
async def inline_seal(event: InlineQueryEvent):
    if event.query:
        images = await asyncio.gather(*[
            async_get_image(event, "glax_seal.png", "text", {"face": face})
            for face in says_faces
        ])
        for image in images:
            event.sticker(image)


@bot_inline("YouWouldntBot")
async def inline_you_wouldnt(event: InlineQueryEvent):
    event.photo(get_image(event, "you_wouldnt.jpg", "what", format="JPEG"))


@bot_inline("BrexitBusBot")
async def inline_brexit_bus(event: InlineQueryEvent):
    event.photo(get_image(event, "brexit_bus.jpg", "text", format="JPEG"))


is_this_pics = [
    "cd",
    "beeps"
]
@bot_inline("IsThisAGlaxBot")
async def inline_is_this(event: InlineQueryEvent):
    if event.query:
        for who in is_this_pics:
            event.photo(get_image(event, "is_this_an_api.jpg", "text", {"who": who, "width": "512"}, format="JPEG"))


@bot_inline("ADRagonBot")
async def inline_adragon(event: InlineQueryEvent):
    if event.query:
        event.sticker(get_image(event, "adragon.png", "text", {"class": "1", "nc": "3"}))
        event.sticker(get_image(event, "adragon.png", "text", {"class": "2", "nc": "3"}))
        event.sticker(get_image(event, "adragon.png", "text", {"class": "3", "nc": "3"}))
        event.sticker(get_image(event, "adragon.png", "text", {"class": "4", "nc": "3"}))
        event.sticker(get_image(event, "adragon.png", "text", {"class": "5", "nc": "3"}))
        event.sticker(get_image(event, "adragon.png", "text", {"class": "6", "nc": "3"}))


def cert_get_image(event):
    title = ""
    text = event.query.strip()
    if text.startswith("**"):
        title, text = text[2:].split("**", 1)

    return get_image(event, "certificate.png", "", {"title": title.strip(), "text": text.strip()})


@bot_inline("GlaxCertBot")
async def inline_certificate(event: InlineQueryEvent):
    event.sticker(cert_get_image(event))


@bot_command("GlaxCertBot")
async def certificate(event: NewMessageEvent):
    """
    Renders a Glax certificate sticker
    """
    await event.client.send_sticker(event.chat, cert_get_image(event))
