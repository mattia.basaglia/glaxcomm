import io
import time
import pathlib
from PIL import Image

from lottie.exporters.cairo import export_png
import lottie
from lottie.utils.font import FontStyle, TextJustify

from telethon.errors.rpcerrorlist import ChatAdminRequiredError, ChatAdminInviteRequiredError

from .animations import emoji_path
from ..advanced_bot import AdvancedBot
from ..connection import NewMessageEvent, MessageFormatter, static_sticker_file


class FloodCheck:
    def __init__(self, join):
        self.join = join
        self.last_media = self.join
        self.count = 0
        self.restricted = False

    def tick(self, has_media):
        now = time.time()
        delta = now - self.last_media
        if delta > 30 or now - self.join > 60:
            return None

        if has_media:
            if delta < 1:
                self.count += 5
            else:
                self.count += 1
            self.last_media = now

            return self.count > 10

        return False


class EurofursBot(AdvancedBot):
    _asset_root = pathlib.Path(__file__).parent / "assets"
    _admin_at = "@Raugh @MattBas @fennecbyte @TubeDude @pyonium"

    def __init__(self, db_model):
        super().__init__(db_model)
        self.monitored = {}

    @AdvancedBot.admin_command()
    async def add_admin(self, event: NewMessageEvent):
        """
        Adds new bot admins
        """
        await self.do_add_admin(event)

    @AdvancedBot.admin_command()
    async def remove_admin(self, event: NewMessageEvent):
        """
        Removes bot admins
        """
        await self.do_remove_admin(event)

    @AdvancedBot.command()
    async def admin(self, event: NewMessageEvent):
        """
        Notifies all admins
        """
        if not self.is_allowed_chat(event.chat):
            return

        await event.reply(self._admin_at)

    @AdvancedBot.admin_command()
    async def admin_help(self, event: NewMessageEvent):
        """
        Shows all the admin commands
        """
        await self.do_show_hidden_commands(event)

    @AdvancedBot.command(hidden=True)
    async def admin_check(self, event: NewMessageEvent):
        """
        Tells you if you are an admin
        """
        await self.do_admin_check(event)

    async def welcome(self, user, chat, event):
        if not self.is_allowed_chat(event.chat):
            return

        if not self.is_admin_user(user):
            self.monitored[user.id] = FloodCheck(time.time())

        full_name = event.client.user_name(user)

        anim = lottie.objects.Animation()
        fill_color = lottie.utils.color.from_uint8(0x00, 0x33, 0x99)
        stroke_color = lottie.utils.color.from_uint8(0xff, 0xcc, 0x00)
        stroke_width = 12

        font = FontStyle("Ubuntu:style=bold", 80, TextJustify.Center, emoji_svg=emoji_path)
        text_layer = lottie.objects.ShapeLayer()
        anim.add_layer(text_layer)
        group = font.render("Welcome", lottie.NVector(256, font.line_height))
        text_layer.add_shape(group)
        text_layer.add_shape(lottie.objects.Fill(fill_color))
        text_layer.add_shape(lottie.objects.Stroke(stroke_color, stroke_width))

        pos = lottie.NVector(256, 512)
        group = font.render(full_name, pos)
        group.transform.anchor_point.value = pos.clone()
        group.transform.position.value = pos.clone()
        bbox = group.bounding_box()
        max_width = 512 - 16
        if bbox.width > max_width:
            group.transform.scale.value *= max_width / bbox.width
            bbox = group.bounding_box()
        group.transform.position.value.y -= bbox.y2 - 512 + 32
        text_layer.add_shape(group)
        text_layer.add_shape(lottie.objects.Fill(stroke_color))
        text_layer.add_shape(lottie.objects.Stroke(stroke_color, stroke_width))

        asset = lottie.objects.Image.embedded(
            self._asset_root / "yay.webp"
        )
        anim.assets.append(asset)
        anim.add_layer(lottie.objects.ImageLayer(asset.id))

        await event.client.send_sticker(event.chat, static_sticker_file(anim))

    async def run_unknown_command(self, event: NewMessageEvent):
        if not self.is_allowed_chat(event.chat):
            return

        if event.text.startswith("@admin") or event.text.startswith("!admin"):
            await self.admin(event)

        if not event.event.sender:
            await event.event.get_sender()
        sender = event.event.sender

        if sender.id in self.monitored:
            check = self.monitored[sender.id]
            result = check.tick(event.event.message.media or len(event.text) > 250)
            if result is None:
                self.monitored.pop(sender.id)
            elif result and not check.restricted:
                name = event.client.user_name(sender)
                check.restricted = True
                try:
                    await self.toggle_mute(event, {sender.id: name}, True)
                finally:
                    await event.reply("%s is flooding\n %s" % (name, self._admin_at))

    @AdvancedBot.admin_command()
    async def add_chat(self, event: NewMessageEvent):
        """
        Adds the current chat to the bot
        """
        chat = await event.event.get_chat()
        if self.do_add_chat(chat):
            await self.reply_in_admin_chat(event, "Chat added")
        else:
            await self.reply_in_admin_chat(event, "Chat was already enabled")

    @AdvancedBot.admin_command()
    async def remove_chat(self, event: NewMessageEvent):
        """
        Removes the current chat from the bot
        """
        chat = await event.event.get_chat()
        if self.do_remove_chat(chat):
            await self.reply_in_admin_chat(event, "Chat removed")
        else:
            await self.reply_in_admin_chat(event, "Chat wasn't enabled")

    @AdvancedBot.admin_command()
    async def list_chats(self, event: NewMessageEvent):
        """
        Shows chats enabled for the bot
        """
        await self.do_list_chats(event)

    @AdvancedBot.admin_command()
    async def set_title(self, event: NewMessageEvent):
        """
        Sets admin title for a user
        """
        chunks = await event.parse_text()

        for index, chunk in enumerate(chunks):
            if chunk.is_mention:
                if index+1 >= len(chunks) or not chunks[index+1].is_text:
                    await self.reply_in_admin_chat(event, "Missing title")
                    return

                await chunk.load()
                id = chunk.mentioned_id
                name = chunk.mentioned_name
                break
        else:
            await self.reply_in_admin_chat(event, "Missing mention")
            return

        title = chunks[index+1].text.strip()

        chats = await self.get_chats(event.client)

        reply = ""
        for chat in chats:
            try:
                entity = await event.client.set_admin_title(chat, chunk.mentioned_name, title)
                reply += "✅ %s\n" % chat.title
            except ChatAdminRequiredError:
                reply += "❌ %s - I'm not and admin\n" % chat.title
            except ChatAdminInviteRequiredError as e:
                reply += "❌ %s - User is already an admin or not in the chat\n" % chat.title
            except Exception as e:
                reply += "❌ %s - %s\n" % (chat.title, e)

        await self.reply_in_admin_chat(event, reply)

    @AdvancedBot.admin_command()
    async def monitor(self, event: NewMessageEvent):
        """
        Monitors a user for flood
        """
        ids = await self.mentions_from_message(event)
        if len(ids) == 0:
            await event.reply("Who?")
            return

        now = time.time()
        for id in ids:
            self.monitored[int(id)] = FloodCheck(now)

        await event.reply("Monitoring %s for flood" % ", ".join(ids.values()))

    @AdvancedBot.admin_command()
    async def mute(self, event: NewMessageEvent):
        """
        Mutes users
        """
        ids = await self.mentions_from_message(event)
        await self.toggle_mute(event, ids, True)

    @AdvancedBot.admin_command()
    async def unmute(self, event: NewMessageEvent):
        """
        Unmutes users
        """
        ids = await self.mentions_from_message(event)
        await self.toggle_mute(event, ids, False)

    @AdvancedBot.admin_command()
    async def naughty_list(self, event: NewMessageEvent):
        """
        Santa will take note
        """
        from telethon.tl.types import ChannelParticipantsKicked, ChannelParticipantsBanned

        msg = MessageFormatter()

        chats = await self.get_chats(event.client)
        for chat in chats:
            msg += "%s\n" % chat.title
            naughty = []

            kicked_users = await event.client.get_participants(chat, filter=ChannelParticipantsKicked)
            for user in kicked_users:
                naughty.append((user, "Kicked"))

            not_banned = await event.client.get_participants(chat)
            not_banned_id = set(u.id for u in not_banned)
            banned_users = await event.client.get_participants(chat, filter=ChannelParticipantsBanned)
            for user in banned_users:
                naughty.append((user, "Restricted" if user.id in not_banned_id else "Banned"))

            for user, reason in naughty:
                msg += " - "
                name = ""
                if user.username:
                    name += "@" + user.username
                else:
                    name += event.client.user_name(user)
                inuser = await event.client.get_input_entity(user)
                msg.mention(name, inuser)
                msg += " : "
                msg.bold(reason)
                msg += "\n"

            if not banned_users and not kicked_users:
                msg += " (No one)\n"

            msg += "\n"

        await self.reply_in_admin_chat(event, msg.text or "Everyone is nice", formatting_entities=msg.entities)
