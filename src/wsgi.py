"""
WSGI config for project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import django
from django.core.wsgi import get_wsgi_application
#from django.core.handlers.wsgi import WSGIHandler
from project import init
#from apps.social_media.integration import Integration


init.set_default_settings()

application = get_wsgi_application()
